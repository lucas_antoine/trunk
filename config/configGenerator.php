<?
/**
	This file generate a configcache.xml file
*/

require_once "../php/core/const.php";
require_once CORE_BASE."site.php";
use CORE\site;

define("CONF_EMPTY_LOCATION_URL","ALL");
define("CONF_EMPTY_CLASSIFICATION_URL","ALL");


\CORE\site::init();
site::$s->setDB(WEBSITES);
$req = site::$s->q("SELECT id,websiteName from websites");
echo "Which website ID do you a configcache.xml for ? \n";
while($res=f($req)){
	echo "   ".$res['id']."  -  ".$res['websiteName']."\n";
}
echo "\n Please enter a number : ";
fscanf(STDIN, "%d\n", $number);
$req = site::$s->q("SELECT * from websites where id=$number");
if(num($req)==0){
	echo "\n!!! This site ID is not defined...stopping ....\n\n";exit(1);
}
$res=f($req);
echo "\n\nWhat is the default language? default : ENG\n";
$lang = trim(fgets(STDIN));
if(strlen($lang)==0){
	$lang="ENG";
}
elseif(strlen($lang)!=3){
	echo "\nInvalid language code...stopping\n";exit(1);
}

echo "\nDefault module? default : expat\n";
$module = trim(fgets(STDIN));
if(strlen($module)==0){
	$module="expat";
}

echo "\nDefault action? default : home\n";
$action = trim(fgets(STDIN));
if(strlen($action)==0){
	$action="home";
}

echo "\nDevelopment Mode ? [1/0] default : 0\n";
$developmentMode = (int)trim(fgets(STDIN));
if(strlen($developmentMode)==0){
	$developmentMode=0;
}
if($developmentMode!=0 && $developmentMode!=1){
    echo "Invalid value for development Mode...exiting\n";
}


$doc = new DOMDocument('1.0');
$doc->formatOutput=true;

$website = $doc->createElement("website");


$id = $doc->createElement("id",$number);
$website->appendChild($id);

$l = $doc->createElement("defaultLang",$lang);
$website->appendChild($l);

$theme = $doc->createElement("theme",$res['theme']);
$website->appendChild($theme);

$skin = $doc->createElement("skin",$res['skin']);
$website->appendChild($skin);

$emptyLoc = $doc->createElement("emptyLocationUrl");
$ll = $doc->createElement($lang, CONF_EMPTY_LOCATION_URL);
$emptyLoc->appendChild($ll);
$website->appendChild($emptyLoc);


$emptyCat = $doc->createElement("emptyClassificationUrl");
$cc = $doc->createElement($lang, CONF_EMPTY_CLASSIFICATION_URL);
$emptyCat->appendChild($cc);
$website->appendChild($emptyCat);

$module = $doc->createElement("defaultModule",$module);
$website->appendChild($module);
$action = $doc->createElement("defaultAction",$action);
$website->appendChild($action);

$ga = $doc->createElement("googleAnalyticID", $res['googleAnalyticID']);
$website->appendChild($ga);
$adfs = $doc->createElement("adsenseChannel", $res['adsenseChannel']);
$website->appendChild($adfs);

$dm = $doc->createElement("developmentMode", $developmentMode);
$website->appendChild($dm);

$kruxID = $doc->createElement("kruxID",$res['kruxID']);
$website->appendChild($kruxID);

$doc->appendChild($website);

echo $doc->saveXML();

echo "\nDo you accept this version ? Y/N \n";
$accept = trim(fgets(STDIN));
if(strtolower($accept)=="y"){
	$txt = $doc->saveXML();
	file_put_contents("./configCache.xml",$txt);
}


?>
