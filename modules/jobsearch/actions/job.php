<?php
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
namespace JOBSEARCH\ACTIONS;

use \CORE\site;
use \JOBSEARCH\COMPONENT\rememberApply;
use \CORE\TEMPLATE\siteJS;

class job extends \CORE\action{
    
    
    /**
     * @todo Implement the spider detection tool and the Apply Job Stats
     */
    public function redirectJob(){
        $job = GET__JOBSEARCH_COMPONENT_job()[0];
        $job->stats->incrClick();
        \CORE\site::redirect($job->getHTTPREF());        
    }
    
    /**
     * job details
     * @throws InvalidArgumentException 
     */
    public function jobDetails() {
        //get the job properties and set them in the registry
        $job = GET__JOBSEARCH_COMPONENT_job()[0];       
        $job->stats->incrVisit();
        \CORE\site::$registry->set("job",  $job);
    }
    
}


?>
