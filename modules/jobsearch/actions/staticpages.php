<?php
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
namespace JOBSEARCH\ACTIONS;

use CORE\registry;
use CORE\site;


class staticpages  extends \CORE\action{

    public function error(){
        header('HTTP/1.0 404 Not Found');
        
    }
    
    
    /**
     * Home page 
     */
    public function home() {       
	 
        //remove breadcrumb 
        if(\CORE\cookie::value("lastVisit") != FALSE ){
            $timestamp = (int) \CORE\cookie::value("lastVisit");
        }
        else{
            $timestamp = time();
        }
        if(strtotime("-24 hours") > $timestamp){
            //Last time the user visted this page was at least yesterday.
            \CORE\cookie::set("lastVisit", (string) time() );
            \CORE\site::$registry->set("firstVisit",false);
            $nbJobs = \JOBSEARCH\COMPONENT\search::getNbJobsPostedSince($timestamp);
        }
        else{
            //The new number of jobs is much more likelly 0. (Refresh...) 
            \CORE\site::$registry->set("firstVisit",true);
            $nbJobs = \JOBSEARCH\COMPONENT\search::getNbTotalJobs();
        }
        \CORE\site::$registry->set("nbJobs",$nbJobs);
        
    }
    
    
    
}
?>
