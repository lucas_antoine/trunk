<?php
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
namespace JOBSEARCH\ACTIONS\AJAX;
use \CORE\site;

class autocomplete extends \CORE\action {


    public function location(){

        header("Access-Control-Allow-Origin: *");
        header("content-type: application/json");        
        $title = $_REQUEST['term'];
        $title = strip_tags(html_entity_decode($title, ENT_QUOTES, "UTF-8"));
        $title = trim($title);
        $title = str_replace(" & "," and ",$title);
        $title = str_replace("/"," ",$title);
        $title = str_replace('\\'," ",$title);
        $title = str_replace("("," ",$title);
        $title = str_replace(")"," ",$title);
        $title = preg_replace('/[0-9]+/', ' ',$title);
        $title = str_replace("-"," ",$title);
        $title = str_replace("$"," ",$title);
        $title = str_replace("*","",$title);
        $title = str_replace("+","\\\+",$title);
        $title = str_replace(".","\\\.",$title);
        $title = str_replace("^","",$title);
        $title = str_replace("?"," ",$title);
        $title = str_replace(","," ",$title);
        $title = preg_replace('/\s+/', ' ',$title);
        $title = preg_replace('/(?:\s\s+|\n|\t)/', ' ', $title);

        $data = explode("_", $title);
        foreach($data as $k=>$v){
            $data[$k] = $v."*";
        }
        $title = implode(" ", $data);        

        require_once EXT_BASE . 'sphinxapi.php';
        $sphinx = new \SphinxClient();
        $sphinx->setSelect("*");
        $sphinx->setServer(SPHINX_SEARCH_IP, 9307);
        //init
        $sphinx->SetArrayResult(true);
        $sphinx->SetLimits(0, 20, 20);
        $sphinx->SetSortMode(SPH_SORT_ATTR_ASC, "depth");

        //load Locationfilters
        $websiteClassifications = \CORE\globalData::$siteConfig->website->classifications;
        $ids = [];
        $depth=null;
        foreach($websiteClassifications as $classification){
            if($classification->typeID == 1){
                if($depth==null){
                    $depth = $classification->depth;
                }
                elseif($depth!=$classification->depth){
                    throw new \myException("Can't get two filters of the same type with different depth");
                }
                $ids[] = $classification->id;
            }
        }
        if(count($ids)>0){
            switch($depth){
                case 0 :
                    $sphinx->setFilter("regionID", $ids);
                    break;
                case 1 :
                    $sphinx->setFilter("countryID", $ids);
                    break;
                case 2 :
                    $sphinx->setFilter("stateID", $ids);
                    break;
                case 3 :
                    $sphinx->setFilter("countyID", $ids);
                    break;
                case 4 :
                    $sphinx->setFilter("localadminID", $ids);
                    break;
                case 5 :
                    $sphinx->setFilter("townID", $ids);
                    break;
                default :
                    throw new \myException("invalid value for 'depth' : '$depth' ");
            }
        }



        //get the results
        $res = $sphinx->Query($title, "index_locations");        

        //if an error was found we throw an exception
        if ($res['error'] != 0) {
            throw new sphinxException("a sphinx exception has just occured : " . var_export($res, true));
        }
        if ($sphinx->_error != 0) {
            throw new sphinxException("a sphinx exception has just occured : " . var_export($sphinx, true));
        }
        if($res['total_found']==0){
            echo json_encode([]);
            exit();
        }
        $results = [];
        if($depth==null){$depth=0;}


        foreach($res['matches'] as $data){
            $text = [];
            $upLevel = round( ($data['attrs']['depth']-$depth) /2 );
            switch($data['attrs']['depth']){
                case 1 :
                    $text[] = html_entity_decode($data['attrs']['countryname'], ENT_QUOTES, "UTF-8");
                    if($upLevel>0){
                        $text[] = $this->getText($data['attrs'], 1, $upLevel);
                    }
                    break;
                case 2 :
                    $text[] = html_entity_decode($data['attrs']['statename'], ENT_QUOTES, "UTF-8");
                    if($upLevel>0){
                        $text[] = $this->getText($data['attrs'], 2, $upLevel);
                    }
                    break;
                case 3 :
                    $text[] = html_entity_decode($data['attrs']['countyname'], ENT_QUOTES, "UTF-8");
                    if($upLevel>0){
                        $text[] = $this->getText($data['attrs'], 3, $upLevel);
                    }
                    break;
                case 4 :
                    $text[] = html_entity_decode($data['attrs']['localadminname'], ENT_QUOTES, "UTF-8");
                    if($upLevel>0){
                        $text[] = $this->getText($data['attrs'], 4, $upLevel);
                    }
                    break;
                case 5 :
                    $text[] = html_entity_decode($data['attrs']['townname'], ENT_QUOTES, "UTF-8");
                    if($upLevel>0){
                        $text[] = $this->getText($data['attrs'], 5, $upLevel);
                    }
                    break;
            }
            $results[] = ["id" => $data['id'], "value"=>implode(", ", $text) ];

        }
        echo json_encode($results, JSON_UNESCAPED_UNICODE);
    }

    private function getText($attr, $depth, $upLevel){
        $d = $depth-$upLevel;
        switch($d){
            case 0 :
                return  html_entity_decode($attr['regionname'], ENT_QUOTES, "UTF-8");
            case 1 :
                return  html_entity_decode($attr['countryname'], ENT_QUOTES, "UTF-8");
            case 2 :
                return html_entity_decode($attr['statename'], ENT_QUOTES, "UTF-8");
            case 3 :
                return  html_entity_decode($attr['countyname'], ENT_QUOTES, "UTF-8");
            case 4 :
                return html_entity_decode($attr['localadminname'], ENT_QUOTES, "UTF-8");
            case 5 :
                return  html_entity_decode($attr['townname'], ENT_QUOTES, "UTF-8");
        }



        return $data;

    }
}
?>
