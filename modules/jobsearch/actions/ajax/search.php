<?php
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
namespace JOBSEARCH\ACTIONS\AJAX;

use CORE\site;
use CORE\siteConfig;
use CORE\cookie;

use JOBSEARCH\COMPONENT\job;



class search extends \CORE\action{
    
    
    public function getSearchUrl(){
        $u = new \CORE\URL\urlBuilder(6);



        //$location = \JOBSEARCH\COMPONENT\classificationP::createFrom_MgetUrlPart1("France", [1,2]);
        $searchParams = [];
        if(isset($_REQUEST['workType'])){
            $searchParams[] = \JOBSEARCH\COMPONENT\classificationP::createFrom_PencodedID((int)$_REQUEST['workType']);
        }
        if(isset($_REQUEST['industry'])){
            $searchParams[] = \JOBSEARCH\COMPONENT\classificationP::createFrom_PencodedID((int)$_REQUEST['industry']);
        }
        if(isset($_REQUEST['text']) && trim($_REQUEST['text'])!=""){
            $searchParams[] = \JOBSEARCH\COMPONENT\keyword::createFrom_MgetText(trim($_REQUEST['text']));
        }
        if(isset($_REQUEST['locID']) && (int)$_REQUEST['locID']!=0){
            $searchParams[] = \JOBSEARCH\COMPONENT\classificationP::createFrom_Pid((int)$_REQUEST['locID']);
        }
        $searchParams['Page']=1;
        
        $url = $u->generateURL($searchParams);
        $url2 = URL_search($searchParams);
        echo $url->url;
    }
    
   
    
}




?>
