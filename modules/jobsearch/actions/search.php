<?php
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
namespace JOBSEARCH\ACTIONS;

use CORE\site;
use CORE\siteConfig;
use CORE\cookie;

class search  extends \CORE\action{

    const searchHistoryLimit = 10;


    /**
     * Search results pages
     */
    public function searchResults() {
        $k=new \JOBSEARCH\COMPONENT\keyword();

        if(\CORE\site::$registry->has("sphinxClient")){
            \CORE\site::$registry->delete("sphinxClient");
        }

        $search = new \JOBSEARCH\COMPONENT\search();
        $sphinx = $search->usualSearch(false,[]);
        
        // Set search History
        $searchParams = \GET__JOBSEARCH_COMPONENT_classificationP();
        if($searchParams ==null){
            $searchParams = [];
        }
        if(GET__JOBSEARCH_COMPONENT_keyword()!==null){
            $searchParams = array_merge($searchParams, GET__JOBSEARCH_COMPONENT_keyword());
        }
        if(count($searchParams)>0){
            if(site::$registry->has("searchHistory")){
                $searchHistory = site::$registry->searchHistory;
            }
            else{
                $searchHistory = [];
            }
            $searchHistory[] = $searchParams;
            while(count($searchHistory)> self::searchHistoryLimit){
                array_shift($searchHistory);
            }
            site::$registry->set("searchHistory", $searchHistory, true, 1);
        }
        // End search History

        site::$registry->set("sphinxClient", $sphinx, false);
        //if 0 results

        site::$registry->set("search", $search);
        
        if($search->foundRows != 0){
            if(site::$registry->has("history")){
                $history = site::$registry->history;
            }
            else{
                $history = [];
            }
            foreach($search->resultList as $job){
                $history[$job->id] = $_SERVER["REQUEST_URI"];
            }
            site::$registry->set("history", $history, true, 1);
        }
    }

    /**
     * Sets the location variable for jobsbyqueries
     */
    private  function setLocationInfo(){
        if(self::$minDepth==null){
            //min depth loc id parent id
            site::$s->setDB(DB_WEBSITES);
            $sql = "select count(*) as nb, min(depth) as depth,locationID from websitesLocations where websiteID= " . siteConfig::$id .";";
            $result = site::$s->q($sql);
            $row = f($result);

            if($row['nb']>0){
                self::$minDepth = $row['depth'];
                switch($row['depth']+1){
                    case '0':
                        self::$parentLocID = 'regionID';
                        self::$locID = 'stateID';
                        break;
                    case '1':
                        self::$parentLocID = 'regionID';
                        self::$locID = 'stateID';
                        break;
                    case '2':
                        self::$parentLocID = 'countryID';
                        self::$locID = 'stateID';
                        break;
                    case '3':
                        self::$parentLocID = 'stateID';
                        self::$locID = 'countyID';
                        break;
                    case '4':
                        self::$parentLocID = 'countyID';
                        self::$locID = 'localAdminID';
                        break;
                    case '5':
                        self::$parentLocID = 'localAdminID';
                        self::$locID = 'townID';
                        break;
                }
            }
            else{
                //country by default
                self::$locID = "countryID";
                //self::$locationJoin = "";
            }
        }
    }


    /**
     * Rss action
     */

    public static function rssResults(){
        $search = new \JOBSEARCH\COMPONENT\search();
        $search->usualSearch(false);
        site::$registry->set("search", $search);
    }





}
?>
