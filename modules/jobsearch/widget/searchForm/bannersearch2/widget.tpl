<?=$richtitle;?>

<div class="subtitle">
    <?=$keywordText;?>    
</div>
<input type="text" id="keywordSearch" class="inputSearch" value="<?= $searchKeyword; ?>" placeholder="<?=$keywordPlaceHolder;?>"/>

<div class="subtitle">
    Browse by sector
</div>
<select id="industrySearch" size="5" class="inputSearch">
    <?
    foreach ($industries as $id => $name) {
        echo "<option value='$id'";
        if ($id == $searchIndustryID) {
            echo " selected='selected' ";
        }
        echo ">$name</option>";
    }
    ?>
</select>


<div class="subtitle">
    <?=$locationText;?>
</div>
<input type="text" id="locationSearch" class="inputSearch"  <?
if ($searchLocationID != null) {
    echo "value='$searchLocationText'";
}
?>  placeholder="<?=$locationPlaceHolder;?>"/>

<div class="subtitle">
    Browse by work types
</div>
<select id="workTypeSearch" class="inputSearch">
    <option value="">Any</option>
    <?
    foreach ($workTypes as $id => $name) {
        echo "<option value='$id'";
        if ($id == $searchEmploymentTypeID) {
            echo " selected='selected' ";
        }
        echo ">$name</option>";
    }
    ?>
</select>
<input type="hidden" id="locID" <?
    if ($searchLocationID != null) {
        echo "value='$searchLocationID'";
    }
    ?> />

<br />

<button id="submitSearch"  type="button" >
    <?=$searchText;?>
</button>
<div style="clear:both"></div>    
