$(document).ready(function() {
        
        $("#submitSearch").click(function() {
            $("input").attr("disabled", "disabled");
            $("select").attr("disabled", "disabled");
            $("button").attr("disabled", "disabled");
            $.ajax({
                url: "<?= $searchUrl; ?>",
                data: {
                    text: $("#keywordSearch").val(),
                    industry: $("#industrySearch").val(),
                    workType: $("#workTypeSearch").val(),
                    locID : $("#locID").val()
                },
                success: function(data) {
					window.location = data;
                }
            });
        });
        $("#locationSearch").typeahead([
            {
                name: 'Enter Location here',
                remote: {
                    url: '<?= $locationUrl ?>?term=%QUERY',
                    cache: false
                },
                template: '<p>{{value}}</p>',
                engine: Hogan
            }
        ]);

        $('#locationSearch').bind('typeahead:selected', function(obj, datum) {      
            $("#locID").val(datum.id);
    });


});
