$("#submitSearch").click(function() {
            submit();
        });

        function submit() {
            $("input").attr("disabled", "disabled");
            $("button").attr("disabled", "disabled");
<? if ($showLocation) { ?>
            if($("#locationSearch").val()==""){
                $("#locID").val("");
            }
<?}?>

            $.ajax({
                url: "<?= $searchUrl; ?>",
                data: {
                    text: $("#keywordSearch").val(),
                    locID: $("#locID").val()
                },
                success: function(data) {
                    window.location = data;
                }
            });
        }
        $("#keywordSearch").keypress(function(event) {
            if (event.which == 13) {
                submit();
            }
        });

<? if ($showLocation) { ?>

            $("#locationSearch").keypress(function(event) {
                if (event.which == 13) {
                    submit();
                }
            });

            $("#locationSearch").typeahead([
                {
                    name: 'Enter Location here',
                    remote: {
                        url: '<?= $locationUrl ?>?term=%QUERY',
                        cache: false
                    },
                    template: '<p>{{value}}</p>',
                    engine: Hogan
                }
            ]);

            $('#locationSearch').bind('typeahead:selected', function(obj, datum) {
                $("#locID").val(datum.id);
            });


<? } ?>