<?php
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
namespace JOBSEARCH\WIDGET;

use \CORE\site;

/**
 * Universal widget
 */

class searchForm extends \CORE\TEMPLATE\widget {

    public $workTypes=[];

    public $industries=[];

    public $searchUrl;
    
    public $locationUrl;

    public $showLocation;

    public $nbJobs=0;

    public $buttonImg;
    
    public $richtitle;

    /**
     *
     * @var string
     */
    public $keywordText,$keywordPlaceHolder,$locationText,$locationPlaceHolder,$searchText,$title;
    
    
    public $searchLocationText = "";
    public $searchLocationID = null;
    public $searchIndustryID = null;
    public $searchEmploymentTypeID = null;
    public $searchKeyword = "";

    public function preprocessJS(){
        $this->showLocation = $this->getOption("showLocation")=="1" ? true : false;
        
        
        $ub = new \CORE\URL\urlBuilder(9);
        $this->searchUrl = $ub->generateURL([], self::$currentUrlMode)->url;

        $ub = new \CORE\URL\urlBuilder(10);
        $this->locationUrl = $ub->generateURL([], self::$currentUrlMode)->url;
        
        
    }

    public  function preProcess(){

        $ub = new \CORE\URL\urlBuilder(9);
        $this->searchUrl = $ub->generateURL([])->url;

        $ub = new \CORE\URL\urlBuilder(10);
        $this->locationUrl = $ub->generateURL([])->url;
        
        
        $this->loadEmploymentTypes();
        
        $this->loadIndustries();
        
        $this->loadPreviousSearchParamaters();

        

        $this->showLocation = $this->getOption("showLocation")=="1" ? true : false;
        $this->keywordText = $this->getOption("keywordText");
        $this->keywordPlaceHolder = $this->getOption("keywordPlaceHolder");
        $this->locationText = $this->getOption("locationText");
        $this->locationPlaceHolder = $this->getOption("locationPlaceHolder");
        $this->searchText = $this->getOption("searchText");
        $this->title = $this->getOption("title");
        $this->richtitle = $this->getOption("richtitle");

        
        if($this->title!="" && strpos($this->title, "%NB")!==false){
            $tmpSearch = new \JOBSEARCH\COMPONENT\search();
            $this->nbJobs = $tmpSearch->getNbTotalJobs();
            $this->title = trim(str_replace("%NB", $this->nbJobs, $this->title));
        }
        try{
            $this->buttonImg = $this->getImageSrc("buttonImage", "buttonImageFileName");
        }
        catch(\Exception $e){
            //var_dump($e);
        }


    }
    
    private function loadPreviousSearchParamaters(){
        $classifications = GET__JOBSEARCH_COMPONENT_classificationP();
        
        if(is_array($classifications)){
            foreach($classifications as $classification){
                switch($classification->typeID){
                    case 1 : 
                        $this->searchLocationText = $classification->lastName;
                        $this->searchLocationID = $classification->id;
                        break;
                    case 2 : 
                        $this->searchIndustryID=$classification->encodedID;
                        break;
                    case 5 : 
                        $this->searchEmploymentTypeID = $classification->encodedID;
                        break;
                    default : 
                        throw new \Exception();
                }
            }
        }
        $keyword = GET__JOBSEARCH_COMPONENT_keyword();
        if($keyword!=null){
            $this->searchKeyword = $keyword[0]->getText();
        }
        
    }

    private function loadIndustries(){
        site::$s->setDB(DB_WEBSITES2);
        $req = site::$s->q("SELECT C.id
            FROM websiteClassifications WC
            INNER JOIN ".DB_CLASSIFICATIONS2.".classifications C ON C.id=WC.classificationID 
            INNER JOIN ".DB_CLASSIFICATIONS2.".classificationOrigins CO ON CO.id=C.classificationOriginID 
            WHERE CO.typeID=2 AND WC.websiteID=".\CORE\globalData::$siteConfig->id);
        if(num($req)>1){
            while($res=f($req)){
                $tmp = \JOBSEARCH\COMPONENT\classificationP::createFrom_Pid($res['id']);
                $this->industries[$tmp->encodedID] = $tmp->lastName;
            }
        }
        else{
            $req = site::$s->q("SELECT C.id
            FROM websiteClassificationOrigins WCO
            INNER JOIN ".DB_CLASSIFICATIONS2.".classifications C ON C.classificationOriginID=WCO.classificationOriginID 
            INNER JOIN ".DB_CLASSIFICATIONS2.".classificationOrigins CO ON CO.id=C.classificationOriginID 
            WHERE CO.typeID=2 AND WCO.websiteID=".\CORE\globalData::$siteConfig->id);
            while($res=f($req)){
                $tmp = \JOBSEARCH\COMPONENT\classificationP::createFrom_Pid($res['id']);
                $this->industries[$tmp->encodedID] = $tmp->lastName;
            }
        }
    }
    
    private function loadEmploymentTypes(){
        site::$s->setDB(DB_WEBSITES2);
        $req = site::$s->q("SELECT C.id
            FROM websiteClassifications WC
            INNER JOIN ".DB_CLASSIFICATIONS2.".classifications C ON C.id=WC.classificationID 
            INNER JOIN ".DB_CLASSIFICATIONS2.".classificationOrigins CO ON CO.id=C.classificationOriginID 
            WHERE CO.typeID=5 AND WC.websiteID=".\CORE\globalData::$siteConfig->id);
        if(num($req)>1){
            while($res=f($req)){
                $tmp = \JOBSEARCH\COMPONENT\classificationP::createFrom_Pid($res['id']);
                $this->workTypes[$tmp->encodedID] = $tmp->lastName;
            }
        }
        else{
            $req = site::$s->q("SELECT C.id
            FROM websiteClassificationOrigins WCO
            INNER JOIN ".DB_CLASSIFICATIONS2.".classifications C ON C.classificationOriginID=WCO.classificationOriginID 
            INNER JOIN ".DB_CLASSIFICATIONS2.".classificationOrigins CO ON CO.id=C.classificationOriginID 
            WHERE CO.typeID=5 AND WCO.websiteID=".\CORE\globalData::$siteConfig->id);
            while($res=f($req)){
                $tmp = \JOBSEARCH\COMPONENT\classificationP::createFrom_Pid($res['id']);
                $this->workTypes[$tmp->encodedID] = $tmp->lastName;
            }
        }
    }


}
?>
