
<div class='search'>
    <div class='head'>
        Search <?=$nbJobs;?> Jobs
    </div>

    <div class="<?= $showLocation ? 'item' : 'item2';?>">
        <?=$keywordText;?><br />
        <input type="text" id="keywordSearch" class="inputSearch" placeholder="<?=$keywordPlaceHolder;?>"/>
    </div>
    <?if($showLocation){?>
    <div class="item">
        <?=$locationText;?><br />
        <input type="text" id="locationSearch" class="inputSearch"  placeholder="<?=$locationPlaceHolder;?>"/>
        <input type="hidden" id="locID" />
    </div>
    <?}?>
    <div class="btn">
        <button id="submitSearch" class="submitSearch"  type="button" >
            <?if($buttonImg!=null){?>
            <img src='<?=$buttonImg;?>' />
            <?}?>
            <?=$searchText;?>
        </button>
    </div>
</div>