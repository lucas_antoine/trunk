
<div class='search'>
    <?if($title!=""){?>
    <div class='head'>
        <?=$title;?>
    </div>
    <?}?>

    <div class="<?= $showLocation ? 'item_1' : 'item_2';?>">
        <?=$keywordText;?><br />
        <input type="text" id="keywordSearch" class="inputSearch" placeholder="<?=$keywordPlaceHolder;?>"/>
    </div>
    <?if($showLocation){?>
    <div class="item_1">
        <?=$locationText;?><br />
        <input type="text" id="locationSearch" class="inputSearch"  placeholder="<?=$locationPlaceHolder;?>"/>
        <input type="hidden" id="locID" />
    </div>
    <?}?>
    <div class="btn">
        <button id="submitSearch" class="submitSearch"  type="button" >
            <?if($buttonImg!=null){?>
            <img src='<?=$buttonImg;?>' />
            <?}?>
            <?=$searchText;?>
        </button>
    </div>
</div>