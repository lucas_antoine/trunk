$("#submitSearch").click(function() {
    $("input").attr("disabled", "disabled");
    $("button").attr("disabled", "disabled");
<? if ($showLocation) { ?>
            if($("#locationSearch").val()==""){
                $("#locID").val("");
            }
<?}?>
    $.ajax({
        url: "<?= $searchUrl; ?>",
        data: {
            text: $("#keywordSearch").val(),
            locID : $("#locID").val()
        },
        success: function(data) {            
            window.location = data;
        }
    });
});
<?if($showLocation){?>

$("#locationSearch").typeahead([
    {
        name: "<?=$locationPlaceHolder;?>",
        remote: {
            url: '<?= $locationUrl ?>?term=%QUERY',
            cache: false
        },
        template: '<p>{{value}}</p>',
        engine: Hogan
    }
]);

$('#locationSearch').bind('typeahead:selected', function(obj, datum) {
    $("#locID").val(datum.id);
});

<?}?>