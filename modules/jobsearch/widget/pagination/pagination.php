<?php
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
namespace JOBSEARCH\WIDGET;

class pagination extends \CORE\TEMPLATE\widget {

    public $links;
    public $firstPage;
    public $lastPage;
    public $currentPage;
    public $nbResults;

    public  function preProcess() {
        
        
        if(!\CORE\site::$registry->has("search")){
            throw new \myException("Search required in pagination");
        }
        
        $search = \CORE\site::$registry->search;
        $this->nbResults = $search->foundRows;
        
        $searchData = \GET__JOBSEARCH_COMPONENT_classificationP();
        if($searchData == null){
            $searchData = [];
        }
        $keywordData = \GET__JOBSEARCH_COMPONENT_keyword();
        if(is_array($keywordData) && isset($keywordData[0])){
            $searchData[] = $keywordData[0];
        }
        
        
        $options = \MODELS\WEBSITES\optionsQuery::create()->findPk($_SERVER['ID']);


        $this->currentPage = GET__LITERAL__PAGE();

        $nbResultsPerPage = $options->getnbResultsPerPage();

        
        $this->links = array();
        
        
        /**
         * @todo put this one into conf.yaml
         */
        $nblinks = $this->getOption("nbPages");
        

        

        $this->links[$this->currentPage] = URL_search(array_merge($searchData,  ["Page"=>($this->currentPage)] ));
        $t = 1;
        $i = 1;
        $s1 = false;
        $s2 = false;
        $page = $this->currentPage;
        while ($i < $nblinks) {
            if (($this->currentPage - $t) > 0) {
                $page = $this->currentPage - $t;
                if ($page < 100) {
                    $this->links[$page] = URL_search( array_merge($searchData,  ["Page"=>($page)] ));
                    $i++;
                }
            } else {
                $s1 = true;
            }
            if (( $nbResultsPerPage * (($this->currentPage-1) + $t) ) < $this->nbResults && $i < $nblinks) {
                $page = $this->currentPage + $t;
                if ($page < 100) {
                    $this->links[$page] = URL_search(array_merge($searchData,  ["Page"=>($page)] ));
                    $i++;
                }
            } else {
                $s2 = true;
            }
            if ($s1 && $s2) {
                $i = $nblinks+1;
            }
            $t++;
        }
        ksort($this->links);
        
        $this->lastPage = round($this->nbResults / $nbResultsPerPage);
        

    }

    public static function RENDER($widgetThemeID) {
        parent::RENDER($widgetThemeID);
    }

}

?>
