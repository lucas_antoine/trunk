<table border=0 cellpadding="5">
    <?php
    foreach($this->list as $job){                        
    ?>
    <tr>
        <td class="c1">
            <h2>
                <a href="<?=$job->getURL()->url;?>"><?=$job->getTitle();?></a>
            </h2>
            <div class="infos">
                <?= isset($job->employmentType[0]) ? $job->employmentType[0]->lastName : "No EMployment Types";?>
                <?
                if($job->getCompanyName()!=""){
                    echo " | ".$job->getCompanyName();
                }
                ?>
            </div>
            <div class="description">
                <?=$job->getShortDescription(70);?>
            </div>
            
        </td>
        <td class="c2">
            <?= isset($job->location[0]) ? $job->location[0]->lastName : "No LOCATION";?>
        </td>
        <td class="c3">
            <?= isset($job->employmentType[0]) ? $job->employmentType[0]->lastName : "No EMployment Types";?>
        </td>
        <td class="c4">
             <?
                if($niceDateFormat=='true'){
                echo $job->nicedate;
                }
                else if($job->isPostedToday()){
                echo $job->getPostedDate()->format($todayFormat);
                }
                else{
                echo $job->getPostedDate()->format($dateFormat);
                }
                ?>
        </td>
    </tr>
    
    <?
    }
    ?>
</table>
<?
if(count($this->list)==0){
?>
<p class="noresult"><?=$noResultsText;?></p>
<?
}
?>


