<?php
$t= "odd";
foreach($this->list as $job){
    if($t=="odd"){
        $t="even";
    }else{
        $t="odd";
    }
?>

<div class="resultItem <?=$t;?>">
    <div class="date">
        <?
        if($niceDateFormat=='true'){
            echo $job->nicedate;
        }
        else if($job->isPostedToday()){
            echo $job->getPostedDate()->format($todayFormat);
        }
        else{
            echo $job->getPostedDate()->format($dateFormat);
        }
        ?>
    </div>
    <h2 class="title">
        <a href="<?=$job->getURL()->url;?>"><?=$job->getTitle();?></a>
    </h2>
    <div class="p1">
        
        <p>
            <span class="key"><?=$companyText;?></span>
            <span class="value"><?= $job->getCompanyName()=="" ? $noCompanyText : $job->getCompanyName();?></span>
        </p>
        <p>
            <span class="key"><?=$locationText;?></span>
            <span class="value"><?=$job->location[0]->lastName;?></span>
        </p>
        <p>
            <span class="key"><?=$employmentTypeText;?></span>
            <span class="value"><?= isset($job->employmentType[0]) ? $job->employmentType[0]->lastName : "";?></span>
        </p>
        <p>
            <span class="key"><?=$industryText;?></span>
            <span class="value"><?= isset($job->industry[0]) ? $job->industry[0]->lastName : "";?></span>
        </p>
    </div>
    <div class="p2">
        <?=$job->getShortDescription(250);?>
    </div>
</div>
<?}

if(count($this->list)==0){
?>
<p class="noresult"><?=$noResultsText;?></p>
<?
}
?>