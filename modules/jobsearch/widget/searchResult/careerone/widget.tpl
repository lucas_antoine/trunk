<div class="resultSearch">
    <ol>
    <?php
    foreach($this->list as $job){                        
    ?>
    <li>
        <dl >
            <dd class='title'>
                <h2>
                    <a  title="<?= $job->getTitle(); ?>"  href="<?=$job->getURL()->url;?>">
                        <?= $job->getTitle();?></a>                    
                </h2>
                <span class="location">
                    <?=$job->location[0]->lastName;?> 
                </span>
                
            </dd>
            <dd class ="company" >
                <?
                if($job->getCompanyName()==""){
                    echo $noCompanyText;
                }
                else{
                    echo $job->getCompanyName();
                }
                
                ?>
            </dd>
            <dd class="date">
                <?
                if($niceDateFormat=='true'){
                    echo $job->nicedate;
                }
                else if($job->isPostedToday()){
                    echo $job->getPostedDate()->format($todayFormat);
                }
                else{
                    echo $job->getPostedDate()->format($dateFormat);
                }
                ?>
            </dd>
        </dl>
    </li>
    <?
    }
    ?>
    </ol>
</div>
<?
if(count($this->list)==0){
?>
<p class="noresult"><?=$noResultsText;?></p>
<?
}
?>


