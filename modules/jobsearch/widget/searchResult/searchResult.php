<?php

/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
namespace JOBSEARCH\WIDGET;

class searchResult extends \CORE\TEMPLATE\widget{
    
    public $list = [];
    
    public $todayFormat;
    public $dateFormat;
    public $niceDateFormat;
    
    public $industry =false;
    public $location =false;
    public $employmentType =false;
    
    public $nbResults;
    
    private $search;
    
    public $locationText,$industryText,$employmentTypeText, $companyText, $noCompanyText, $noResultsText;
    
    public  function preProcess(){
        
        
        
        if(!\CORE\site::$registry->has("search")){
            throw new \EX\NotFoundException("This widget requires a search defined");
        }    
        $this->todayFormat = $this->getOption("todayFormat");
        $this->dateFormat = $this->getOption("dateFormat");
        $this->niceDateFormat = $this->getOption("niceDateFormat");
        $this->nbResults = \CORE\site::$registry->search->foundRows;
        
        $this->locationText = $this->getOption("locationText");
        $this->industryText = $this->getOption("industryText");
        $this->employmentTypeText = $this->getOption("employmentTypeText");
        $this->companyText = $this->getOption("companyText");
        $this->noCompanyText = $this->getOption("noCompanyText");
        $this->noResultsText = $this->getOption("noResultsText");
        
        foreach(\CORE\site::$registry->search->searchParameters as $classificationP){
            switch($classificationP->typeID){
                case 1 : 
                    $this->location = $classificationP;
                    break;
                case 2 : 
                    $this->industry = $classificationP;
                    break;
                case 5 : 
                    $this->employmentType = $classificationP;
                    break;
            }
        }
        
        foreach(\CORE\site::$registry->search->resultList as $job){
            
            //$this->jobDetails = \GENERATED\urlHelper::jobsearchJobDetails(array($job,$job->location));
            if ($job->getPostedDate()->format('Y-m-d') == date("Y-m-d") ){
                //get hour
                $hours = round((strtotime("now") - strtotime($job->getPostedDate()->format("Y-m-d H:m")))/3600,1);
                if($hours < 1){
                    $job->nicedate = "Less than 1 hour ago";
                }
                else if($hours == 1){
                    $job->nicedate = "1 hour ago";
                }
                else{
                    $job->nicedate = $hours . " hours ago";
                }
            }
            else{
                $age = round((time()-strtotime($job->getPostedDate()->format("Y-m-d")))/(60*60*24),0);
                if($age==1){
                    $job->nicedate = " 1 day old";
                }
                else{
                    $job->nicedate = $age." days ago";
                }
            }

            $match = array();
            $description = html_entity_decode($job->getDescription() , ENT_QUOTES , "UTF-8");
            if (preg_match('/^.{1,150}\b/s', $description, $match)){
                    $description=$match[0];
            }
            $this->list[]=$job;
        }
        
    }


}

?>