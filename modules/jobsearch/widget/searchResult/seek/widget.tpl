<?php
 foreach($this->list as $job){                        
 ?>
    <!-- google_ad_section_start(weight=ignore) -->
    <div class="element">
        <div class="part1">
            <a target='_blank'  title="<?= $job->getTitle(); ?>"  href="<?=$job->getURL()->url;?>">
                            <!-- google_ad_section_end -->
                            <!-- google_ad_section_start --><?= $job->getTitle() ?> <!-- google_ad_section_end --></a><br />
            <span class="company"><?=$job->getCompanyName();?></span>
            
            <div class="description">
                <?=$job->getShortDescription(80);?>
            </div>
            <div class="classification">
                <?=isset($job->industry[0]) ? $job->industry[0]->lastName : '';?>
            </div>
        </div>
        <div class="part2">
            <div class="date">
                <?
                if($niceDateFormat=='true'){
                    echo $job->nicedate;
                }
                else if($job->isPostedToday()){
                    echo $job->getPostedDate()->format($todayFormat);
                }
                else{
                    echo $job->getPostedDate()->format($dateFormat);
                }
                ?>
            </div>
            
            <div class="location">
                <?=$job->location[0]->lastName;?>
            </div>
        </div>
    </div>
    <!-- google_ad_section_end -->
<?
}

if(count($this->list)==0){
?>
<p class="noresult"><?=$noResultsText;?></p>
<?
}
?>


