<div class="list">
    <div class="breacrumb">
        <?= $location != false ? $location->lastName . " Jobs" : "" ?> 
        <?= $industry != false ? " > " : false; ?>
        <?= $industry != false ? $industry->lastName : ""; ?>
        - <?= $nbResults; ?> jobs found

    </div>
    <ul>
        <?php
        foreach ($this->list as $job) {
            ?>
            <!-- google_ad_section_start(weight=ignore) -->

            <li>

                <h2>
                    <a target='_blank'  title="<?= $job->getTitle(); ?>"  href="<?= $job->getURL()->url; ?>">
                        <?= $job->getTitle(); ?></a>
                </h2>
                <span  class="time">

                    Listed
                    <?
                    if ($niceDateFormat == 'true') {
                        echo $job->nicedate;
                    } else if ($job->isPostedToday()) {
                        echo $job->getPostedDate()->format($todayFormat);
                    } else {
                        echo $job->getPostedDate()->format($dateFormat);
                    }
                    ?>
                </span>
                <span class="type">
                    <?= $job->employmentType[0]->lastName; ?>                             
                </span>
                <div class="clear"></div>    
                <div class="company">
                    <?= $job->getCompanyName(); ?>
                </div>
                <div class="location">
                    <?= $job->location[0]->lastName; ?>
                </div>
                <div class="description">
                    <?= $job->getShortDescription(175); ?> ...
                </div>
            </li>
            <?
        }
        ?>
    </ul>
    <div style="clear:both"></div>
</div>
<?
if (count($this->list) == 0) {
    ?>
    <p class="noresult"><?= $noResultsText; ?></p>
    <?
}
?>

