<ul>

    <? if ($searchText != null) { ?>
        <li>
            <span><?= $searchText; ?></span>
        </li>
    <? } ?>


    <?
    if (count($locationNames) > 0) {
        foreach ($locationNames as $name => $link) {
            ?>
            <li>
                <? if ($links) { ?>
                    <a href="<?= $link->url; ?>"><?= $name; ?></a>
                <? } else { ?>
                    <?= $name; ?>
            <? } ?>
            </li>
        <? }
    }
    ?>

        <? if ($industryName != null) { ?>
        <li>
            <? if ($links) { ?>
                <a href="<?= $industryLink->url; ?>"><?= $industryName; ?></a>
            <? } else { ?>
            <?= $industryName; ?>
        <? } ?>
        </li>
    <? } ?>

        <? if ($employmentTypeName != null) { ?>
        <li>
            <? if ($links) { ?>
                <a href="<?= $employmentTypeLink->url; ?>"><?= $employmentTypeName; ?></a>
            <? } else { ?>
            <?= $employmentTypeName; ?>
        <? } ?>
        </li>
    <? } ?>

    <? if ($keywords != null) { ?>
        <li>
            <? if ($links) { ?>
                <a href="<?= $keywordLink->url; ?>"><?= $keywords; ?></a>
            <? } else { ?>
            <?= $keywords; ?>
        <? } ?>
        </li>
    <? } ?>

    <? if (!$hasParameters) { ?>
        <li><span><?= $noParametersText; ?></span></li>
<? } ?>
</ul>