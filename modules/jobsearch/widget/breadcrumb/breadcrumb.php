<?
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
namespace JOBSEARCH\WIDGET;

class breadcrumb extends \CORE\TEMPLATE\widget {

    public $searchText = null;
    public $industryName = null;

    /**
     *
     * @var \CORE\URL\url[]
     */
    public $industryLink = null;

    /**
     *
     * @var \CORE\URL\url[]
     */
    public $locationNames = [];

    /**
     *
     * @var \CORE\URL\url[]
     */
    public $employmentTypeLink = null;
    public $employmentTypeName = null;
    public $locationFormat;

    /**
     *
     * @var \CORE\URL\url[]
     */
    public $keywordLink = null;
    public $keywords = null;
    public $noParametersText = null;

    /**
     *
     * @var boolean
     */
    public $links;
    private $search;
    public $hasParameters = false;

    private function getWebsiteLocationDepth() {
        $classifications = \CORE\globalData::$siteConfig->website->classifications;
        $depth = 0;
        foreach ($classifications as $classi) {
            if ($classi->typeID == 1 && $classi->depth > $depth) {
                $depth = $classi->depth;
            }
        }
        return $depth;
    }

    public function preProcess() {

        $this->searchText = $this->getOption("searchText");
        $this->locationFormat = $this->getOption("locationFormat");
        $this->links = $this->getOption("links");

        $k = GET__JOBSEARCH_COMPONENT_keyword();
        if ($k != null) {
            $this->hasParameters = true;
            $this->keywords = $k[0]->getText();
            if ($this->links) {
                $this->keywordLink = URL_search($k);
            }
        }

        $classifications = GET__JOBSEARCH_COMPONENT_classificationP();


        if (is_array($classifications)) {
            $this->hasParameters = true;
            foreach ($classifications as $classi) {
                switch ($classi->typeID) {
                    case 1 :
                        $location = $classi;
                        switch ($this->locationFormat) {
                            case "full" :
                                foreach ($classi->elements as $element) {
                                    if ($this->links) {
                                        $this->locationNames[$element->name] = URL_search([$location]);
                                    } else {
                                        $this->locationNames[$element->name] = null;
                                    }
                                }
                                break;
                            case "relative" :
                                $depth = $this->getWebsiteLocationDepth();
                                if ($location->depth == $depth) {
                                    if ($this->links) {
                                        $this->locationNames[$classi->lastName] = URL_search([$classi]);
                                    } else {
                                        $this->locationNames[$classi->lastName] = null;
                                    }
                                } else {
                                    for ($i = $depth; $i <= $classi->depth; $i++) {
                                        if ($this->links) {
                                            $cl = new \JOBSEARCH\COMPONENT\classificationP("scratch", $classi->elements[$i]->id);
                                            $this->locationNames[$classi->elements[$i]->name] = URL_search([$cl]);
                                        } else {
                                            $this->locationNames[$classi->elements[$i]->name] = null;
                                        }
                                    }
                                }
                                break;
                            case "short" :
                                if ($this->links) {
                                    $this->locationNames[$classi->lastName] = URL_search([$classi]);
                                } else {
                                    $this->locationNames[$classi->lastName] = null;
                                }
                                break;
                            default :
                                throw new \myException("Unknown locationFormat '$this->locationFormat'");
                        }
                        break;
                    case 2 :
                        if ($this->links) {
                            $this->industryLink = URL_search([$classi]);
                        }
                        $this->industryName = $classi->lastName;
                        break;
                    case 5 :
                        if ($this->links) {
                            $this->employmentTypeLink = URL_search([$classi]);
                        }
                        $this->employmentTypeName = $classi->lastName;
                    default :
                        throw new \myException("This classification type ID is not implemented in the breadCrumb '$classi->typeID'");
                }
            }
        }
        if (!$this->hasParameters) {

            $seo = new \CORE\SEO\seo(6, []);

            $this->noParametersText = $seo->evaluatedTitle;
        }
    }

}

?>