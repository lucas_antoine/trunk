<?php
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
namespace JOBSEARCH\WIDGET;

class similarJobs extends \CORE\TEMPLATE\widget{

    public $search;

    public $limit, $jobLikeThisText;

    public  function preProcess(){
        $this->limit = (int)$this->getOption("limit");
        $this->jobLikeThisText = $this->getOption("jobLikeThisText");
        $job = \CORE\site::$registry->job;
        $this->search = new \JOBSEARCH\COMPONENT\search();
        $this->search->jobLikeThis($job, $this->limit);
    }

    public static function RENDER($localData=[]){
        parent::RENDER($localData);
    }
}

?>
