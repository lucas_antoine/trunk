<?php

/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace JOBSEARCH\WIDGET;

use \CORE\site;

class directory extends \CORE\TEMPLATE\widget {

    /**
     *
     * @var int
     */
    private $classificationTypeID;

    /**
     *
     * @var int
     */
    private $classificationOriginID;

    /**
     *
     * @var \JOBSEARCH\COMPONENT\classificationP[]
     */
    private $websiteClassifications = [];
    
    private $nameFormat;

    /**
     *
     * @var boolean
     */
    public $showChildren;

    /**
     *
     * @var boolean
     */
    public $showNbJobs;

    /**
     *
     * @var string
     */
    public $title;

    /**
     *
     * @var int
     */
    public $nbColumns;

    /**
     *
     * @var string Key is the URL, value is the text.
     */
    public $urls = [];

    private function setClassificationType() {
        $classificationName = $this->getOption("classificationName");

        site::$s->setDB(DB_CLASSIFICATIONS2);
        $req = site::$s->q("SELECT id FROM types WHERE name='$classificationName'");
        if (num($req) == 0) {
            throw new \myException("This classification '$classificationName' doesn;t exist");
        }
        $res = f($req);
        $this->classificationTypeID = $res['id'];

        $classifications = \CORE\globalData::$siteConfig->website->classifications;
        foreach ($classifications as $classification) {
            if ($classification->typeID == $this->classificationTypeID) {
                $this->websiteClassifications[] = $classification;
            }
        }

        site::$s->setDB(DB_WEBSITES2);
        $req = site::$s->q("SELECT CO.id "
                . "FROM websiteClassificationOrigins WCO "
                . "INNER JOIN " . DB_CLASSIFICATIONS2 . ".classificationOrigins CO ON CO.id=WCO.classificationOriginID "
                . "WHERE CO.typeID=$this->classificationTypeID");
        if (num($req) == 0) {
            throw new \myException("This website don't have any classificationOriginID associated with the typeID $this->classificationTypeID");
        }
        $res = f($req);
        $this->classificationOriginID = $res['id'];
    }

    private function getUrl($classification, $search) {
        $nbJobs = $search->nbResults($classification);
        
        
        if ($nbJobs > 0) {

            $url = new \stdClass();
            $url->href = \URL_search([ $classification])->url;

            $name = $this->nameFormat;
            $name = str_replace("%NAME%", $classification->lastName, $name);
            $url->name = str_replace("%NB%", $nbJobs, $name);

            $url->nbJobs = $nbJobs;
            $url->children = [];

            return $url;
        }
        else{
            return false;
        }
    }

    public function preProcess() {
            $this->setClassificationType();
            $this->showChildren = $this->getOption("showChildren");
            $this->title = $this->getOption("title");
            $this->nameFormat = $this->getOption("nameFormat");
            
            $urls = [];

            
            
            $search = new \JOBSEARCH\COMPONENT\search();

            if (count($this->websiteClassifications) == 0) {
                //get all teh classifications lvl 0 for this website's origin
                site::$s->setDB(DB_CLASSIFICATIONS2);
                $req = site::$s->q("SELECT C.id "
                        . "FROM classifications C "
                        . "INNER JOIN classificationNames N ON N.id=C.id "
                        . "WHERE C.depth=0 AND N.languageID=1 AND C.classificationOriginID=$this->classificationOriginID "
                        . "ORDER BY N.name ASC");
                while ($res = f($req)) {
                    $classification1 = new \JOBSEARCH\COMPONENT\classificationP('scratch', $res['id']);
                    $url = $this->getUrl($classification1, $search);
                    if($url!==false){
                        $urls[] = $url;
                    }
                }
            } else {

                

                foreach ($this->websiteClassifications as $classification) {
                    site::$s->setDB(DB_CLASSIFICATIONS2);
                    $req = site::$s->q("SELECT id FROM classifications WHERE parentID=" . $classification->id);

                    while ($res = f($req)) {

                        $classification1 = new \JOBSEARCH\COMPONENT\classificationP('scratch', $res['id']);

                        $url = $this->getUrl($classification1, $search);
                        if($url!==false){
                            $urls[] = $url;
                        }
                    }
                }
            }

            $this->nbColumns = $this->getOption("nbColumns");
            $nbUrlsPerColumns = ceil(count($urls) / $this->nbColumns);

            $i = 0;
            for ($c = 1; $c <= $this->nbColumns; $c++) {
                $this->urls[$c] = [];
                $counter = 0;
                while ($counter <= $nbUrlsPerColumns && isset($urls[$i])) {
                    $this->urls[$c][] = $urls[$i];
                    $i++;
                    $counter++;
                }
            }
        }

    }
    