<?
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
namespace JOBSEARCH\WIDGET;

class freeText extends \CORE\TEMPLATE\widget {

    public $text;
    public $nbJobs;
    public function preProcess() {
        $this->text = $this->getOption("text");
        if(strpos($this->text, "%NB")!==false){
            $search = new \JOBSEARCH\COMPONENT\search();
            $this->nbJobs = $search->getNbTotalJobs();
            $this->text = str_replace("%NB", $this->nbJobs, $this->text);
        }
    }

}

?>
