<?
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
namespace JOBSEARCH\WIDGET;

class backSearchResultsLink extends \CORE\TEMPLATE\widget{

    public $iconPosition;
    
    public $iconSrc=false;
    
    public $backUrl;
    
    public $iconWidth;
    
    public $iconHeight;
    
    public $linkText;
    
    
    private function getDevSrc(){
        $option = \MODELS\WEBSITES\widgetOptionsQuery::create()->filterBywidgetThemeID($this->widgetTheme->getid())->filterByoptionName("icon")->findOne();
        if($option==null){
            return false;
        }

        $option = \MODELS\WEBSITES\containerWidgetOptionsQuery::create()->filterBycontainerWidgetID($this->containerWidget->getid())
                ->usewidgetOptionsQuery()
                    ->filterByoptionName("icon")
                ->endUse()
                ->findOne();

        if($option == null){
            return false;
        }
        return  "/assets/images/".$option->getid()."_".$option->getvalue();
    }
    
    /**
     * 
     * @param \MODELS\WEBSITES\containerWidgetOptions $filename
     */
    private function getProdSrc($filename){
        $websiteOption = \MODELS\WEBSITES\optionsQuery::create()->findPk($this->getWebsiteID());
        return  $websiteOption->getimgFolderName().$filename->getvalue();
    }
    
    public  function preProcess() {
        switch(DEVMODE){
            case "development" : 
                $this->iconSrc =$this->getDevSrc();
                break;
            default : 
                $filename = \MODELS\WEBSITES\containerWidgetOptionsQuery::create()->filterBycontainerWidgetID($this->containerWidget->getid())
                    ->usewidgetOptionsQuery()
                        ->filterByoptionName("icon")
                    ->endUse()
                    ->findOne();
                if($filename ==null){
                    $this->iconSrc=$this->getDevSrc();
                }
                else{
                    $this->iconSrc = $this->getProdSrc($filename);
                }
                break;
        }
        
        $this->iconPosition = $this->getOption("icon-position");
        $this->iconWidth = $this->getOption("icon-width");
        $this->iconHeight = $this->getOption("icon-height");
        $this->linkText = $this->getOption("link-text");
        
        if(\CORE\site::$registry->has("history")){
            
            $history = \CORE\site::$registry->history;
            $jobID = \CORE\site::$registry->job->id;
            if(isset($history[$jobID])){
                $this->backUrl = $history[$jobID];
            }
        }
    }

    public static function RENDER($widgetThemeID){
        parent::RENDER($widgetThemeID);
    }

}

?>