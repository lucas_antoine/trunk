<?php
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
namespace JOBSEARCH\WIDGET;

class latestJobs extends \CORE\TEMPLATE\widget {

    public $links;
    public $nbJobs;
    public $totalJobs;
    public $title;
    public $todayFormat,$dateFormat,$noCompanyText;

    public  function preProcess() {
        $this->nbJobs = (int)$this->getOption("nbJobs");

        $this->title = $this->getOption("title");
        $this->todayFormat = $this->getOption("todayFormat");
        $this->dateFormat = $this->getOption("dateFormat");
        $this->noCompanyText = $this->getOption("noCompanyText");


        $search = new \JOBSEARCH\COMPONENT\search();
        $sphinx = $search->latestJobs($this->nbJobs);

        $this->links = $search->resultList;
        $this->totalJobs = $search->getNbTotalJobs();

        $this->title=str_replace("%NB", $this->totalJobs, $this->title);
    }

}

?>
