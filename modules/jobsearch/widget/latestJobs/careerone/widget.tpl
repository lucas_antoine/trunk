<?
if (count($links) > 0) {
    ?>
    <p><?= $title; ?></p>

    <div class="resultSearch">
        <ol>
            <?php
            foreach ($links as $job) {
                ?>
                <li>
                    <dl >
                        <dd class='title'>
                            <h2>
                                <a  title="<?= $job->getTitle(); ?>"  href="<?= $job->getURL()->url; ?>">
                                    <?= $job->getTitle(); ?></a>
                            </h2>
                            <span class="location">
                                <?= $job->location[0]->lastName; ?>
                            </span>

                        </dd>
                        <dd class ="company" >
                            <?
                            if ($job->getCompanyName() == "") {
                                echo $noCompanyText;
                            } else {
                                echo $job->getCompanyName();
                            }
                            ?>
                        </dd>
                        <dd class="date">
        <?
        if ($job->isPostedToday()) {
            echo $job->getPostedDate()->format($todayFormat);
        } else {
            echo $job->getPostedDate()->format($dateFormat);
        }
        ?>
                        </dd>
                    </dl>
                </li>
        <?
    }
    ?>
        </ol>
    </div>

<? } ?>


