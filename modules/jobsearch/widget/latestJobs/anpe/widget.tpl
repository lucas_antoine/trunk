<?
if (count($links) > 0) {
    ?>
    <p><?= $title; ?></p>

    <table border=0 cellpadding="5">
        <?php
        foreach ($links as $job) {
            ?>
            <tr>
                <td class="c1">
                    <h2>
                        <a href="<?= $job->getURL()->url; ?>"><?= $job->getTitle(); ?></a>
                    </h2>
                    <div class="infos">
                        <?= $job->employmentType[0]->lastName; ?>
                        <?
                        if ($job->getCompanyName() != "") {
                            echo " | " . $job->getCompanyName();
                        }
                        ?>
                    </div>
                    <div class="description">
                        <?= $job->getShortDescription(70); ?>
                    </div>

                </td>
                <td class="c2">
                    <?= $job->location[0]->lastName; ?>
                </td>
                <td class="c3">
                    <?= $job->employmentType[0]->lastName; ?>
                </td>
                <td class="c4">
                    <?
                    if ($job->isPostedToday()) {
                        echo $job->getPostedDate()->format($todayFormat);
                    } else {
                        echo $job->getPostedDate()->format($dateFormat);
                    }
                    ?>
                </td>
            </tr>

            <?
        }
        ?>
    </table>

    <?
}
?>
