<?php
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
namespace JOBSEARCH\WIDGET;

use JOBSEARCH\COMPONENT\classificationCode;
use CORE\site;

class refine extends \CORE\TEMPLATE\widget {

    public $refineData = [];

    /**
     *
     * @var JOBSEARCH\WIDGET\tree[]
     */
    public $trees;
    /**
     *
     * @var string
     */
    public $typeName;
    
    private $dd = false;

    

    /**
     * get the Min Depth for the given classification Type
     * @param int $typeID
     * @return int
     */
    private function getWebsiteDepth($typeID) {

        if (\CORE\globalData::$siteConfig->website->classifications == null) {
            return 0;
        }
        
        $minDepth = [];
        foreach (\CORE\globalData::$siteConfig->website->classifications as $classification) {
            if ($classification->typeID == $typeID){
                if(!isset($minDepth[$classification->depth])){
                    $minDepth[$classification->depth] = 1;
                }
                else{
                    $minDepth[$classification->depth]++;
                }
            }
        }
        if(count($minDepth)==0){
            return 0;
        }
        ksort($minDepth);
        reset($minDepth);
        $depth = key($minDepth);
        //var_dump($minDepth);
        $nb = $minDepth[$depth];
        $min = $nb>1 ? ($depth) : ($depth+1);
        
        $this->debug("Min Depht calculated : $min");
        return $min;
    }
    
    private function debug($str){
        if($this->dd){
            echo "$str \n<br />";
        }
    }
    private function dump($data){
        if($this->dd){
            echo var_dump($data);
        }
    }

    public function preProcess() {
        
        
        \CORE\site::$s->setDB(DB_CLASSIFICATIONS2);
        $req = \CORE\site::$s->q("SELECT * from types where id in (1,2,5)");
        
        while($re=f($req)){  
            
            $typeID = $re['id'];
            
            
            $tree = new tree();
            $tree->typeName = $re['name'];
            $this->debug("<br /><br />Current Type : $tree->typeName");

            $sphinx = \CORE\site::$registry->sphinxClient;

            $sphinx2 = new \CORE\sphinxClient();
            $sphinx2->setLimits(0, 20, 20);
            $sphinx2->setServer(SPHINX_SEARCH_IP, \CORE\globalData::$siteConfig->website->sphinxPort);
            $sphinx2->loadWebsiteFilters();

            \CORE\site::$s->setDB(DB_CLASSIFICATIONS2);
            //get the current filter value
            $currentFilter = null;
            $otherFilters = [];

            //get current filter        
            $data = isset($sphinx->dataS[$typeID]) ? $sphinx->dataS[$typeID] : null;

            $this->debug("Data extracted from dataS (previous searchs");
            


            if ($data != null) {
                $currentClassificationID = array_pop($data['ids']);
                $tree->current = new \JOBSEARCH\COMPONENT\classificationP("scratch", $currentClassificationID);
                $currentDepth = $data['depth'];
            } else {
                $currentClassificationID = null;
                $currentDepth = null;
            }

            //set search parameters

            foreach ($sphinx->dataS as $ntypeID => $data) {
                $sphinx2->addClassificationFilter($data['ids'], $ntypeID, $data['depth']);
            }


            //now create the filter range
            //depth is used for the refine
            if($currentDepth == null){

                $depth = $this->getWebsiteDepth($typeID);
                $this->debug("Depth Is null : depth = $depth");
            }
            else{
                $depth = $currentDepth+1;
                $this->debug("Depth Is NOT null : depth = $depth");
            }
            
            switch($typeID){
                case 1 : 
                    $sphinx2->setGroupBy("locations_" . $depth , SPH_GROUPBY_ATTR, "@count desc");
                    break;
                case 2 : 
                    $sphinx2->setGroupBy("industry_" . $depth , SPH_GROUPBY_ATTR, "@count desc");
                    break;
                case 3 : 
                    $sphinx2->setGroupBy("employmentType", SPH_GROUPBY_ATTR, "@count desc");
                    break;
            }

            
            $sphinx2->setSelect("*,@count, $depth as level");

            
            $res = \CORE\site::sphinxExec($sphinx2, $sphinx->q, $sphinx->index);
            
            //if an error was found we throw an exception 
            if ($res['error'] != 0) {
                throw new sphinxException("a sphinx exception has just occured : " . var_export($res, true));
            }
            if($sphinx2->_error != 0){
                throw new sphinxException("a sphinx exception has just occured : " . var_export($sphinx, true));
            }

            $nbCurrent = 0;
            $refineList = [];

            if ($res["total_found"] > 0) {

                //rebuild the classification data for the URL
                $objects = [];
                if (GET__JOBSEARCH_COMPONENT_classificationP() != null) {
                    foreach(GET__JOBSEARCH_COMPONENT_classificationP() as $classification){
                        if($classification->typeID != $typeID){
                            $objects[] = $classification;
                        }
                    }
                    //$objects = GET__JOBSEARCH_COMPONENT_classificationP();
                }
                /*
                  foreach($sphinx->dataS as $ntypeID=>$data){
                  if($ntypeID!=$typeID){
                  $objects[] = new \JOBSEARCH\COMPONENT\classificationP("scratch", array_pop($data['ids']));
                  }
                  } */

                if (GET__JOBSEARCH_COMPONENT_keyword() !== null) {
                    $data = array_merge($objects, [GET__JOBSEARCH_COMPONENT_keyword()]);
                }


                foreach ($res["matches"] as $match) {
                    if ($currentClassificationID != null && $match["attrs"]["@groupby"] == $currentClassificationID) {
                        $tree->currentNB = $match["attrs"]["@count"];
                    } else {

                        if(count($tree->children)<NB_REFINE_ITEMS){
                            $c = $match["attrs"]["@groupby"];

                            $classification = new \JOBSEARCH\COMPONENT\classificationP("scratch", $c);
                            $tree->children[$classification->id] = $classification;
                            $tree->childrenNB[$classification->id] = $match["attrs"]["@count"];

                            $data = array_merge($objects, [$classification]);
                            
                            $tree->urls[$classification->id] = URL_search($data);
                        }
                    }
                }
            }

            $wDepth = $this->getWebsiteDepth($typeID);
            if ($tree->current != null && $tree->current->depth > 0 && $tree->current->depth > $wDepth) {
                $tree->top = $tree->current->getParent();
            }
            
            $this->trees[]=$tree;
        }
        
    }

    public static function RENDER($localData) {
        parent::RENDER($localData);
    }

}

class tree {
    /**
     *
     * @var string
     */
    public $typeName;
    /**
     *
     * @var \JOBSEARCH\COMPONENT\classificationP
     */
    public $top = null;

    /**
     *
     * @var int
     */
    public $topNB = 0;

    /**
     *
     * @var \JOBSEARCH\COMPONENT\classificationP
     */
    public $current = null;

    /**
     *
     * @var int
     */
    public $currentNB = 0;

    /**
     *
     * @var \JOBSEARCH\COMPONENT\classificationP[]
     */
    public $children = [];

    /**
     *
     * @var int[], key is the classificationP id
     */
    public $childrenNB = [];

    /**
     *
     * @var \CORE\URL\url[]  with classificationID as a key
     */
    public $urls = [];

    public function order() {
        krsort($this->children());
    }

}

?>
