
<?
foreach($trees as $tree){
    if(count($tree->children)!=0){    
    ?>
    <span class='refineTitle'>
        Refine by <?= $tree->typeName ?>
    </span>
    <hr/>
    <ul>
        <?
        foreach($tree->children as $id=>$classification){
            $urlStr = $tree->urls[$id]->render($classification->lastName);
            echo "
            <li>$urlStr <span class='number'> (".$tree->childrenNB[$id].")</span></li>
            ";
        }
        ?>
    </ul>
    <br /><br />
    <?
    }
}
?>
