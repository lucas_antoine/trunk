<?
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
namespace JOBSEARCH\WIDGET;

class ad extends \CORE\TEMPLATE\widget{

    public $dimension;

    public $width;
    public $height;

    public  function preProcess() {
        $this->dimension = $this->getOption("dimension");
        switch($this->dimension){
            case "336x280 Large Rectangle" :
                $this->width = 336;
                $this->height = 280;
                break;
            case "300x250 Medium Rectangle" :
                $this->width = 300;
                $this->height = 250;
                break;
            case "728x90 Leaderboard" :
                $this->width = 728;
                $this->height = 90;
                break;
            case "160x600 Wide Skyscraper" :
                $this->width = 160;
                $this->height = 600;
                break;
        }
    }

}

?>