<?php
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
namespace JOBSEARCH\WIDGET;

class jobDetails extends \CORE\TEMPLATE\widget {

    public $job;
    public $dateFormat;
    public $target;
    
    
    public $locationText,$industryText,$employmentTypeText, $applyText, $descriptionText, $companyText, $noCompanyText, $postedDateText;
    
    public function preProcess() {

        $this->job = \CORE\site::$registry->job;
        $this->dateFormat = $this->getOption("dateFormat");
        $this->target = $this->getOption("target");
        
        $this->locationText = $this->getOption("locationText");
        $this->industryText = $this->getOption("industryText");
        $this->employmentTypeText = $this->getOption("employmentTypeText");
        $this->applyText = $this->getOption("applyText");
        $this->descriptionText = $this->getOption("descriptionText");
        $this->companyText = $this->getOption("companyText");
        $this->noCompanyText = $this->getOption("noCompanyText");
        $this->postedDateText = $this->getOption("postedDateText");
        
    }


}

?>
