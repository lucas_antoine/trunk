<div class="job">
    <div class="title">
        <?=$job->getTitle();?>
    </div>
    <div class="date">
        <div class="date2">
            <span class="s1">Updated : </span>
            <span class="s2"><?=$job->getPostedDate()->format($dateFormat);?></span>
        </div>
    </div>
    <div style="clear:both"></div>
    <div class="info">
        <div class="key"><?=$locationText;?></div>
        <div class="value"><?=$job->location[0]->lastName;?></div>
    </div>
    <div class="info">
        <div class="key"><?$employmentTypeText;?></div>
        <div class="value"><?= isset($job->employmentType[0]) ? $job->employmentType[0]->lastName : '';?></div>
    </div>
    <div class="info">
        <div class="key"><?=$industryText;?></div>
        <div class="value"><?= isset($job->industry[0]) ? $job->industry[0]->lastName : '';?></div>
    </div>
    
    <div style="clear:both"></div>
    <br />
    <div class="description">
        <?=$job->getDescription();?>
    </div>
    <br /><br />
    <form action="<?=$job->getRedirectURL()->url;?>" method="get" target="<?=$target;?>" style="float : right">        
        <button  style="float : right" type="submit"><?=$applyText;?></button>        
    </form>
    

</div>
