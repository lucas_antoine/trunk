<h1><?=$job->getTitle();?></h1>

<hr class='hr1' />

<h2><?=$job->getCompanyName();?></h2>

<div class='title'><?=$descriptionText;?></div>
<hr class='hr2' />
<div class='jobDescription'><?=$job->getDescription();?></div>
<br />
<div class='title'>Details</div>
<hr class='hr2' />
<span class='key'>
<?=$locationText;?>  
</span>
<span class='value'>
 <?=$job->location[0]->lastName;?>
</span>
<div style="clear:both"></div>
<span class='key'>
<?=$companyText;?> 
</span>
<span class='value'>
 <?= $job->getCompanyName()=="" ? "Private advertiser" : $job->getCompanyName();?>
</span>
<div style="clear:both"></div>
<span class='key'>
<?=$employmentTypeText;?>  
</span>
<span class='value'>
 <?=$job->employmentType[0]->lastName;?>
</span>
<div style="clear:both"></div>
<span class='key'>
<?=$postedDateText;?>  
</span>
<span class='value'>
 <?= $job->getPostedDate()->format($dateFormat);?>
</span>
<div style="clear:both"></div>
<span class='key'>
<?=$industryText;?> 
</span>
<span class='value'>
 <?=$job->industry[0]->lastName;?>
</span>
<div style="clear:both"></div>
<br />

<div class='title'>How to apply</div>
<hr class='hr2' />
To apply, please click here : <a class="apply" href="<?$job->getRedirectURL()->url;?>" target="<?=$target;?>"><?=$applyText;?></a>