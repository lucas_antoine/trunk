<div>
    <p class="id"><?= $job->location[0]->longName; ?></p>
    ID : <?=$job->id;?>
    <p class="title">
        <?= $job->getTitle(); ?>
    </p>
    <p class="description">
        <?= $job->getDescription(); ?>
    </p>
    <form action="<?= $job->getRedirectURL()->url; ?>" method="get" target="<?= $target; ?>" style="float : right">
        <button  type="submit"><?= $applyText; ?></button>
    </form>
</div>