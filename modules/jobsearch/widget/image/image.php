<?php
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
namespace JOBSEARCH\WIDGET;

class image extends \CORE\TEMPLATE\widget{

    public $src;
    public $isLogo;
    public $alt;

    public  function preProcess(){
        $this->src = $this->getImageSrc("image","filename");
        $this->alt = $this->getOption("alt");
        $this->isLogo = $this->getOption("isLogo");
        $this->isLogo = $this->isLogo=="1" ? true : false;
    }

}
?>
