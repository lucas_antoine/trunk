<div class="infos">
    <div class='date'><?=$job->getPostedDate()->format($dateFormat);?></div>
    <div class="info"><?=$locationText;?></div>
    <div class="data"><?=$job->location[0]->lastName;?></div>
    <div style="clear:both"></div>
    <div class="info"><?=$industryText;?></div>
    <div class="data"><?=$job->industry[0]->lastName;?></div>
    <div style="clear:both"></div>
    <?if(isset($job->employmentType[0])){?>
    <div class="info"><?=$employmentTypeText;?></div>
    <div class="data"><?=$job->employmentType[0]->lastName;?></div>
    <div style="clear:both"></div>    
    <br /><br />
    <form action="<?=$job->getRedirectURL()->url;?>" method="get" target="<?=$target;?>">
        <center>
            <button type="submit"><?=$applyText;?></button>
        </center>
    </form>
    <?}?>
</div>