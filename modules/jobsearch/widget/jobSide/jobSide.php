<?php
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
namespace JOBSEARCH\WIDGET;

class jobSide extends \CORE\TEMPLATE\widget {

    public $job;
    public $dateFormat;
    public $target;

    public $backUrl=false;

    public $locationText,$industryText,$employmentTypeText, $applyText, $companyText, $noCompanyText, $titleText;

    public $locationData;


    public function preProcess() {

        $this->job = \CORE\site::$registry->job;
        $this->dateFormat = $this->getOption("dateFormat");
        $this->target = $this->getOption("target");

        if(\CORE\site::$registry->has("history")){
            $history = \CORE\site::$registry->history;
            if(isset($history[$this->job->id])){
                $this->backUrl = $history[$this->job->id];
            }
        }
        $this->locationData = $this->job->location[0]->getLocationData();

        $this->locationText = $this->getOption("locationText");
        $this->industryText = $this->getOption("industryText");
        $this->employmentTypeText = $this->getOption("employmentTypeText");
        $this->applyText = $this->getOption("applyText");
        $this->companyText = $this->getOption("companyText");
        $this->noCompanyText = $this->getOption("noCompanyText");
        $this->titleText = $this->getOption("titleText");

    }

    public static function RENDER($widgetThemeID) {
        parent::RENDER($widgetThemeID);
    }

}

?>
