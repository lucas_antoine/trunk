<div class="right">
    <h3><?= $titleText; ?></h3>
    <?if($locationData->getlatitude()!=0 || $locationData->getlongitude()!=0){?>
    <div id="map"></div>
    <?}?>
    <br />
    <span class="key"><?= $employmentTypeText; ?></span>
    <span class="value">
        <? if (isset($job->employmentType[0])) { ?>
            <?= $job->employmentType[0]->lastName; ?>
        <? } else { ?>
            Not specified
        <? } ?>
    </span>
    <hr />
    <span class="key"><?= $locationText; ?></span>
    <span class="value">
        <?= $job->location[0]->lastName; ?>
    </span>
    <hr />
    <span class="key"><?= $industryText; ?></span>
    <span class="value">
        <?= $job->industry[0]->lastName; ?>
    </span>
    <hr />
    <span class="key"><?= $companyText; ?></span>
    <span class="value">
        <? if ($job->getCompanyName() != "") { ?>
            <?= $job->getCompanyName(); ?>
        <? } else { ?>
            <?= $noCompanyText; ?>
        <? } ?>
    </span>
    <hr />
    <form action="<?= $job->getRedirectURL()->url; ?>" method="get" target="<?= $target; ?>">
        <center>
            <button type="submit"><?= $applyText; ?></button>
        </center>
    </form>



</div>
<script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script>
    <?if($locationData->getlatitude()!=0 || $locationData->getlongitude()!=0){?>
    function initialize() {
        var map_canvas = document.getElementById('map');
        var map_options = {
            center: new google.maps.LatLng(<?= $locationData->getlatitude(); ?>, <?= $locationData->getlongitude(); ?>),
            zoom: 8,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        var map = new google.maps.Map(map_canvas, map_options)
    }
    google.maps.event.addDomListener(window, 'load', initialize);
    <?}?>
</script>

