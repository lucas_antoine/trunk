<?php
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
namespace JOBSEARCH\COMPONENT;

use CORE\site;

class elementP{
    
    
    /**   @var int   **/
	public $id;
        /** @var string **/
	public $name;
        /**
         *
         * @var string
         */
        public $description;
        /** @var string **/
	public $link;
        /** @var int **/
	public $depth;
        /** @var int **/
	public $parentID ;
        /** @var int */
        public $classificationOriginID;
        
        /**
         *
         * @var int
         */
        public $typeID;

	public function __construct($id=null){
            if($id!=null){
                $this->id=$id;
                $this->loadFromScratch($id);
            }
	}

        public function getParent(){
            if(isset($this->parentID) && $this->parentID!=0){
                return new elementP($this->parentID);
                
            }
            
        }

	public function loadFromScratch($id){
		$this->id=$id;
		\CORE\site::$s->setDB(DB_CLASSIFICATIONS2);
		$req = site::$s->q("SELECT C.parentID,C.depth,C.classificationOriginID, CO.typeID, N.name,D.description 
                    from classifications C 
                    INNER JOIN classificationNames N ON N.id=C.id 
                    INNER JOIN classificationOrigins CO ON CO.id=C.classificationOriginID
                    LEFT JOIN descriptions D ON D.id=C.id AND D.languageID=1 
                    where N.languageID=1 AND C.id=".$this->id);
		if(num($req)==0){
                    var_dump($id);
                    throw new \EX\NotFoundException("The elementP $id is not found");
		}
		$res=f($req);
                if($res['depth']==null){
                    $this->depth = 0;
                    $this->parentID=0;
                }
                else{
                    $this->depth=$res['depth'];
                    $this->parentID=$res['parentID'];
                    $this->classificationOriginID=$res['classificationOriginID'];
                    $this->typeID=$res['typeID'];
                }
                $this->description = (string)$res["description"];
		$this->name = $res['name'];
                

	}

	public static function load($id,$depth,$name,$link=null){
		$p = new elementP();
		$p->id=$id;
		$p->depth=$depth;
		$p->name=$name;
		$p->link=$link;
		return $l;
	}
}
?>
