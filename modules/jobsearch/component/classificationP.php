<?php
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
namespace JOBSEARCH\COMPONENT;

use CORE\site;

class classificationP implements \CORE\URL\urlObject{

    use \TRAITS\formula;
    /**
     *
     * @var int
     */
    public $id;

    /**
     *
     * @var int
     */
    public $depth;

    /**
     *
     * @var \JOBSEARCH\COMPONENT\elementP[]
     */
    public $elements;

    /**
     *
     * @var string
     */
    public $longName;

    /**
     *
     * @var string
     */
    public $shortName;

    /**
     *
     * @var string
     */
    public $lastName;

    /**
     *
     * @var string
     */
    public static $emptyURLString;

    /**
     *
     * @var int
     */
    public  $typeID;

    /**
     *
     * @var int
     */
    public $classificationOriginID;

    /**
     *
     * @var string
     */
    public $url0;

    /**
     *
     * @var string
     */
    public $url1;

    /**
     *
     * @var string
     */
    public $url2;

    /**
     *
     * @var string
     */
    public $url3;

    /**
     *
     * @var string
     */
    public $url4;

    /**
     *
     * @var string
     */
    public $url5;

    public static function createFrom_Pid($id) {

        if (is_numeric($id) && $id > 0) {
            return  new classificationP("scratch",(int)$id);
        }
        return null;
    }

    public static function createFrom_PencodedID($id) {

        if (is_numeric($id) && $id > 0) {
            return  new classificationP("encoded",(int)$id);
        }
        return null;
    }
    /**
     *
     * @return \MODELS\CLASSIFICATIONS\locationData or null if not found
     * @throws \Exception
     */
    public function getLocationData(){
        if($this->typeID!=1){
            throw new \myException("This function is only available for location  : typeID==1");
        }
        $locationData = \MODELS\CLASSIFICATIONS\locationDataQuery::create()->findPk($this->id);

        return $locationData;

    }

    /**
     *
     * @return string
     */
    public function getObjectName(){
        site::$s->setDB(DB_CLASSIFICATIONS2);
        $req = site::$s->q("SELECT * from types where id=".$this->typeID);
        $res=f($req);
        return $res['name'];
    }

    /**
     *
     * @param \SHARED\COMPONENT\website $website
     * @return boolean
     */
    public function belongsToWebsite(\SHARED\COMPONENT\website $website){
        $classifications = $website->classifications;
        if($classifications==null){
            return true;
        }
        $toCompare = [];
        foreach($classifications as $classification){
            if($this->typeID == $classification->typeID){
                $toCompare[]=$classification;
            }
        }
        if(count($toCompare)==0){
            return true;
        }
        else{
            foreach($toCompare as $classification){
                if($classification->depth> $this->depth){
                    return false;
                }
                else{
                    if($this->elements[$classification->depth]->id == $classification->id){
                        return true;
                    }
                }
            }
        }
        return false;
    }


    /**
     *
     * @param [type]] $data
     * @param int $classificationOriginID
     * @return \JOBSEARCH\COMPONENT\classificationP
     */
    public static function createFrom_Name($data, $classificationOriginID) {
        return new classificationP("fromName", $data, $classificationOriginID);
    }
    
    public static function getMinWebsiteDepth($classificationOriginID){
        site::$s->setDB(DB_WEBSITES2);
        $req = site::$s->q("SELECT MIN(C.depth) as depth "
                . "FROM websiteClassifications WC "
                . "INNER JOIN ".DB_CLASSIFICATIONS2.".classifications C ON C.id=WC.classificationID "
                . "WHERE C.classificationOriginID=$classificationOriginID AND WC.websiteID=".$_SERVER['ID']);
        if(num($req)==0){
            return null;
        }
        $res=f($req);
        return $res['depth'];
    }

    /**
     * 
     * @param string $locationName1
     * @param int $classificationOriginID Was urlData / $urlData
     * @return null
     * @throws \myException
     */
    public static function createFrom_MgetUrlPart1($locationName1, $classificationOriginID = 0) {
        if($locationName1==null){
            return null;
        }
        if($classificationOriginID==0){
            throw new \myException();
        }
        
        $minDepth = self::getMinWebsiteDepth($classificationOriginID);
        
        switch($minDepth){
            case null : 
                $depth = [0,1];
                break;
            case 0 :
                $depth = [1];
                break;
            case 1 : 
                $depth = [2];
                break;
            case 2 : 
                $depth = [3];
                break;
            case 3 : 
                $depth = [4];
                break;
            default : 
                throw new \Exception("This is a very very narrow website definition.");
        }
        
        
        return self::createFrom_Name([$locationName1=>$depth], $classificationOriginID);
    }

    public static function createFrom_MgetUrlPart1_MgetUrlPart2($locationName1, $locationName2=null, $classificationOriginID = 0 ) {
        if(is_numeric($locationName2) && $classificationOriginID==0){
            //more likelly a bug
            $classificationOriginID = $locationName2;
            $locationName2=null;
        }
        if($classificationOriginID==0){
            var_dump($_GET);
            echo "<pre>";
            debug_print_backtrace();
            echo "</pre>";
            throw new \myException();
        }
        
        
        site::$s->setDB(DB_WEBSITES2);
        $req = site::$s->q("SELECT MIN(C.depth) as depth "
                . "FROM websiteClassifications WC "
                . "INNER JOIN ".DB_CLASSIFICATIONS2.".classifications C ON C.id=WC.classificationID "
                . "WHERE C.classificationOriginID=$classificationOriginID AND WC.websiteID=".$_SERVER['ID']);
        
        if(num($req)==0){
            $depth1 = [0,1];
            $depth2 = [2,3,4,5];
        }
        else{
            $res=f($req);
            switch($res['depth']){
                case 0 :
                    $depth1 = [1];
                    $depth2 = [2,3,4,5];
                    break;
                case 1 : 
                    $depth1 = [2];
                    $depth2 = [3,4,5];
                    break;
                case 2 : 
                    $depth1 = [3];
                    $depth2 = [4,5];
                    break;
                case 3 : 
                    $depth1 = [4];
                    $depth2 = [5];
                    break;
                default : 
                    throw new \Exception("This is a very very narrow website definition.");
            }
        }
        
        
        
        if($locationName2==null){
            return self::createFrom_MgetUrlPart1($locationName1, $classificationOriginID);
        }
        else{
            return self::createFrom_Name([$locationName1 => $depth1 , $locationName2 => $depth2 ], $classificationOriginID);
        }
    }

    public static function createFrom_PlastName($lastName, $classificationOriginID = 0){
        if($lastName==null){
            return null;
        }
        if($classificationOriginID==0){
            throw new \myException();
        }
        return self::createFrom_Name([$lastName=>[0,1,2,3,4,5]], $classificationOriginID);
    }


    /**
     * Generate the short name, spearetd by ',', order by depth DESC
     */
    public  function generateShortName(){
        $names = array();
        foreach ($this->elements as $element) {
            $names[] = $element->name;
        }
        krsort($names);
        if(count($names)==1){
            $this->shortName = $names[0];
        }
        else{
            $this->shortName = $names[0].", ".$names[1];
        }
    }

    /**
     * Generate the long name, spearetd by ',', order by depth DESC
     */
    public function generateLongName() {
        $names = array();
        foreach ($this->elements as $element) {
            $names[] = $element->name;
        }
        krsort($names);
        $this->longName = implode(", ", $names);
    }

    /**
     * generate The lastname
     */
    public function generateLastName() {
        if (is_null($this->depth)) {
            $this->lastName = static::$emptyURLString;
        } else {
            $this->lastName = $this->elements[$this->depth]->name;
        }
    }

    /**
     * @return classificationP[]
     */
    public function getChilds() {
        $childs = array();
        $c = new \ReflectionClass(get_called_class());
        site::$s->setDB(DB_CLASSIFICATIONS2);
        $req = site::$s->q("SELECT id from classifications where parentID=" . $this->id);
        while ($res = f($req)) {
            $childs[] = new $c->newInstanceArgs(["scratch", $res['id']]);
        }
        usort($childs, function($a, $b) {
                    return strcmp($a->lastName, $b->lastName);
                });
        return $childs;
    }

    /**
     *
     * @return \JOBSEARCH\COMPONENT\classificationP
     * @throws \Exception
     */
    public function getParent(){
        if($this->depth ==0 ){
            throw new \myException("Can't get any parent. Top level");
        }
        else{
            $parentID = $this->elements[$this->depth]->parentID;
            return self::createFrom_Pid($parentID);
        }
    }

    private function setName($depth) {
        $var = "url" . $depth;
        if ($this->depth >= $depth) {
            $this->$var = $this->elements[$depth]->name;
        } else {
            $this->$var = static::$emptyURLString;
        }
        //$this->$var = str_replace(' ', \CORE\environment::$config->url->space, $this->$var);
    }

    /**
     *
     * @throws \EXCEPTIONS\NotFoundException  When the language is not defined
     */
    public function generateURLNames() {
        $this->setName(0);
        $this->setName(1);
        $this->setName(2);
        $this->setName(3);
        $this->setName(4);
        $this->setName(5);
    }

    
    public function getUrlData(){
        return $this->classificationOriginID;
    }
    
    

    public function getUrlPart1(){
        $minDepth = self::getMinWebsiteDepth($this->classificationOriginID);
        
        switch($minDepth){
            case null : 
                if($this->depth==0){
                    return $this->url0;
                }
                else{
                    return $this->url1;
                }         
                break;
            case 0 :
                return $this->url1;                
                break;
            case 1 : 
                return $this->url2;
                break;
            case 2 : 
                return $this->url3;
                break;
            case 3 : 
                return $this->url4;
                break;
            default : 
                throw new \Exception("This is a very very narrow website definition.");
        }
    }

    public function getUrlPart2(){
        $minDepth = self::getMinWebsiteDepth($this->classificationOriginID);
        
        switch($minDepth){
            case null : 
            case 0 :
                if($this->depth<2){
                    return false;
                }
                else{
                    return $this->lastName;
                }
                break;
            case 1 : 
                if($this->depth<3){
                    return false;
                }
                else{
                    return $this->lastName;
                }
                
                break;
            case 2 : 
                if($this->depth<4){
                    return false;
                }
                else{
                    return $this->lastName;
                }
                break;
            case 3 : 
                if($this->depth<5){
                    return false;
                }
                else{
                    return $this->lastName;
                }
                break;
            default : 
                throw new \Exception("This is a very very narrow website definition.");
        }
        
    }

    private function strdecode($name) {
        return html_entity_decode($name, ENT_QUOTES, "UTF-8");
    }

    private function strencode($name) {
        return htmlentities($name, ENT_QUOTES, "UTF-8");
    }

    /**
     *
     * @param [] $nameData is an array of name binding
     * Structure is as followed  :
     *      [string Name1] => int [ possible Depths] ,
     *      [string Name2] => int [ possible Depths]
     * @param int classificationOriginID
     *
     * @throws \EX\NotFoundException
     */
    public function loadFromName($nameData, $classificationOriginID) {
        site::$s->setDB(DB_CLASSIFICATIONS2);
        $this->classificationOriginID=$classificationOriginID;
        site::$s->setDB(DB_CLASSIFICATIONS2);
        $req = site::$s->q("SELECT typeID FROM classificationOrigins WHERE id=$this->classificationOriginID");
        if(num($req)==0){
            throw new \LogicException();
        }
        $res=f($req);
        $this->typeID = $res['typeID'];
        
        $filters = [];
        $websiteClassifications = \CORE\globalData::$siteConfig->website->classifications;
        foreach($websiteClassifications as $classification){
            if($classification->typeID == $this->typeID){
                $filters[] = $classification;
            }
        }
        
        $levelConditions = [];
        if(count($filters)>0){
            foreach($filters as $classification){
                $levelConditions[] = "L.level".$classification->depth."=".$classification->id;
            }
        }
        
        //checknameData validity
        foreach ($nameData as $i => $datai) {
            foreach ($nameData as $j => $dataj) {
                if ($i != $j && count(array_intersect($datai, $dataj)) > 0) {
                    throw new Exception("Depths must never interstect");
                }
            }
        }
        if (count($nameData) == 0) {
            throw new Exception('the parameter is an empty array');
        }
        /**
         * @todo IMplement Sphinx here to replace the LIKE, and improve performances
         */
        if (count($nameData) == 1) {
            foreach ($nameData as $i => $data) {
                $i = $this->strencode($this->strdecode($i));
                
                $sql = "SELECT C.id FROM  classifications C
                    INNER JOIN classificationNames N ON N.id=C.id AND N.languageID=1 
                    INNER JOIN levels L ON L.id=C.id 
                    INNER JOIN classificationOrigins CO ON CO.id=C.classificationOriginID 
                    WHERE
                    C.classificationOriginID = $this->classificationOriginID";
                if (count($data) > 0) {
                    $sql.=" AND C.depth IN (" . implode(",", $data) . ")";
                }
                $sql.=" AND N.name LIKE \"$i\"";
                
                if(count($levelConditions)>0){
                    $sql.=" AND ( ".implode(" OR ", $levelConditions)." )";
                }
            }
        } else {
            //get depth master data
            $maxIndex = null;
            $maxValue = -1;
            foreach ($nameData as $i => $datai) {
                foreach ($datai as $int) {
                    if ($int > $maxValue) {
                        $maxIndex = $i;
                    }
                }
            }
            if ($maxIndex == null) {
                throw new \myException("investigate here");
            }
            $maxIndex = $this->strencode($this->strdecode($maxIndex));


            $from = 'FROM levels L '
                    . 'INNER JOIN classifications C ON C.id=L.id  '
                    . 'INNER JOIN classificationNames N ON N.id=C.id AND N.languageID=1';

            if (count($nameData[$maxIndex]) > 0) {
                $where = "\n WHERE C.classificationOriginID=$this->classificationOriginID 
                    AND C.depth IN (" . implode(",", $nameData[$maxIndex]) . ")
                    AND N.name LIKE \"$maxIndex\" ";
            } else {
                $where = "\n WHERE C.classificationOriginID=" . $this->classificationOriginID . "                    
                    AND N.name LIKE \"$maxIndex\" ";
            }
            
            if(count($levelConditions)>0){
                $where.=" AND ( ".implode(" OR ", $levelConditions)." )";
            }
            $where.="ORDER BY C.depth ASC";
            
            
            unset($nameData[$maxIndex]);
            $inner = [];

            $c = 0;
            foreach ($nameData as $i => $data) {
                $i = $this->strencode($this->strdecode($i));

                $sql = "(
	SELECT
		L$c.*,N$c.name
		FROM levels L$c
		INNER JOIN classifications C$c ON C$c.id=L$c.id
		INNER JOIN classificationNames N$c ON N$c.id=C$c.id AND N$c.languageID=1
		WHERE
			C$c.classificationOriginID=" . $this->classificationOriginID;
                if (count($data) != 0) {
                    $sql.=" AND C$c.depth IN (" . \implode(",", $data) . ") ";
                }
                
                $lvlCdt = [];
                
                if(count($filters)>0){
                    foreach($filters as $classification){
                        $lvlCdt[] = "L$c.level".$classification->depth."=".$classification->id;
                    }
                }
                
                if(count($lvlCdt)>0){
                    $sql.=" AND ( ".implode(" OR ", $lvlCdt)." ) ";
                }
                
                
                $sql.=" AND N$c.name LIKE \"$i\" ) AS LL$c ON ";
                
                $ons = [];
                if (count($data) != 0) {
                    foreach ($data as $int) {
                        $ons[] = "L.level$int = LL$c.id";
                    }
                } else {
                    $ons[] = "L.level0 = LL$c.id";
                    $ons[] = "L.level1 = LL$c.id";
                    $ons[] = "L.level2 = LL$c.id";
                    $ons[] = "L.level3 = LL$c.id";
                    $ons[] = "L.level4 = LL$c.id";
                    $ons[] = "L.level5 = LL$c.id";
                }
                $sql.=implode(" OR ", $ons);
                
                
                
                $inner[] = $sql;
            }

            $sql = "SELECT C.id $from INNER JOIN " . implode(" INNER  JOIN ", $inner) . $where;
        }
        
        $req = site::$s->q($sql);
        if (num($req) == 0) {
            var_dump($_REQUEST);
            throw new \NotFoundException("This classification can't be found  . '$sql'");
        }
        $res = f($req);
        $this->loadFromScratch($res['id']);
    }

    /**
     *
     * @param int $locationID The locationID
     * Initialise the location Object, will make up to 5 database calls, depending on the depth.
     */
    public function loadFromScratch($id) {
        if (!is_numeric($id)) {
            error_log(var_export(debug_backtrace(), true), 0);
            throw new \InvalidArgumentException("The id must be a numeric Value : '$id'");
        }
        if ($id === 0) {
            error_log(var_export(debug_backtrace(), true), 0);
            throw new \InvalidArgumentException("You can't provide a id equal to ZERO");
        }
        $this->id = $id;
        $tmp = new \JOBSEARCH\COMPONENT\elementP($id);
        $this->depth = $tmp->depth;
        $this->typeID=$tmp->typeID;
        $this->classificationOriginID = $tmp->classificationOriginID;
        switch ($this->depth) {
            case 0 :
                $this->elements[0] = $tmp;
                break;
            case 1 :
                $this->elements[1] = $tmp;
                $this->elements[0] = $tmp->getParent();
                break;
            case 2 :
                $this->elements[2] = $tmp;
                $this->elements[1] = $tmp->getParent();
                $this->elements[0] = $this->elements[1]->getParent();
                break;
            case 3 :
                $this->elements[3] = $tmp;
                $this->elements[2] = $tmp->getParent();
                $this->elements[1] = $this->elements[2]->getParent();
                $this->elements[0] = $this->elements[1]->getParent();
                break;
            case 4 :
                $this->elements[4] = $tmp;
                $this->elements[3] = $tmp->getParent();
                $this->elements[2] = $this->elements[3]->getParent();
                $this->elements[1] = $this->elements[2]->getParent();
                $this->elements[0] = $this->elements[1]->getParent();
                break;
            case 5 :
                $this->elements[5] = $tmp;
                $this->elements[4] = $tmp->getParent();
                $this->elements[3] = $this->elements[4]->getParent();
                $this->elements[2] = $this->elements[3]->getParent();
                $this->elements[1] = $this->elements[2]->getParent();
                $this->elements[0] = $this->elements[1]->getParent();
                break;
            default :
                throw new Exception("Unknwon depth $this->depth");
        }
    }
    

    /**
     *
     * @param string $source  XML|scratch|encoded|fromName
     * @param mixed $data Is the id when from scratch, or the SimpleXMLElement
     * @param int $classificationOriginID When loading from tname, this field is required
     * Construct the object either from scratch, given the ID, or loaded from the job details XML
     */
    public function __construct($source, $data, $classificationOriginID=0) {
        switch ($source) {
            case "XML" :
                throw new \myException("Not implemented anymore");
                break;
            case "scratch" :
                $this->loadFromScratch($data);
                break;
            case "encoded" :
                $id = $this->formulaDecode($data);
                $this->loadFromScratch($id);
                break;
            case "fromName" :
                if (is_array($data)) {
                    $this->loadFromName($data,$classificationOriginID);
                } elseif (is_string($data)) {
                    $this->loadFromName([$data => []],$classificationOriginID);
                } else {
                    throw new \InvalidArgumentException("This array must contains  2 elements or it must be a single string " . var_export($data, true));
                }
                break;
            default:
                throw new \InvalidArgumentException("THIS CONSTRUCTOR DOES NOT EXISTS " . var_export($data, true));
                break;
        }
        $this->generateURLNames();
        $this->generateLastName();
        $this->generateShortName();
        $this->generateLongName();
        $this->formulaEncode($this->id);
    }


}

?>
