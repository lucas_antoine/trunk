<?php
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
namespace JOBSEARCH\COMPONENT;


use \CORE\site;
use TRAITS\string;

class job {

    use \AUTO\jobClassifications;
    
    use \TRAITS\formula;
    
    use \TRAITS\string;
    
    const descLength = 500;
    
    /**
     *
     * @var int The job ID
     */
    public $id;

    /**
     *
     * @var \JOBSEARCH\COMPONENT\jobStats
     */
    public $stats;

    /**
     * @var expired 
     */
    public $expired;

    /**
     * This is the classification Name used by the URL Builder. takes a random (first) classification property, and set it to its lastname, or siteConfig::$defaultEmptyClassification if NULL
     * @var string
     */
    public $mainClassificationName;
    
    public $isSuspended;
    
    public $urlTitle;
    
    
    
    //PRIVATE    
    private $title;
    
    private $postedDate;
    
    private $modificationDate;
    
    private $expirationDate;
    
    private $HTTPREF;
    
    private $companyName;
    
    private $originalCompanyName;
    
    private $contactEmail;
    
    private $contactName;
    
    private $vacRef;
    
    private $originID;
    
    private $weight;
    
    private $originCompanyName;
    
    private $originBranchName;
    
    private $originLinkedInURL;
    
    private $originLocationName;
    
    private $description;
    
    

   
    
    public static function createFrom_PencodedID($jobID) {
        return new \JOBSEARCH\COMPONENT\job($jobID,true);
    }
    
    public function __construct($jobID, $encoded=false) {
        if($encoded){
            $jobID = $this->formulaDecode($jobID);
        }
        
        if (!is_numeric($jobID) || $jobID == 0) {
            throw new \InvalidArgumentException("The jobID $jobID is invalid - either not numeric or =0");
        }
        $this->id = $jobID;
        
        $this->loadFromDB();
        
        $this->stats = new \JOBSEARCH\COMPONENT\jobStats($this->id);        
        $this->setUrlTitle();
        $this->formulaEncode($this->id);
        $this->loadClassifications();
        
    }
    
    
    
    
    /**
     * 
     * @return \CORE\URL\url
     */
    public function getDetailsLink(){
        return "";
        //return \GENERATED\urlHelper::jobsearchJobDetails(array($this, $this->location));
    }
    

    
   
    /**
     * 
     * @return string
     */
    public function getDescription(){
              
        $string = preg_replace("~(<img\s.*?/>)~ims", "", $this->description);        
        $string = preg_replace("~(\s+)~ims", " ", $string);        
        $string = strip_tags($string, "<table><tbody><td><tr><th><font><ul><li><ol><a>");
        $string = str_ireplace("<br>", "<br />", $string);
        
        return $string;
    }
    
    /**
     * 
     * @param int $length number oif character maximum
     * @return string
     */
    public function getShortDescription($length){
        
        $string = $this->DBDecode( (string) $this->description );
        $string = $this->removeAllSpaces( $this->removeAllHTMLTags( $this->removeUnprintableCharacters( $string )));
        
        
        if(strlen($string) > $length){
            $secondPart = substr($string, $length+1);

            
            if(strpos( $secondPart, " ") !==false){
                return substr($string , 0,$length+  strpos( $secondPart, " ")+2 );
            }
            else{
                return $string;
            }
        }
        else{
            return $string;
        }
    }
    
    public function getDescriptionPart($part){
        switch($part){
            case 1 : 
                if(strlen($this->getDescription())> self::descLength){                    
                    $secondPart = substr($this->getDescription(), self::descLength+1);
                    
                    $breaks = ["<br />", "<br>", "<br/>"];
                    foreach($breaks as $break){
                        if(strpos( $secondPart, $break) !==false && strpos( $secondPart, $break) < self::descLength){
                            return substr($this->getDescription() , 0, self::descLength + strpos( $secondPart, $break)+1 );
                        }
                    }
                    if(strpos( $secondPart, ".") !==false && strpos( $secondPart, ".") < self::descLength){
                        return substr($this->getDescription() , 0,self::descLength +  strpos( $secondPart, ".")+2 );
                    }
                    else{
                        return $this->getDescription();
                    }
                }
                else{
                    return $this->getDescription();
                }
            case 2 :
                if(strlen($this->getDescription())> self::descLength){                    
                    $secondPart = substr($this->getDescription(), self::descLength+1);
                    $breaks = ["<br />", "<br>", "<br/>"];
                    foreach($breaks as $break){
                        if(strpos( $secondPart, $break) !==false && strpos( $secondPart, $break) < self::descLength){
                            return substr($this->getDescription() , self::descLength + strpos( $secondPart, $break)+1 );
                        }
                    }
                    if(strpos( $secondPart, ".") !==false && strpos( $secondPart, ".") < self::descLength){
                        return substr($this->getDescription() ,self::descLength + strpos( $secondPart, ".")+2 );
                    }
                    else{
                        return "";
                    }
                }
                else{
                    return "";
                }
            default : throw new \myException();
        }
    }
    
    /**
     * 
     * @return \DateTime
     */
    public function getPostedDate(){
        return \CORE\date::createFromSQLDateTime((string)$this->postedDate);
    }
    
    /**
     * 
     * @return boolean
     */
    public function isPostedToday(){
        $postedDate = date_format($this->getPostedDate(), 'Y-m-d');        
        $today = date_format(new \DateTime(), 'Y-m-d');
        return $postedDate == $today;        
    }
    /**
     * 
     * @return \DateTime
     */
    public function getModificationDate(){
        return \CORE\date::createFromSQLDateTime((string)$this->modificationDate);
    }
    /**
     * 
     * @return \DateTime
     */
    public function getExpirationDate(){
        if((string)$this->expirationDate!=""){
            return \CORE\date::createFromSQLDate((string)$this->expirationDate);
        }
        else{
            return null;
        }
    }
    
    /**
     * 
     * @return string
     */
    public function getHTTPREF(){
        return $this->HTTPREF;
    }
    
    /**
     * 
     * @return \CORE\URL\url
     */
    public function getURL(){
        return URL_job([$this]);
    }
    
    /**
     * 
     * @return \CORE\URL\url
     */
    public function getRedirectURL(){
        return URL_jobRedirection([$this]);
    }
    
    
    /**
     * 
     * @return string
     */
    public function getContactEmail(){
        return (string)$this->contactEmail;
    }
    
    /**
     * 
     * @return string
     */
    public function getContactName(){
        return (string)$this->contactName;
    }
    
    /**
     * 
     * @return string
     */
    public function getVacRef(){
        return (string)$this->vacRef;
    }
    
    /**
     * 
     * @return int
     */
    public function getOriginID(){
        return (int)$this->originID;
    }
    /**
     * 
     * @return int
     */
    public function getWeight(){
        return (int)$this->weight;
    }
    /**
     * 
     * @return string
     */
    public function getOriginCompanyName(){
        return (string)$this->originCompanyName;
    }
    /**
     * 
     * @return string
     */
    public function getOriginBranchName(){
        return (string)$this->originBranchName;
    }
    /**
     * 
     * @return string
     */
    public function getOriginLinkedInURL(){
        return (string)$this->originLinkedInURL;
    }
    /**
     * 
     * @return string
     */
    public function getOriginLocationName(){
        return (string)$this->originLocationName;
    }
    
    /**
     * @return string
     */
    public function getCompanyName(){
        return ( $this->originalCompanyName!="" && $this->originalCompanyName!=null) ? $this->originalCompanyName : $this->originCompanyName;        
    }
    
    public function getTitle(){
        return $this->title;
    }
    
    
    /********************************   PRIVATE ***************************************/
    
    private function loadFromDB(){
        site::$s->setDB(DB_JOBS);
        $req = site::$s->q("SELECT "
                . "J.*, JP.HTTPREF, JP.companyName as originalCompanyName, JP.contactName, JP.contactEmail, JP.HTTPREF, "
                . "JSu.suspendedDate, JSu.suspendedReasonID, "
                . "C.name as companyName, B.name as branchName, CN.name as branchLocationName, B.linkedInURL, "
                . "JO.unverifiedCompanyWeight as weight  "
                . "FROM jobs J "
                . "INNER JOIN jobsPrivate JP ON JP.id=J.id "
                . "LEFT JOIN jobsSuspended JSu ON JSu.id=J.id "
                . "LEFT JOIN jobsSalary JS ON JS.id=J.id "
                . "LEFT JOIN MARKETING2.origins JO ON JO.id=J.originID "
                . "LEFT JOIN MARKETING2.branches B ON B.id=JO.branchID "
                . "LEFT JOIN MARKETING2.company C ON C.id=B.id "
                . "LEFT JOIN CLASSIFICATIONS2.classificationNames CN ON CN.id=B.locationID AND CN.languageID=1 "
                . "WHERE J.id=$this->id");
        if(num($req)==0){
            throw new \EX\NotFoundException("The job $this->id is not defined in the database");
        } 
        $res=f($req);
        
        $this->title = $this->DBDecode($res['title']);
        
        $this->postedDate= $this->DBDecode($res['postedDate']);
    
        $this->modificationDate= $this->DBDecode($res['modificationDate']);

        $this->expirationDate= $this->DBDecode($res['expirationDate']);

        $this->HTTPREF= $this->DBDecode($res['HTTPREF']);

        $this->companyName= $this->DBDecode($res['companyName']);

        $this->originalCompanyName= $this->DBDecode($res['originalCompanyName']);

        $this->contactEmail= $this->DBDecode($res['contactEmail']);

        $this->contactName= $this->DBDecode($res['contactName']);

        $this->vacRef= $this->DBDecode($res['vacRef']);

        $this->originID= $this->DBDecode($res['originID']);

        $this->weight= $this->DBDecode($res['weight']);

        $this->originCompanyName= $this->DBDecode($res['companyName']);

        $this->originBranchName= $this->DBDecode($res['branchName']);

        $this->originLinkedInURL= $this->DBDecode($res['linkedInURL']);

        $this->originLocationName= $this->DBDecode($res['branchLocationName']);

        $this->description= $this->DBDecode( $this->DBDecode($res['description']) );
        
        $this->stats = new \JOBSEARCH\COMPONENT\jobStats($this->id);
        
        $this->isSuspended = isset($res['suspendedReasonID']) ? true : false;
        
        $this->setUrlTitle($res['title']);                
        
    }
    
    /**
     * @todo double check this function in time
     */
    private function setUrlTitle( $separator="-"){
        $title = substr($this->title, 0, 50);
        $title = str_replace("/", $separator, $title);
        $title = str_replace(" ", $separator, $title);
        $this->urlTitle =  $title;
    }
    
    /**
     *
     * @param string $file The path to the XML FILE cache
     * @throws \EXCEPTIONS\XMLErrorException 
     */
    private function loadXML($file) {
        try{
        	$this->XML = new \SimpleXMLElement($file, null, true);
	}
	catch (\Exception $e){
		//echo $e->getMessage();
		return false;	
	}
        if ($this->XML === false) {
            //throw new \EX\XMLErrorException("The file $file is not a valid XML File");
            return false;
        }
        
        $this->loadClassifications();
        return true;
    }

    
    
    private function loadClassifications() {
        site::$s->setDB(DB_CLASSIFICATIONS2);
        $defaultTypes = \CORE\globalData::$siteConfig->website->classificationOrigins;
        
        
        $req = site::$s->q("SELECT * from types");
        while($res=f($req)){
            $types[$res['id']] = $res;
        }
        foreach($defaultTypes as $typeID=>$classificationOriginID){
            site::$s->setDB(DB_JOBS);
            $req = site::$s->q("SELECT C.id "
                    . "FROM jobsClassifications JC "
                    . "INNER JOIN CLASSIFICATIONS2.classifications C ON C.id=JC.classificationID "
                    . "WHERE JC.jobID=$this->id AND C.classificationOriginID=$classificationOriginID");
            while($res=f($req)){
                $classificationID=$res['id'];
                $n = $types[$typeID]['name'];
                $this->{$n}[] = new \JOBSEARCH\COMPONENT\classificationP("scratch",$classificationID);
            }
        }
        
        if(count($this->industry)==0){
            /**
             * @TODO check if the website has a unique classifictaiond efined, and pick up this one.
             */
            $this->mainClassificationName = "";
        }
        else{
            $this->mainClassificationName = $this->industry[0]->shortName;
        }
    }
}

?>
