<?php
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
namespace JOBSEARCH\COMPONENT;
use \CORE\site;

class jobStats{
        /**
         *
         * @var int NUmber of Time a job has been visited
         */
	public $nbVisits;
        /**
         *
         * @var int number of Time a job has been applied for
         */
	public $nbClicks;
        /**
         *
         * @var int
         */
	private $jobID;
        
        /**
         *
         * @param int $jobID 
         */
	public function __construct($jobID){
            $this->jobID=$jobID;
            site::$s->setDB(DB_JOBS);
            $sql="SELECT visits,clicks from jobsAnalytics WHERE id=".$jobID ." AND websiteID=".\CORE\globalData::$siteConfig->id;
            $req=\CORE\site::$s->q($sql);
            if(num($req)==0){
                $this->nbVisits = 0;
                $this->nbApply = 0;
                site::$s->q("INSERT INTO jobsAnalytics SET id=".$jobID ." ,websiteID=".\CORE\globalData::$siteConfig->id.",clicks=0,visits=0");
            }
            else{
                $res=f($req);
                $this->nbVisits = $res['visits'];
                $this->nbClicks = $res['clicks'];                
            }
	}
        
        /**
         * @todo implement with authorized ips
         */
	public  function incrVisit(){
		site::$s->setDB(DB_JOBS);
                site::$s->q("UPDATE jobsAnalytics SET visits=visits+1 WHERE id=".$this->jobID ." AND websiteID=".\CORE\globalData::$siteConfig->id);       
	}
        public function incrClick(){
            site::$s->setDB(DB_JOBS);
                site::$s->q("UPDATE jobsAnalytics SET clicks=clicks+1 WHERE id=".$this->jobID ." AND websiteID=".\CORE\globalData::$siteConfig->id);       
        }
        
        public static  function getUserIpAddr(){
            if (!empty($_SERVER['HTTP_CLIENT_IP'])){ //shared
                return $_SERVER['HTTP_CLIENT_IP'];
            }
            else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){ //rpoxy
                return $_SERVER['HTTP_X_FORWARDED_FOR'];
            }
            else{
                return $_SERVER['REMOTE_ADDR'];
            }
        }
    
        private  function isAuthorizedIP($ip){
            \CORE\site::$s->setDB(DB_SPIDERS);
            $sql = "SELECT id FROM spiders WHERE ip= '".$ip."'";
            $req = \CORE\site::$s->q($sql);
            return num($req)>0 ? false : true;
        }
}

?>