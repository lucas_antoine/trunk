<?php
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
//should be shared
namespace JOBSEARCH\COMPONENT;

class keyword {
    /**
     *
     * @var string the keyword has the use type it
     */
    public  $text;
    /**
     *
     * @var string[] the keywords uuina array - OR
     */
    public  $formatedText=array();
    
    
    public static function createFrom_MgetText($k) {
        if($k==null){
            return null;
        }
        return new keyword($k);
    }
    /*
    public static function createFrom_advancedKeyword_k($k=null) {
        return new keyword($k);
    }*/
    
    /**
     *
     * @param string $keywords
     */
    public function __construct($keywords=null){
        if(!is_null($keywords)){
            $encoding = mb_detect_encoding($keywords);
            $this->text = iconv($encoding , "UTF-8//TRANSLIT//IGNORE",preg_replace('/[^(\x20-\x7F)]*/','', $keywords));
            $this->text = str_replace("%2C",",",$this->text);
            if(strlen($this->text)>0){
                $this->formatKeyword();
            }
        }
    }
    /**
     * Format the keyword into an array
     */
    private function formatKeyword(){
        $tmp = explode("," , $this->text);
        if(count($tmp)==1&&$tmp[0]!='a'){
            $this->formatedText=$tmp;        
        }
        else{
            foreach($tmp as $text){
                //prevent having only a in the keyword (for urls coming from old webjobz)
                if($text != 'a'){
                    $this->formatedText[]=trim($text);
                }
                
            }
        }
    }
    /**
     *
     * @return string A well formated keyword string
     */
    public function getText(){
        return count($this->formatedText)>0 ? implode(", ",$this->formatedText) : "";
    }
    /**
     *
     * @return atring A well formated keyword compatible with SPHINX
     */
    public function getSphinxText(){
        $tmp =array();
        foreach($this->formatedText as $text){
            $tmp[]='"'.htmlentities($text , ENT_QUOTES , "UTF-8").'"';
        }
        return  implode(" | ",$tmp);
    }

}
?>
