<?
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
/*
 * Error Codes
 */
define("NOTHING_FOUND", 2);
define("CONFLICTS_FOUND", 3);
define("NO_COUNTRY", 4);
define("MATCH", 0);
define("EQUAL", 1);

class LocationType {

    const REGION = 1;
    const ALL = 0;
    const COUNTRY = 2;
    const STATE = 3;
    const TOWN = 4;

}

class LocationLookup {

    public $places = array();
    public static $string;
    public $tmpNames;
    public static $errorCode;
    public static $objectID, $objectType, $command;
    public static $resolutionType = EQUAL;

    public function __construct($objectID, $objectType, $command = "none") {
        self::$objectID = $objectID;
        self::$objectType = $objectType;
        self::$command = $command;
        $this->places = array();
        $this->places['regions'] = array();
        $this->places['countries'] = array();
        $this->places['states'] = array();
        $this->places['places'] = array();
        Suggestions::initialise();
        $this->tmpNames = array();
    }

    public function add($name, $type = 0) {
        //echo "Adding $name - $type \n";
        $name = trim(html_entity_decode($name, ENT_QUOTES, "UTF-8"));
        if ($name == "") {
            return;
        }
        if (!in_array($name, $this->tmpNames)) {
            $this->tmpNames[] = strtolower($name);
        }
        if (is_array($type)) {
            foreach ($type as $ty) {
                switch ($ty) {
                    case LocationType::REGION :
                        $this->places['regions'][] = $name;
                        break;
                    case LocationType::COUNTRY :
                        $this->places['countries'][] = $name;
                        break;
                    case LocationType::STATE :
                        $this->places['states'][] = $name;
                        break;
                    case LocationType::TOWN :
                        $this->places['places'][] = $name;
                        break;
                    case LocationType::ALL :
                    default :
                        $this->places['regions'][] = $name;
                        $this->places['countries'][] = $name;
                        $this->places['states'][] = $name;
                        $this->places['places'][] = $name;
                        return;
                        break;
                }
            }
        } else {
            switch ($type) {
                case LocationType::REGION :
                    $this->places['regions'][] = $name;
                    break;
                case LocationType::COUNTRY :
                    $this->places['countries'][] = $name;
                    break;
                case LocationType::STATE :
                    $this->places['states'][] = $name;
                    break;
                case LocationType::TOWN :
                    $this->places['places'][] = $name;
                    break;
                case LocationType::ALL :
                default :
                    $this->places['regions'][] = $name;
                    $this->places['countries'][] = $name;
                    $this->places['states'][] = $name;
                    $this->places['places'][] = $name;
                    return;
                    break;
            }
        }
    }

    public function resolve() {
        /*
         * Check if a country or a region is defined
         */
        if (count($this->places['regions']) == 0 && count($this->places['countries']) == 0) {
            throw new Exception("You must specify at least one region or one country");
        }
        /*
         * Construct String
         */
        sort($this->tmpNames);
        self::$string = implode("||", $this->tmpNames);
        if (self::$string == "worldwide" || self::$string == "various locations") {
            $l = new Location();
            $l->regionID = 0;
            $l->countryID = 0;
            $l->stateID = 0;
            $l->countiesID = 0;
            $l->localAdminID = 0;
            $l->townID = 0;
            $l->score = 1;
            Suggestions::add($l);
            Suggestions::resolveBest();
            foreach (Suggestions::$data as $dati) {
                $dati->resolveNames();
            }
            return true;
        }
        /*
         * Check if it is remembered
         */
        $remember = Remember::init();
        if ($remember !== false) {
            $l = new Location();
            $l->regionID = $remember['regionID'];
            $l->countryID = $remember['countryID'];
            $l->stateID = $remember['stateID'];
            $l->countiesID = $remember['countyID'];
            $l->localAdminID = $remember['localAdminID'];
            $l->townID = $remember['townID'];
            $l->score = 1;
            Suggestions::add($l);
            Suggestions::resolveBest();
            foreach (Suggestions::$data as $dati) {
                $dati->resolveNames();
            }
            return true;
        }
        /*
         * Resolve Region & Country
         */;
        $resolution = new Resolution($this->places);
        Suggestions::resolveBest();
    }

    public function getConflicts() {
        if (self::$errorCode == CONFLICTS_FOUND) {
            return Suggestions::$data;
        } else {
            return null;
        }
    }

    public function getResults() {
        if (self::$errorCode == null) {
            return reset(Suggestions::$data);
        } else {
            return null;
        }
    }

}

class Location {

    public $regionNames, $regionID, $stateNames, $stateID, $countyNames, $countyID, $localAdminNames, $localAdminID, $townNames, $townID, $score, $countryID, $countryNames, $iso, $finalScore;

    public function __construct() {
        $this->stateID = null;
        $this->countyID = null;
        $this->localAdminID = null;
        $this->townID = null;
        $this->regionID = null;
        $this->score = 0;
        $this->finalScore = 0;
    }

    public function getSimpleName() {
        $names = array();
        if ($this->regionID != 0) {
            $names[] = $this->regionNames['ENG'];
        }
        if ($this->countryID != 0) {
            $names[] = $this->countryNames['ENG'];
        }
        if ($this->stateID != 0) {
            $names[] = $this->stateNames['ENG'];
        }
        if ($this->countyID != 0) {
            $names[] = $this->countyNames['ENG'];
        }
        if ($this->localAdminID != 0) {
            $names[] = $this->localAdminNames['ENG'];
        }
        if ($this->townID != 0) {
            $names[] = $this->townNames['ENG'];
        }
        return implode(", ", $names);
    }

    public function getID() {
        return $this->regionID . " - " . $this->countryID . " - " . $this->stateID . " - " . $this->countyID . " - " . $this->localAdminID . " - " . $this->townID;
    }

    public function getDepth() {
        if ($this->townID != null && $this->townID != 0) {
            $this->depth = 6;
        } else if ($this->localAdminID != null && $this->localAdminID != 0) {
            $this->depth = 5;
        } else if ($this->countyID != null && $this->countyID != 0) {
            $this->depth = 4;
        } else if ($this->stateID != null && $this->stateID != 0) {
            $this->depth = 3;
        } else if ($this->countryID != null && $this->countryID != 0) {
            $this->depth = 2;
        } else if ($this->regionID != null && $this->regionID != 0) {
            $this->depth = 1;
        } else {
            throw new Exception("Problem into depth calcul ");
        }
    }

    private static $langs = array("ARA", "CHI", "ENG", "FRE", "GER", "ITA", "POR", "SPA");

    public static function getLocationNames($id) {
        $sql = "SELECT " . implode(",", self::$langs) . " FROM Locations.locationNames WHERE id=$id";
        try {
            $req = $req = \CORE\site::$s->q($sql);
        } catch (Exception $e) {
            
        }
        $res = f($req);
        foreach ($res as $lang => $name) {
            $res[$lang] = html_entity_decode($name, ENT_QUOTES, "UTF-8");
        }
        return $res;
    }

    public function resolveNames() {
        $this->stateNames = array();
        $this->countyNames = array();
        $this->localAdminNames = array();
        $this->townNames = array();
        $this->countryNames = array();
        $this->regionNames = array();

        if (isset($this->stateID) && $this->stateID != "" && $this->stateID != 0) {
            $this->stateNames = self::getLocationNames($this->stateID);
        } else {
            $this->stateID = 0;
        }
        if (isset($this->countyID) && $this->countyID != "" && $this->countyID != 0) {
            $this->countyNames = self::getLocationNames($this->countyID);
        } else {
            $this->countyID = 0;
        }

        if (isset($this->localAdminID) && $this->localAdminID != "" && $this->localAdminID != 0) {
            $this->localAdminNames = self::getLocationNames($this->localAdminID);
        } else {
            $this->localAdminID = 0;
        }

        if (isset($this->townID) && $this->townID != "" && $this->townID != 0) {
            $this->townNames = self::getLocationNames($this->townID);
        } else {
            $this->townID = 0;
        }

        if (isset($this->countryID) && $this->countryID != "" && $this->countryID != 0) {
            $this->countryNames = self::getLocationNames($this->countryID);
        } else {
            $this->countryID = 0;
        }

        if (isset($this->regionID) && $this->regionID != "" && $this->regionID != 0) {
            $this->regionNames = self::getLocationNames($this->regionID);
        } else {
            $this->regionID = 0;
        }
    }

    public function merge($Location) {
        /*
         * Add Location's score to this
         */
        //echo "Adding $this->finalScore to $Location->score SCORE\n";
        $this->finalScore+=$Location->score;
    }

    public function hasNoRelationWith($l) {
        if ($this->isEqual($l)) {
            return false;
        }
        if ($this->isSubOf($l)) {
            return false;
        }
        if ($this->isParentOf($l)) {
            return false;
        }
        return true;
    }

    public function isEqual($l) {
        if ($this->regionID == $l->regionID && $this->countryID == $l->countryID && $this->stateID == $l->stateID && $this->countyID == $l->countyID && $this->localAdminID == $l->localAdminID && $this->townID == $this->townID) {
            return true;
        }
        //echo $this->regionID." - ".$this->countryID." - ".$this->stateID." - ".$this->countyID." - ".$this->localAdminID."\n";
        //echo $l->regionID." - ".$l->countryID." - ".$l->stateID." - ".$l->countyID." - ".$l->localAdminID."\n";
        return false;
    }

    public function isSubOf($l) {
        if ($this->isEqual($l)) {
            return false;
        }
        if ($this->regionID == $l->regionID) {
            if ($l->countryID == 0 && $this->regionID != 0) {
                return true;
            } else if ($this->countryID == $l->countryID) {
                if ($l->stateID == 0 && $this->stateID != 0) {
                    return true;
                } elseif ($this->stateID == $l->stateID) {
                    if ($l->countyID == 0 && $this->countyID != 0) {
                        return true;
                    } elseif ($this->countyID == $l->countyID) {
                        if ($l->localAdminID == 0 && $this->localAdminID != 0) {
                            return true;
                        } elseif ($this->localAdminID == $l->localAdminID) {
                            if ($location->townID == 0 && $this->townID != 0) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    public function isParentOf($l) {
        if ($this->isEqual($l)) {
            return false;
        }
        if ($this->regionID == $l->regionID) {
            if ($l->countryID != 0 && $this->regionID == 0) {
                return true;
            } else if ($this->countryID == $l->countryID) {
                if ($l->stateID != 0 && $this->stateID == 0) {
                    return true;
                } elseif ($this->stateID == $l->stateID) {
                    if ($l->countyID != 0 && $this->countyID == 0) {
                        return true;
                    } elseif ($this->countyID == $l->countyID) {
                        if ($l->localAdminID != 0 && $this->localAdminID == 0) {
                            return true;
                        } elseif ($this->localAdminID == $l->localAdminID) {
                            if ($location->townID != 0 && $this->townID == 0) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    public function showNames() {
        echo "regionID : $this->regionID \ncountryID  : $this->countryID \nstateID : $this->stateID \ncountyID : $this->countyID \nlocalAdminID : $this->localAdminID\n";
    }

}

class Suggestions {

    /**
     *
     * @var Locations[]
     */
    public static $data = array();
    public static $stockNotFound = true;
    public static $debug = false;

    public static function initialise() {
        self::$data = array();
    }

    public function keepOld($location) {
        self::$data[] = $location;
    }

    public static function setAllScores() {
        foreach (Suggestions::$data as $location) {

            /*
             * Get All those who are present in a subLevel
             */

            /* foreach(Suggestions::$data as $parentLocation){
              if($location    !== $parentLocation){//that should always happen
              $isSub = $location->isSub($parentLocation);
              if($isSub){
              $location->merge($parentLocation);
              }
              }
              } */
        }
    }

    public static function setAlldepth() {
        foreach (Suggestions::$data as $location) {
            $location->getDepth();
        }
    }

    public static function resolveDepth() {
        /*
         * Get the lowest depoth result
         */
        $minDepth = 6;
        $indexes = array();
        foreach (Suggestions::$data as $index => $location) {
            if ($location->depth < $minDepth) {
                $minDepth = $location->depth;
                unset($indexes);
                $indexes = array();
                $indexes[] = $index;
            } elseif ($location->depth == $minDepth) {
                $indexes[] = $index;
            } else {
                
            }
        }
        foreach (Suggestions::$data as $index => $location) {
            if (!in_array($index, $indexes)) {
                unset(Suggestions::$data[$index]);
            }
        }
    }

    public static function resolveBest() {

        $best = array();
        self::setAllScores();
        //var_dump(Suggestions::$data);
        $maxScore = 0;

        if (count(Suggestions::$data) == 0) {
            LocationLookup::$errorCode = NOTHING_FOUND;
            return;
        }

        foreach (Suggestions::$data as $index => $data) {
            if ($data->finalScore > $maxScore) {
                $maxScore = $data->finalScore;
                unset($best);
                $best = array();
            }
            if ($data->finalScore == $maxScore) {
                $best[] = $index;
            }
        }
        foreach (Suggestions::$data as $index => $data) {
            if (!in_array($index, $best)) {
                unset(Suggestions::$data[$index]);
            }
        }
        if (Suggestions::ENB() != 1) {

            self::setAllDepth();
            self::resolveDepth();
        }
        if (Suggestions::ENB() != 1) {

            //Stock The Data into the failed Resolution Lookup
            if (Suggestions::ENB() == 0) {
                LocationLookup::$errorCode = NOTHING_FOUND;
            } else {
                LocationLookup::$errorCode = CONFLICTS_FOUND;
            }
            if (self::$stockNotFound) {
                Remember::stock();
            }
        } else {
            LocationLookup::$errorCode = null;
            foreach (self::$data as $dati) {
                $dati->resolveNames();
            }
        }
    }

    public function nb() {
        return count(Suggestions::$data);
    }

    public static function ENB() {
        return count(Suggestions::$data);
    }

    /**
     * 
     * @param Location $data
     * @return type
     */
    public static function add($data) {

        if (self::$debug) {
            $data->resolveNames();
            echo "Adding " . $data->getSimpleName() . "\n";
            echo $data->getID() . "\n\n";
            echo "Is Equal?\n";
        }
        foreach (Suggestions::$data as $i => $location) {
            if (self::$debug) {
                //echo $location->getID()."\n";
            }
            if ($location->isEqual($data)) {
                if (self::$debug) {
                    echo "    - equal found\n";
                }
                Suggestions::$data[$i]->merge($data); //Score added
                if (self::$debug) {
                    echo "new score : " . Suggestions::$data[$i]->finalScore . "\n";
                }
                return;
            }
        }
        if (self::$debug) {
            echo "Not equal\n\n";
        }

        Suggestions::$data[] = $data;
        $indexOfData = count(Suggestions::$data) - 1;
        if (self::$debug) {
            echo "location added to suggestions\n";
            echo "\n has relations?\n";
        }


        foreach (Suggestions::$data as $i => $location) {
            if (self::$debug) {
                //echo $location->getID()."\n";
            }
            if ($location->isSubOf($data)) {
                if (self::$debug) {
                    echo "    - " . $location->getSimpleName() . " is Sub of " . $data->getSimpleName() . "\n";
                }
                Suggestions::$data[$i]->merge($data);
                if (self::$debug) {
                    echo "new score : " . Suggestions::$data[$i]->finalScore . "\n";
                }
            } elseif ($location->isParentOf($data)) {
                if (self::$debug) {
                    echo "    - " . $location->getSimpleName() . " is Parent of " . $data->getSimpleName() . "\n";
                }
                Suggestions::$data[$indexOfData]->merge($location);
                if (self::$debug) {
                    echo "new score : " . Suggestions::$data[$indexOfData]->finalScore . "\n";
                }
            } elseif ($location->hasNoRelationWith($data)) {
                if (self::$debug) {
                    echo "    - " . $location->getSimpleName() . " has no relation with " . $data->getSimpleName() . "\n";
                }
            } elseif ($location->isEqual($data)) {
                if (self::$debug) {
                    //normal
                    //echo "normal\n";
                }
            } else {
                echo "this shouldn't happen\n";
                die;
                var_dump($this);
            }
        }
    }

}

class Remember {

    public static $params = array();

    public static function getString() {
        return LocationLookup::$string;
    }

    public static function init() {
        $string = htmlentities(Remember::getString(), ENT_QUOTES, "UTF-8");
        \CORE\site::$s->setDB(DB_SOAP);
        $sql = "SELECT * from SOAP.locationLookupData where remember=1 and locationString='$string'";
        $req = \CORE\site::$s->q($sql);
        if (num($req) == 0) {
            return false;
        } else {
            return f($req);
        }
    }

    public static function isStocked($string) {
        $sql = "SELECT * FROM SOAP.locationLookupFailed WHERE data='$string'";
        \CORE\site::$s->setDB(DB_SOAP);
        $req = \CORE\site::$s->q($sql);
        if (num($req) == 0) {
            return false;
        } else {
            $res = f($req);
            return $res['id'];
            ;
        }
    }

    public static function stock() {
        $data = htmlentities(Remember::getString(), ENT_QUOTES, 'UTF-8');
        $entryID = Remember::isStocked($data);
        if ($entryID === false) {
            /*
             * Create the entry
             */
            $suggestions = htmlentities(serialize(Suggestions::$data), ENT_QUOTES, 'UTF-8');
            $sql = "INSERT  INTO
			SOAP.locationLookupFailed
			SET
			suggestions=\"" . $suggestions . "\",
			data='$data',
			error=" . LocationLookup::$errorCode;
            \CORE\site::$s->setDB(DB_SOAP);
            \CORE\site::$s->q($sql);
            //$entryID = configData::$s->last_insert(SOAP);

            /*
             * Send a Mail
             */


            $message = "
			The location '$data' hasn't been resolved correctly.\n
			This mean that all jobs coming from SOAP  job won't get a location defined.\n
			Please go on
			http://admin.expatjob.net/index.php?r=locationLookupFailed/resolve&id=$entryID to fix that, and update all the jobs concerned with it\n\n
			Thx.\n\n
			Antoine.
			";
            $subject = "A new SOAP Location has been detected, please update the resolutions";

            //soapCall::sendNotificationMail($subject , $message);
        }



        /*
         * Set the job ID
         */
        $sql = "INSERT INTO SOAP.locationLookupJobs
		SET objectID=" . LocationLookup::$objectID . ",
		locationLookupID=$entryID ,
		command = '" . LocationLookup::$command . "',
		dateIncoming = NOW() ,
		objectType = '" . LocationLookup::$objectType . "'";
        \CORE\site::$s->setDB(DB_SOAP);
        \CORE\site::$s->q($sql);
    }

}

class Resolution {

    public $locations;
    public $iso;
    public static $limit = 3;
    public static $minScore = 8;

    public function __construct($locations) {
        \CORE\site::$s->setDB(DB_LOCATION);
        //Suggestions::$debug =true;
        if (Suggestions::$debug) {
            var_dump($locations);
        }
        $this->locations = $locations;
        if (Suggestions::$debug) {
            echo "/******************REGIONS*************/\n\n";
        }
        foreach ($this->locations['regions'] as $regionName) {
            $this->lookupRegion($regionName);
        }
        if (Suggestions::$debug) {
            echo "/******************COUNTRY*************/\n\n";
        }
        foreach ($this->locations['countries'] as $countryName) {
            $this->lookupCountry($countryName);
        }
        if (Suggestions::$debug) {
            echo "/******************STATE*************/\n\n";
        }
        foreach ($this->locations['states'] as $stateName) {
            $this->lookupState($stateName);
        }
        if (Suggestions::$debug) {
            echo "/******************PLACE*************/\n\n";
        }
        foreach ($this->locations['places'] as $locationName) {
            $this->lookupLocation($locationName);
        }
        foreach (Suggestions::$data as $dati) {
            $dati->resolveNames();
        }
        
    }

    public function lookupRegion($name) {

        switch (LocationLookup::$resolutionType) {
            case MATCH :
                $sql = "SELECT distinct id ,name_type , match(name) AGAINST(\"$name\" IN NATURAL LANGUAGE MODE) as score
					from Locations.regions where
					match(name) AGAINST(\"$name\" IN NATURAL LANGUAGE MODE)
					HAVING score >=" . Resolution::$minScore . " order by score DESC limit 0," . Resolution::$limit;
                break;
            case EQUAL :
                $sql = "SELECT distinct id,name_type  from Locations.regions where
					name LIKE \"$name\"
					limit 0," . Resolution::$limit;
                break;
        }
        if (Suggestions::$debug) {
            echo "$sql \n\n";
        }
        $req = \CORE\site::$s->q($sql);
        while ($res = f($req)) {
            $suggestion = new Location();
            $suggestion->regionID = $res['id'];
            if (in_array($res['name_type'], array("V", "A", "S"))) {
                $suggestion->score = 0.5;
                $suggestion->finalScore = $suggestion->score;
            } else {
                $suggestion->score = 1;
                $suggestion->finalScore = $suggestion->score;
            }
            Suggestions::add($suggestion);
        }
    }

    public function lookupCountry($name) {
        $name = trim($name);
        if (strlen($name) == 2) {
            $name = strtoupper($name);
            if ($name == "UK") {
                $name = "GB";
            }
            $sql = "SELECT distinct P.id as countryID,l1.parentID as regionID ,P.name_type
			FROM Locations.simplifiedPlaces P
			INNER JOIN  Locations.parentChild l1 ON l1.id=P.id
			WHERE
			P.iso =  \"$name\"
			AND P.depth=1
			group by P.id
			limit 0," . Resolution::$limit;

            if (Suggestions::$debug) {
                echo "$sql\n";
            }


            $req = \CORE\site::$s->q($sql);
            if (num($req) != 0) {
                $res = f($req);
                $suggestion = new Location();
                $suggestion->regionID = $res['regionID'];
                $suggestion->countryID = $res['countryID'];
                $suggestion->score = 2;
                $suggestion->finalScore = $suggestion->score;
                Suggestions::add($suggestion);
            }
        } else {

            switch (LocationLookup::$resolutionType) {
                case MATCH :
                    $sql = "
					SELECT  P.id as countryID,l1.parentID as regionID ,P.name_type ,match(P.name) AGAINST(\"$name\" IN NATURAL LANGUAGE MODE) as score
					FROM Locations.simplifiedPlaces P
					INNER JOIN  Locations.parentChild l1 ON l1.id=P.id
					WHERE
					match(P.name) AGAINST(\"$name\" IN NATURAL LANGUAGE MODE)
					AND P.depth=1
					group by P.id
					HAVING score >=" . Resolution::$minScore . " order by score DESC limit 0," . Resolution::$limit;
                    break;
                case EQUAL :
                    $name = strtoupper($name);
                    $sql = "SELECT P.id as countryID,l1.parentID as regionID ,P.name_type
					FROM Locations.simplifiedPlaces P
					INNER JOIN  Locations.parentChild l1 ON l1.id=P.id
					WHERE
					P.name = \"$name\"
					AND P.depth=1
					limit 0," . Resolution::$limit;

                    break;
            }
            if (Suggestions::$debug) {
                echo "$sql\n";
            }

           $req = \CORE\site::$s->q($sql);
            while ($res = f($req)) {
                $suggestion = new Location();
                $suggestion->regionID = $res['regionID'];
                $suggestion->countryID = $res['countryID'];
                if (in_array($res['name_type'], array("V", "A", "S"))) {
                    $suggestion->score = 1;
                    $suggestion->finalScore = $suggestion->score;
                } else {
                    $suggestion->score = 2;
                    $suggestion->finalScore = $suggestion->score;
                }
                Suggestions::add($suggestion);
            }
        }
    }

    public function lookupState($name) {
        switch (LocationLookup::$resolutionType) {
            case MATCH :
                $sql = "
				SELECT  P.id as stateID,l2.parentID as countryID ,l1.parentID as regionID,P.name_type ,match(P.name) AGAINST(\"$name\" IN NATURAL LANGUAGE MODE) as score
					FROM Locations.simplifiedPlaces P
					INNER JOIN  Locations.parentChild l2 ON l2.id=P.id
					INNER JOIN  Locations.parentChild l1 ON l1.id=l2.parentID
					WHERE
					match(P.name) AGAINST(\"$name\" IN NATURAL LANGUAGE MODE)
					AND P.depth=2
					group by P.id
					HAVING score >=" . Resolution::$minScore . " order by score DESC limit 0," . Resolution::$limit;
                break;
            case EQUAL :
                $sql = "SELECT P.id as stateID,l2.parentID as countryID ,l1.parentID as regionID,P.name_type
					FROM Locations.simplifiedPlaces P
					INNER JOIN  Locations.parentChild l2 ON l2.id=P.id
					INNER JOIN  Locations.parentChild l1 ON l1.id=l2.parentID
					WHERE
					P.name LIKE \"$name\"
					AND P.depth=2
					group by P.id
					 limit 0," . Resolution::$limit;
                break;
        }
        if (Suggestions::$debug) {
            echo "$sql\n";
        }

        $req = \CORE\site::$s->q($sql);
        while ($res = f($req)) {

            $suggestion = new Location();
            $suggestion->regionID = $res['regionID'];
            $suggestion->countryID = $res['countryID'];
            $suggestion->stateID = $res['stateID'];

            if (in_array($res['name_type'], array("V", "A", "S"))) {
                $suggestion->score = 0.7;
                $suggestion->finalScore = $suggestion->score;
            } else {
                $suggestion->score = 1.5;
                $suggestion->finalScore = $suggestion->score;
            }

            Suggestions::add($suggestion);
        }
    }

    public function getParentID($id) {
        if ($id == 0) {
            return 0;
        }
        $sql = "SELECT parentID FROM Locations.parentChild WHERE id=$id";
        $req = \CORE\site::$s->q($sql);
        if (num($req) == 0) {
            return 0;
        }
        $res = f($req);
        return $res['parentID'];
    }

    public function getFull($id, $depth) {
        $full = array();
        switch ($depth) {
            case 5 :
                $full['townID'] = $id;
                $full['localAdminID'] = $this->getParentID($full['townID']);
                $full['countyID'] = $this->getParentID($full['localAdminID']);
                $full['stateID'] = $this->getParentID($full['countyID']);
                $full['countryID'] = $this->getParentID($full['stateID']);
                $full['regionID'] = $this->getParentID($full['countryID']);
                break;
            case 4 :
                $full['townID'] = 0;
                $full['localAdminID'] = $id;
                $full['countyID'] = $this->getParentID($full['localAdminID']);
                $full['stateID'] = $this->getParentID($full['countyID']);
                $full['countryID'] = $this->getParentID($full['stateID']);
                $full['regionID'] = $this->getParentID($full['countryID']);
                break;
            case 3 :
                $full['townID'] = 0;
                $full['localAdminID'] = 0;
                $full['countyID'] = $id;
                $full['stateID'] = $this->getParentID($full['countyID']);
                $full['countryID'] = $this->getParentID($full['stateID']);
                $full['regionID'] = $this->getParentID($full['countryID']);
                break;
            case 2 :
                $full['townID'] = 0;
                $full['localAdminID'] = 0;
                $full['countyID'] = 0;
                $full['stateID'] = $id;
                $full['countryID'] = $this->getParentID($full['stateID']);
                $full['regionID'] = $this->getParentID($full['countryID']);
                break;
            case 1 :
                $full['townID'] = 0;
                $full['localAdminID'] = 0;
                $full['countyID'] = 0;
                $full['stateID'] = 0;
                $full['countryID'] = $id;
                $full['regionID'] = $this->getParentID($full['countryID']);
                break;
            case 0 :
                $full['townID'] = 0;
                $full['localAdminID'] = 0;
                $full['countyID'] = 0;
                $full['stateID'] = 0;
                $full['countryID'] = 0;
                $full['regionID'] = $id;
                break;
        }
        return $full;
    }

    public function lookupLocation($name) {
        switch (LocationLookup::$resolutionType) {
            case MATCH :
                $sql = "
					SELECT  id,name_type ,match(name) AGAINST(\"$name\" IN NATURAL LANGUAGE MODE) as score ,depth
						FROM Locations.simplifiedPlaces
						WHERE
						match(name) AGAINST(\"$name\" IN NATURAL LANGUAGE MODE)
						AND depth >= 3 group by id
						HAVING score >=" . Resolution::$minScore . " order by score DESC,depth ASC ";
                break;
            case EQUAL :
                $sql = "SELECT id ,name_type ,depth
						FROM Locations.simplifiedPlaces
						WHERE
						name LIKE \"$name\"
						AND depth >= 3  group by id
						 order by depth ASC";
                break;
        }
        if (Suggestions::$debug) {
            echo "$sql\n";
        }
        $req = \CORE\site::$s->q($sql);

        while ($res = f($req)) {
            $full = $this->getFull($res['id'], $res['depth']);
            $suggestion = new Location();
            $suggestion->regionID = $full['regionID'];
            $suggestion->countryID = $full['countryID'];
            $suggestion->stateID = $full['stateID'];

            if ($full['countyID'] != 0) {
                $depth = 3;
                $suggestion->countyID = $full['countyID'];
            }
            if ($full['localAdminID'] != 0) {
                $depth = 4;
                $suggestion->localAdminID = $full['localAdminID'];
            }
            if ($full['townID'] != 0) {
                $depth = 5;
                $suggestion->townID = $full['townID'];
            }
            if (in_array($res['name_type'], array("V", "A", "S"))) {
                $suggestion->score = 3 / (2 * $depth);
                $suggestion->finalScore = $suggestion->score;
            } else {
                $suggestion->score = 3 / $depth;
                $suggestion->finalScore = $suggestion->score;
            }

            Suggestions::add($suggestion);
        }
    }

}

?>