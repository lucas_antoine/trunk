<?php
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
namespace JOBSEARCH\COMPONENT;

use \CORE\site;


class sphinx {
    
    public static function GetIndexName($websiteID){
        
    }
    public static function restart(){
        $output = shell_exec("whoami");
        
        if(trim($output) !== "root"){
            var_dump($output);
            throw new \EX\SecurityException("You must run it as root");
        }
        $output = shell_exec(SPHINX_SEARCHD_RESTART." restart");
        
        echo $output;
    }
    public static function index($rotate=true){
        switch($rotate){
            case true : 
                $cmd = SPHINX_INDEXER_PATH." --config ".SPHINX_CONF_PATH." --all --rotate";
                break;
            case false : 
                $cmd = SPHINX_INDEXER_PATH." --config ".SPHINX_CONF_PATH." --all";
                break;
        }
        $output = shell_exec("whoami");
        
        if(trim($output) !== "root"){
            var_dump($output);
            throw new \EX\SecurityException("You must run it as root");
        }
        $output = shell_exec($cmd);
        
        
        echo $output;
    }
    
    public static function generateConfFile(){
        site::$s->setDB(DB_WEBSITES);
        $req = site::$s->q("SELECT * from websites where websiteID <> 1");
        $conf="";
        while($res=f($req)){
            $website = new website("scratch",$res['id']);
            $conf.=$website->generateSphinxConf();
        }
        $conf.=self::generateIndexerSection();
        $conf.=self::generateSearchDSection();
        
        
        $f = fopen(SPHINX_CONF_PATH, "w");
        fwrite($f, $conf);
        fclose($f);
    }
    
    private static function generateIndexerSection(){
        return "indexer{
    mem_limit			= ".SPHINX_CONF_INDEXER_MEM_LIMIT."
    # max_iops			= 40
    # max_iosize		= 1048576
    # write_buffer		= 1M
}
";
    }
    private static function generateSearchDSection(){
        $conf="#############################################################################
## searchd settings
#############################################################################

searchd{
        listen      =   ".SPHINX_CONF_HOST.":".SPHINX_CONF_PORT."
        log         =   ".SPHINX_CONF_LOG."
        pid_file    =   ".SPHINX_CONF_PID."
        read_timeout		= ".SPHINX_CONF_READ_TIMEOUT."
	client_timeout		= ".SPHINX_CONF_CLIENT_TIMEOUT."
	max_children		= ".SPHINX_CONF_MAX_CHILDREN."
	max_matches		= ".SPHINX_CONF_MAX_MATCHES."
	seamless_rotate		= ".SPHINX_CONF_SEAMLESS_ROTATE."
	preopen_indexes		= ".SPHINX_CONF_PREOPEN_INDEXES."
	unlink_old		= ".SPHINX_CONF_UNLINK_OLD."
	# attr_flush_period	= 900
	# ondisk_dict_default	= 1
	mva_updates_pool	= ".SPHINX_CONF_MVA_UPDATES_POOL."
	max_packet_size		= ".SPHINX_CONF_MAX_PACKET_SIZE."
	max_filters		= ".SPHINX_CONF_MAX_FILTERS."
	max_filter_values	= ".SPHINX_CONF_MAX_FILTER_VALUES."
	# listen_backlog	= 5
	# read_buffer		= 256K
	# read_unhinted		= 32K
}
";  
        return $conf;
        
        
    }

    
    public static function generateSourceIndexName($websiteID){
        return "jobs_".$websiteID;
    }
    public static function generateIndexName($websiteID){
        return "index_jobs_".$websiteID;
    }
}

?>
