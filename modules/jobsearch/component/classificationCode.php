<?php
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
namespace JOBSEARCH\COMPONENT;

use \CORE\site;

class classificationCode{
    
    
    public static function createRange($depth){
        $range = new \stdClass();
        $depth++;
        $range->from    = (double) $depth."000000000";
        $range->to      = (double) $depth."999999999";
        return $range;        
    }
    /**
     * 
     * @param double $code
     * @return \stdClass with the prop depth and cid
     */
    public static function extractCode($code){
        $code = (string)$code;
        
        $data = new \stdClass();
        $data->depth = (int)substr($code, 0, 1)-1;
        $data->cid = (int)substr($code, 1);        
        return $data;        
    }
    
    /**
     * 
     * @param string $code
     * @return \JOBSEARCH\COMPONENT\classificationP
     */
    public static function getClassification($code){
        $data  = self::extractCode($code);
        return new \JOBSEARCH\COMPONENT\classificationP("scratch", $data->cid);
    }
    
    
    
    public static function generateClassificationID($classificationID,$depth=null){
        $classificationID = (int)$classificationID;
        if($depth==null){
            site::$s->setDB(DB_CLASSIFICATIONS2);
            $res = f(site::$s->q("SELECT depth FROM classifications WHERE id=$classificationID"));
            $depth = $res["depth"]+1;
        }
        $depth++;
        
        
        if($classificationID < 10){
            $cID = "00000000".$classificationID;
        }
        elseif($classificationID < 100){
            $cID = "0000000".$classificationID;
        }
        elseif($classificationID < 1000){
            $cID = "000000".$classificationID;
        }
        elseif($classificationID < 10000){
            $cID = "00000".$classificationID;
        }
        elseif($classificationID < 100000){
            $cID = "0000".$classificationID;
        }
        elseif($classificationID < 1000000){
            $cID = "000".$classificationID;
        }
        elseif($classificationID < 10000000){
            $cID = "00".$classificationID;
        }
        elseif($classificationID < 100000000){
            $cID = "0".$classificationID;
        }
        else{
                $cID =$classificationID;
        }
        $id = $depth.$cID;
        return (double)$id;
        
    }
}
?>
