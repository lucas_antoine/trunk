<?php
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
namespace JOBSEARCH\COMPONENT;

use \CORE\site;

class search {

    /**
     *
     * @var float The time taken to make the search
     */
    public $searchTime;

    /**
     *
     * @var int The total number of result found
     */
    public $foundRows;

    /**
     *
     * @var job[]
     */
    public $resultList;

    /**
     * For mobile version, tells if we are at the last page (or not)
     *
     * @var boolean
     */
    public $endList;

    /**
     *
     * @var \JOBSEARCH\COMPONENT\classificationP[]
     */
    public $searchParameters = [];

    public function __construct() {

    }


    public static function getNbJobsLoc($loc){
        require_once EXT_BASE.'sphinxapi.php';
        $sphinxIndexName = \CORE\globalData::$siteConfig->website->sphinxIndex;
        $sphinx = new \SphinxClient();
        $sphinx->setServer(SPHINX_SEARCH_IP, \CORE\globalData::$siteConfig->website->sphinxPort);
        self::setLocationsFilter($sphinx,array($loc));


        $data = array(
            "sphinx"=>$sphinx,
            "q"=>null,
            "index"=>$sphinxIndexName
        );

        $res = $sphinx->Query ( $data['q'] , $data['index'] );
        if($res['error']!=0 || $res===false){
            throw new \EX\sphinxException("a sphinx exception has just occured : result : ".var_export($res,true)."<pre>\n\nSphinx Object : ".var_export($sphinx,true));
        }
        //nb new jobs;
        return  $res['total_found'];
    }

    public static function getNbJobsPostedSince($timestamp){
        $time = time();
        require_once EXT_BASE.'sphinxapi.php';
        $sphinxIndexName = \CORE\globalData::$siteConfig->website->sphinxIndex;
        $sphinx = new \SphinxClient();
        $sphinx->setServer(SPHINX_SEARCH_IP,\CORE\globalData::$siteConfig->website->sphinxPort);

        $sphinx->setFilterRange("postedDate", $timestamp,$time );



        $data = array(
            "sphinx"=>$sphinx,
            "q"=>null,
            "index"=>$sphinxIndexName
        );

        $res = $sphinx->Query ( $data['q'] , $data['index'] );
        if($res['error']!=0 || $res===false){
            throw new \EX\sphinxException("a sphinx exception has just occured : result : ".var_export($res,true)."<pre>\n\nSphinx Object : ".var_export($sphinx,true));
        }
        //nb new jobs;
        return  $res['total_found'];

    }

    
    /**
     *
     * @return int
     * @throws \EX\sphinxException
     */
    public static function getNbTotalJobs(){
        site::intialiseSphinxConnection();
        /**
         * Load Website Preferences - classification definition + sources
         */
        site::$sphinx->loadWebsiteFilters();
        site::$sphinx->setSelect("*");
        site::$sphinx->setLimits(0, 100,SPHINX_MAX_MATCH);
        site::$sphinx->setServer(SPHINX_SEARCH_IP, \CORE\globalData::$siteConfig->website->sphinxPort);
        //init
        site::$sphinx->SetArrayResult(true);
        //get the results
        
        $res = site::sphinxExec(site::$sphinx, "", \CORE\globalData::$siteConfig->website->sphinxIndex);
        //if an error was found we throw an exception
        if ($res['error'] != 0) {
            throw new sphinxException("a sphinx exception has just occured : " . var_export($res, true));
        }
        /**
         * @todo Add the sphinx error control (Make it generic)
         */
        return  $res['total_found'];


    }

    

    /**
     * Initialisae the SPHINX search Engine
     * @param \CORE\SphinxClient $sphinx
     * @param filters array of filters ( TypeID => [list of classification].
     * Example : $filters = [1 => 123456,2=> [11,22]], means it will search for (typeID=1 AND classificationID=1) AND (typeID=2 AND ( classificationID=11 OR classificationID=22 ) )
     * @param int $limitFrom
     * @param int $nbResults
     * @param int $sortMode
     * @param array $fieldWeights
     * @return array with "q" as the search query, and "sphinxIndexName" properties
     */
    public static function setSphinxData(\CORE\SphinxClient &$sphinx,$filters, $limitFrom, $nbResults, $sortMode = true, $fieldWeights = null) {
        //set the limits
        $sphinx->setLimits($limitFrom, $nbResults,SPHINX_MAX_MATCH);
        //$sphinx->setSelect("id,postedDate,vacRef");
        $sphinx->setSelect("*");
        $sphinx->setServer(SPHINX_SEARCH_IP, \CORE\globalData::$siteConfig->website->sphinxPort);
        //do we need to sort?
        //might wanna pass the sort attributes as a param instead of the boolean and have a defaultt sort attribute
        if ($sortMode) {
            $sphinx->SetSortMode(SPH_SORT_EXTENDED, "postedDate DESC, @weight DESC, superweight DESC");
        }

        //init
        $sphinx->SetArrayResult(true);

        //deal with user input filter
        if(isset($filters["keywords"])){
            //$sphinx->SetMatchMode(SPH_MATCH_EXTENDED2);
            //$sphinx->SetMatchMode(SPH_MATCH_ANY);
            //$sphinx->SetRankingMode(SPH_RANK_WORDCOUNT);
            if ($fieldWeights != null) {
                $sphinx->SetFieldWeights($fieldWeights);
            }
            $q = $filters["keywords"];
            unset($filters["keywords"]);
        }
        else{
            $q="";
        }


        foreach($filters as $typeID => $data) {
            
            if(count($data)!=1){
                throw new \myException("Check the case where we get more than one search parameter");
            }
            
                
            $depth0 = [];
            $depth1 = [];
            $depth2 = [];
            $depth3 = [];
            $depth4 = [];
            $depth5 = [];
            foreach($data as $classification){
                $arname = "depth".$classification->depth;
                array_push($$arname,  $classification->id);
            }
            for($i=0;$i<=5;$i++){
                $arname = "depth".$i;
                if(count($$arname)>0){
                    $sphinx->addClassificationFilter($$arname, $typeID, $i);
                }
            }
        }

        return array(
            "q" => $q,
            "sphinxIndexName" => \CORE\globalData::$siteConfig->website->sphinxIndex
        );
    }

    /**
     * Populate the resultList properties with jobs.
     * @param array $res  The array returned by Sphinx search engine
     */

    private function formatSphinxResults($res) {
        $this->resultList = array();
        ;
        if ($this->foundRows == 0) {
            return;
        }
        if (!isset($res['matches'])) {
            return;
        }
        
        

        foreach ($res['matches'] as $match) {
            //$jobID = $match['id'];
            $jobID = $match['id'];
            try {
                $this->resultList[] = new job($jobID);
            }
            catch (\Exception $e) {
                switch (get_class($e)) {
                    case "EX\NotFoundException" :
                        //XMl doesn't exists
                        //echo "NOT FOUND";
                        site::$logger->error("Job Not Found");
                        break;
                    case "EX\XMLErrorException" :
                        //echo "NOT FOUND";
                        site::$logger->error("Job XML Parsing Error");
                        break;
                    default :
                        site::$logger->error($e->getMessage());
                        var_dump($e);
                        die;
                }
            }
        }
    }



    /**
     *
     * @param \JOBSEARCH\COMPONENT\job $job
     * @param int $maxResults
     * @return type
     * @throws sphinxException
     */
    public function jobLikeThis(\JOBSEARCH\COMPONENT\job $job, $maxResults=10){
        $t1 = microtime(true);
        site::intialiseSphinxConnection();


        $page = 1;
        $limitFrom = (int) $page * (int) NB_RESULTS_PER_PAGE;
        $nbResultPerPage = (int) NB_RESULTS_PER_PAGE;

        /**
         * Load Website Preferences - classification definition + sources
         */
        site::$sphinx->loadWebsiteFilters();


        $filters = array();

        //keywords
        $keywords = new \JOBSEARCH\COMPONENT\keyword($job->getTitle());
        $filters["keywords"] =  $keywords->getSphinxText();

        /**
         * @todo SORTING MODE IMPLEMENTATION
         */
        //init sphinx with the filters
        $data = self::setSphinxData(site::$sphinx, $filters, 0, $maxResults, true, array("title" => 25, "description" => 10, "companyName" => 1, "vacRef" => 1));

        //get the results
        $res = site::sphinxExec(site::$sphinx, $data['q'], \CORE\globalData::$siteConfig->website->sphinxIndex);

        //if an error was found we throw an exception
        if ($res['error'] != 0) {
            throw new sphinxException("a sphinx exception has just occured : " . var_export($res, true));
        }
        //remove the current job from the list
        foreach($res['matches'] as $i=>$result){
            if($result['id'] == $job->id){
                $res['total_found'] = (int)$res['total_found']-1;
                unset($res['matches'][$i]);
            }
        }
        //otherwise init the class params
        $this->foundRows = (int)$res['total_found'];

        $this->formatSphinxResults($res);
        //end of the time taken
        $this->searchTime = microtime(true) - $t1;
        return site::$sphinx;
    }

    public function latestJobs($nbResults){

        $t1 = microtime(true);
        site::intialiseSphinxConnection();

        $limitFrom = 0;
        $nbResultPerPage = $nbResults;
        /**
         * Load Website Preferences - classification definition + sources
         */
        site::$sphinx->loadWebsiteFilters();

        $filters = array();
        /**
         * @todo SORTING MODE IMPLEMENTATION
         */
        //init sphinx with the filters
        $data = self::setSphinxData(site::$sphinx, $filters, $limitFrom, $nbResultPerPage, true, array("title" => 25, "description" => 10, "companyName" => 1));
        //get the results
        $res = site::sphinxExec(site::$sphinx, $data['q'], \CORE\globalData::$siteConfig->website->sphinxIndex);
        //if an error was found we throw an exception
        if ($res['error'] != 0) {
            throw new sphinxException("a sphinx exception has just occured : " . var_export($res, true));
        }
        //otherwise init the class params
        $this->foundRows = $res['total_found'];
        if ($this->foundRows > ( $limitFrom + (int) $nbResultPerPage)) {
            $this->endList = false;
        } else {
            $this->endList = true;
        }
        $this->formatSphinxResults($res);
        //end of the time taken
        $this->searchTime = microtime(true) - $t1;
        return site::$sphinx;
    }

    /**
     * 
     * @param \JOBSEARCH\COMPONENT\classificationP $classification
     * @return boolean
     * @throws sphinxException
     */
    public function hasResults(\JOBSEARCH\COMPONENT\classificationP $classification){
        site::intialiseSphinxConnection();
        $limitFrom = 0;
        $nbResultPerPage = (int) NB_RESULTS_PER_PAGE;
        site::$sphinx->loadWebsiteFilters();
        $filters = array();
        $filters[$classification->typeID] = array($classification);
        
        $data = self::setSphinxData(site::$sphinx, $filters, $limitFrom, $nbResultPerPage, true, /*array("title" => 25, "description" => 10, "companyName" => 1)*/[]);
        
        //get the results
        $res = site::sphinxExec(site::$sphinx, $data['q'], $data['sphinxIndexName']);
        
        //if an error was found we throw an exception
        if ($res['error'] != 0) {
            throw new sphinxException("a sphinx exception has just occured : " . var_export($res, true));
        }
        if(site::$sphinx->_error!=0){
            throw new sphinxException("a sphinx exception has just occured : " . var_export(site::$sphinx, true));
        } 
        
        $this->foundRows = $res['total_found'];
        
        return $this->foundRows>0 ? true : false;
        
    }
    
    /**
     * 
     * @param \JOBSEARCH\COMPONENT\classificationP $classification
     * @return boolean
     * @throws sphinxException
     */
    public function nbResults(\JOBSEARCH\COMPONENT\classificationP $classification){
        site::intialiseSphinxConnection();
        $limitFrom = 0;
        $nbResultPerPage = (int) NB_RESULTS_PER_PAGE;
        site::$sphinx->loadWebsiteFilters();
        $filters = array();
        $filters[$classification->typeID] = array($classification);
        
        $data = self::setSphinxData(site::$sphinx, $filters, $limitFrom, $nbResultPerPage, true, /*array("title" => 25, "description" => 10, "companyName" => 1)*/[]);
        
        //get the results
        $res = site::sphinxExec(site::$sphinx, $data['q'], $data['sphinxIndexName']);
        
        //if an error was found we throw an exception
        if ($res['error'] != 0) {
            throw new sphinxException("a sphinx exception has just occured : " . var_export($res, true));
        }
        if(site::$sphinx->_error!=0){
            throw new sphinxException("a sphinx exception has just occured : " . var_export(site::$sphinx, true));
        } 
        
        $this->foundRows = $res['total_found'];
        
        return $this->foundRows;
        
    }
    
    /**
     * Make a usual Search -
     * @throws sphinxException
     * @param boolean $all Do we want to display all the results or not
     * @param mixed array $jobsToExclude .. if we did a previous search in the same page we want to avoid duplicates..
     * @return \SphinxSearch $sphinx used b the search
     */
    public function usualSearch($all=false) {
        //to benchmark the time taken
        $t1 = microtime(true);
        site::intialiseSphinxConnection();

        //compute the sphinx limit filter
        if($all===false){

            $page = GET__LITERAL__PAGE()-1;
            if($page<0){
                $page=0;
            }
            $limitFrom = (int) $page * (int) NB_RESULTS_PER_PAGE;
            $nbResultPerPage = (int) NB_RESULTS_PER_PAGE;
        }
        else{
            $limitFrom=0;
            $nbResultPerPage=1000;
        }
        /**
         * Load Website Preferences - classification definition + sources
         */
        site::$sphinx->loadWebsiteFilters();

        $filters = array();

        $classifications = \GET__JOBSEARCH_COMPONENT_classificationP();
        if($classifications!==null){
            $this->searchParameters = $classifications;
        }
        else{
            $this->searchParameters = [];
        }

        if(is_array($classifications)){
            foreach($classifications as $classification){
                if($classification->belongsToWebsite(\CORE\globalData::$siteConfig->website)){
                    //overide it, so we got only one search parameter
                    $filters[$classification->typeID] = array($classification);
                }
            }
        }

        //keywords
        $keywords = \GET__JOBSEARCH_COMPONENT_keyword();
        if($keywords!=null  && count($keywords)>0){
            $keywords = $keywords[0];
        }
        else{
            $keywords = null;
        }
        if($keywords!=null  && $keywords->getText() != ""){
            $filters["keywords"] =  $keywords->getSphinxText();
        }
        
        /**
         * @todo SORTING MODE IMPLEMENTATION
         */
        //init sphinx with the filters
        $data = self::setSphinxData(site::$sphinx, $filters, $limitFrom, $nbResultPerPage, true, /*array("title" => 25, "description" => 10, "companyName" => 1)*/[]);
        
        //get the results
        $res = site::sphinxExec(site::$sphinx, $data['q'], $data['sphinxIndexName']);
        
        //if an error was found we throw an exception
        if ($res['error'] != 0) {
            throw new sphinxException("a sphinx exception has just occured : " . var_export($res, true));
        }
        if(site::$sphinx->_error!=0){
            throw new sphinxException("a sphinx exception has just occured : " . var_export(site::$sphinx, true));
        } 


        //otherwise init the class params
        $this->foundRows = $res['total_found'];
        if ($this->foundRows > ( $limitFrom + (int) $nbResultPerPage)) {
            $this->endList = false;
        } else {
            $this->endList = true;
        }

        $this->formatSphinxResults($res);

        site::$registry->set("foundRows", $this->foundRows);

        //end of the time taken
        $this->searchTime = microtime(true) - $t1;
        return site::$sphinx;
    }


    /**
     * generate the RSS search link
     * @return \CORE\link
     */
    public function getRSS() {
        $link = url::generateRSSData(count(parser::$locations) == 1 ? parser::$locations[0] : null
                        , count(parser::$classifications) == 1 ? parser::$classifications[0] : null
                        , parser::$keywords
                        , parser::$companyName);
        return $link;
    }

    /**
     * generate the adSense F





    or search Query
     * @return string
     */
    public function generateAFSQuery() {
        $terms = array("job");
        if (parser::$keywords != null) {
            $terms[] = parser::$keywords->getText();
        }
        if (count(parser::$classifications) > 0) {
            foreach (parser::$classifications as $classification) {
                $terms[] = $classification->name;
            }
        }
        if (count(parser::$locations) > 0) {
            foreach (parser::$locations as $location) {
                $terms[] = $location->shortName;
            }
        }
        return implode(",", $terms);
    }

}


?>
