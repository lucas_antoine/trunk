<?php
/**
 * @author Antoine LUCAS <cooluhuru@gmail.com>
 * @copyright 2013 Antoine LUCAS
 * @license Exclusive Use by Antoine LUCAS. This code can't be used by anyone else.
 * 
 */
namespace JOBSEARCH\COMPONENT;

class refineTree {
    
    public $parent=null;
    public $current=null;
    public $childs=array();
    
    
    public function setParent($link,$nb){
        $this->parent = new refineElement($link,$nb);
    }
    public function setCurrent($link,$nb,$name){
        $this->current = new refineElement($link,$nb,null,$name);
    }
    public function addChild($link,$hideLink,$nb, $forceName=null){
        $this->childs[]=new refineElement($link,$nb,$hideLink, $forceName);
    }
    public function isNull(){
        if($this->parent==null && $this->childs==array()){
            return true;
        }
        return false;
    }
    
}

class refineElement{
    
    public $link;
    public $nb;
    public $name;
    public $hideLink;
    
    public function __construct($link,$nb,$hideLink=null,$name=null){
        $this->link=$link;
        $this->nb=$nb;
        $this->name=$name;
        $this->hideLink = $hideLink;
    }
    
    
}
?>
