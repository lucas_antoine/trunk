<?php

namespace MODELS\WEBSITES;

use MODELS\WEBSITES\om\BasedefaultClassificationOriginsPeer;


/**
 * Skeleton subclass for performing query and update operations on the 'defaultClassificationOrigins' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.6.7 on:
 *
 * Wed Nov 20 22:42:51 2013
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.websites
 */
class defaultClassificationOriginsPeer extends BasedefaultClassificationOriginsPeer
{
}
