<?php

namespace MODELS\WEBSITES\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use MODELS\WEBSITES\containerWidgets;
use MODELS\WEBSITES\containers;
use MODELS\WEBSITES\containersPeer;
use MODELS\WEBSITES\containersQuery;
use MODELS\WEBSITES\row;

/**
 * Base class that represents a query for the 'containers' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.6.7 on:
 *
 * Sun Feb 16 17:09:56 2014
 *
 * @method containersQuery orderByid($order = Criteria::ASC) Order by the id column
 * @method containersQuery orderBycolumns($order = Criteria::ASC) Order by the columns column
 * @method containersQuery orderByrowID($order = Criteria::ASC) Order by the rowID column
 *
 * @method containersQuery groupByid() Group by the id column
 * @method containersQuery groupBycolumns() Group by the columns column
 * @method containersQuery groupByrowID() Group by the rowID column
 *
 * @method containersQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method containersQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method containersQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method containersQuery leftJoinrowRelatedByrowID($relationAlias = null) Adds a LEFT JOIN clause to the query using the rowRelatedByrowID relation
 * @method containersQuery rightJoinrowRelatedByrowID($relationAlias = null) Adds a RIGHT JOIN clause to the query using the rowRelatedByrowID relation
 * @method containersQuery innerJoinrowRelatedByrowID($relationAlias = null) Adds a INNER JOIN clause to the query using the rowRelatedByrowID relation
 *
 * @method containersQuery leftJoincontainerWidgets($relationAlias = null) Adds a LEFT JOIN clause to the query using the containerWidgets relation
 * @method containersQuery rightJoincontainerWidgets($relationAlias = null) Adds a RIGHT JOIN clause to the query using the containerWidgets relation
 * @method containersQuery innerJoincontainerWidgets($relationAlias = null) Adds a INNER JOIN clause to the query using the containerWidgets relation
 *
 * @method containersQuery leftJoinrowRelatedBycontainerID($relationAlias = null) Adds a LEFT JOIN clause to the query using the rowRelatedBycontainerID relation
 * @method containersQuery rightJoinrowRelatedBycontainerID($relationAlias = null) Adds a RIGHT JOIN clause to the query using the rowRelatedBycontainerID relation
 * @method containersQuery innerJoinrowRelatedBycontainerID($relationAlias = null) Adds a INNER JOIN clause to the query using the rowRelatedBycontainerID relation
 *
 * @method containers findOne(PropelPDO $con = null) Return the first containers matching the query
 * @method containers findOneOrCreate(PropelPDO $con = null) Return the first containers matching the query, or a new containers object populated from the query conditions when no match is found
 *
 * @method containers findOneByid(int $id) Return the first containers filtered by the id column
 * @method containers findOneBycolumns(int $columns) Return the first containers filtered by the columns column
 * @method containers findOneByrowID(int $rowID) Return the first containers filtered by the rowID column
 *
 * @method array findByid(int $id) Return containers objects filtered by the id column
 * @method array findBycolumns(int $columns) Return containers objects filtered by the columns column
 * @method array findByrowID(int $rowID) Return containers objects filtered by the rowID column
 *
 * @package    propel.generator.websites.om
 */
abstract class BasecontainersQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BasecontainersQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'WEBSITES2', $modelName = 'MODELS\\WEBSITES\\containers', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new containersQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     containersQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return containersQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof containersQuery) {
            return $criteria;
        }
        $query = new containersQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   containers|containers[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = containersPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(containersPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   containers A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `COLUMNS`, `ROWID` FROM `containers` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new containers();
            $obj->hydrate($row);
            containersPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return containers|containers[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|containers[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return containersQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(containersPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return containersQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(containersPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterByid(1234); // WHERE id = 1234
     * $query->filterByid(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterByid(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return containersQuery The current query, for fluid interface
     */
    public function filterByid($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(containersPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the columns column
     *
     * Example usage:
     * <code>
     * $query->filterBycolumns(1234); // WHERE columns = 1234
     * $query->filterBycolumns(array(12, 34)); // WHERE columns IN (12, 34)
     * $query->filterBycolumns(array('min' => 12)); // WHERE columns > 12
     * </code>
     *
     * @param     mixed $columns The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return containersQuery The current query, for fluid interface
     */
    public function filterBycolumns($columns = null, $comparison = null)
    {
        if (is_array($columns)) {
            $useMinMax = false;
            if (isset($columns['min'])) {
                $this->addUsingAlias(containersPeer::COLUMNS, $columns['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($columns['max'])) {
                $this->addUsingAlias(containersPeer::COLUMNS, $columns['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(containersPeer::COLUMNS, $columns, $comparison);
    }

    /**
     * Filter the query on the rowID column
     *
     * Example usage:
     * <code>
     * $query->filterByrowID(1234); // WHERE rowID = 1234
     * $query->filterByrowID(array(12, 34)); // WHERE rowID IN (12, 34)
     * $query->filterByrowID(array('min' => 12)); // WHERE rowID > 12
     * </code>
     *
     * @see       filterByrowRelatedByrowID()
     *
     * @param     mixed $rowID The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return containersQuery The current query, for fluid interface
     */
    public function filterByrowID($rowID = null, $comparison = null)
    {
        if (is_array($rowID)) {
            $useMinMax = false;
            if (isset($rowID['min'])) {
                $this->addUsingAlias(containersPeer::ROWID, $rowID['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($rowID['max'])) {
                $this->addUsingAlias(containersPeer::ROWID, $rowID['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(containersPeer::ROWID, $rowID, $comparison);
    }

    /**
     * Filter the query by a related row object
     *
     * @param   row|PropelObjectCollection $row The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   containersQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByrowRelatedByrowID($row, $comparison = null)
    {
        if ($row instanceof row) {
            return $this
                ->addUsingAlias(containersPeer::ROWID, $row->getid(), $comparison);
        } elseif ($row instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(containersPeer::ROWID, $row->toKeyValue('PrimaryKey', 'id'), $comparison);
        } else {
            throw new PropelException('filterByrowRelatedByrowID() only accepts arguments of type row or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the rowRelatedByrowID relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return containersQuery The current query, for fluid interface
     */
    public function joinrowRelatedByrowID($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('rowRelatedByrowID');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'rowRelatedByrowID');
        }

        return $this;
    }

    /**
     * Use the rowRelatedByrowID relation row object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MODELS\WEBSITES\rowQuery A secondary query class using the current class as primary query
     */
    public function userowRelatedByrowIDQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinrowRelatedByrowID($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'rowRelatedByrowID', '\MODELS\WEBSITES\rowQuery');
    }

    /**
     * Filter the query by a related containerWidgets object
     *
     * @param   containerWidgets|PropelObjectCollection $containerWidgets  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   containersQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBycontainerWidgets($containerWidgets, $comparison = null)
    {
        if ($containerWidgets instanceof containerWidgets) {
            return $this
                ->addUsingAlias(containersPeer::ID, $containerWidgets->getcontainerID(), $comparison);
        } elseif ($containerWidgets instanceof PropelObjectCollection) {
            return $this
                ->usecontainerWidgetsQuery()
                ->filterByPrimaryKeys($containerWidgets->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBycontainerWidgets() only accepts arguments of type containerWidgets or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the containerWidgets relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return containersQuery The current query, for fluid interface
     */
    public function joincontainerWidgets($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('containerWidgets');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'containerWidgets');
        }

        return $this;
    }

    /**
     * Use the containerWidgets relation containerWidgets object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MODELS\WEBSITES\containerWidgetsQuery A secondary query class using the current class as primary query
     */
    public function usecontainerWidgetsQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joincontainerWidgets($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'containerWidgets', '\MODELS\WEBSITES\containerWidgetsQuery');
    }

    /**
     * Filter the query by a related row object
     *
     * @param   row|PropelObjectCollection $row  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   containersQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByrowRelatedBycontainerID($row, $comparison = null)
    {
        if ($row instanceof row) {
            return $this
                ->addUsingAlias(containersPeer::ID, $row->getcontainerID(), $comparison);
        } elseif ($row instanceof PropelObjectCollection) {
            return $this
                ->userowRelatedBycontainerIDQuery()
                ->filterByPrimaryKeys($row->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByrowRelatedBycontainerID() only accepts arguments of type row or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the rowRelatedBycontainerID relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return containersQuery The current query, for fluid interface
     */
    public function joinrowRelatedBycontainerID($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('rowRelatedBycontainerID');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'rowRelatedBycontainerID');
        }

        return $this;
    }

    /**
     * Use the rowRelatedBycontainerID relation row object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MODELS\WEBSITES\rowQuery A secondary query class using the current class as primary query
     */
    public function userowRelatedBycontainerIDQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinrowRelatedBycontainerID($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'rowRelatedBycontainerID', '\MODELS\WEBSITES\rowQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   containers $containers Object to remove from the list of results
     *
     * @return containersQuery The current query, for fluid interface
     */
    public function prune($containers = null)
    {
        if ($containers) {
            $this->addUsingAlias(containersPeer::ID, $containers->getid(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
