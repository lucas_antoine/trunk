<?php

namespace MODELS\WEBSITES\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \PDO;
use \Propel;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use MODELS\WEBSITES\defaultSources;
use MODELS\WEBSITES\defaultSourcesPeer;
use MODELS\WEBSITES\defaultSourcesQuery;

/**
 * Base class that represents a query for the 'defaultSources' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.6.7 on:
 *
 * Wed Nov 20 05:13:39 2013
 *
 * @method defaultSourcesQuery orderBytypeID($order = Criteria::ASC) Order by the typeID column
 * @method defaultSourcesQuery orderBysourceID($order = Criteria::ASC) Order by the sourceID column
 *
 * @method defaultSourcesQuery groupBytypeID() Group by the typeID column
 * @method defaultSourcesQuery groupBysourceID() Group by the sourceID column
 *
 * @method defaultSourcesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method defaultSourcesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method defaultSourcesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method defaultSources findOne(PropelPDO $con = null) Return the first defaultSources matching the query
 * @method defaultSources findOneOrCreate(PropelPDO $con = null) Return the first defaultSources matching the query, or a new defaultSources object populated from the query conditions when no match is found
 *
 * @method defaultSources findOneBytypeID(int $typeID) Return the first defaultSources filtered by the typeID column
 * @method defaultSources findOneBysourceID(int $sourceID) Return the first defaultSources filtered by the sourceID column
 *
 * @method array findBytypeID(int $typeID) Return defaultSources objects filtered by the typeID column
 * @method array findBysourceID(int $sourceID) Return defaultSources objects filtered by the sourceID column
 *
 * @package    propel.generator.websites.om
 */
abstract class BasedefaultSourcesQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BasedefaultSourcesQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'WEBSITES2', $modelName = 'MODELS\\WEBSITES\\defaultSources', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new defaultSourcesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     defaultSourcesQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return defaultSourcesQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof defaultSourcesQuery) {
            return $criteria;
        }
        $query = new defaultSourcesQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   defaultSources|defaultSources[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = defaultSourcesPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(defaultSourcesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   defaultSources A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `TYPEID`, `SOURCEID` FROM `defaultSources` WHERE `TYPEID` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new defaultSources();
            $obj->hydrate($row);
            defaultSourcesPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return defaultSources|defaultSources[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|defaultSources[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return defaultSourcesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(defaultSourcesPeer::TYPEID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return defaultSourcesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(defaultSourcesPeer::TYPEID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the typeID column
     *
     * Example usage:
     * <code>
     * $query->filterBytypeID(1234); // WHERE typeID = 1234
     * $query->filterBytypeID(array(12, 34)); // WHERE typeID IN (12, 34)
     * $query->filterBytypeID(array('min' => 12)); // WHERE typeID > 12
     * </code>
     *
     * @param     mixed $typeID The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return defaultSourcesQuery The current query, for fluid interface
     */
    public function filterBytypeID($typeID = null, $comparison = null)
    {
        if (is_array($typeID) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(defaultSourcesPeer::TYPEID, $typeID, $comparison);
    }

    /**
     * Filter the query on the sourceID column
     *
     * Example usage:
     * <code>
     * $query->filterBysourceID(1234); // WHERE sourceID = 1234
     * $query->filterBysourceID(array(12, 34)); // WHERE sourceID IN (12, 34)
     * $query->filterBysourceID(array('min' => 12)); // WHERE sourceID > 12
     * </code>
     *
     * @param     mixed $sourceID The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return defaultSourcesQuery The current query, for fluid interface
     */
    public function filterBysourceID($sourceID = null, $comparison = null)
    {
        if (is_array($sourceID)) {
            $useMinMax = false;
            if (isset($sourceID['min'])) {
                $this->addUsingAlias(defaultSourcesPeer::SOURCEID, $sourceID['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sourceID['max'])) {
                $this->addUsingAlias(defaultSourcesPeer::SOURCEID, $sourceID['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(defaultSourcesPeer::SOURCEID, $sourceID, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   defaultSources $defaultSources Object to remove from the list of results
     *
     * @return defaultSourcesQuery The current query, for fluid interface
     */
    public function prune($defaultSources = null)
    {
        if ($defaultSources) {
            $this->addUsingAlias(defaultSourcesPeer::TYPEID, $defaultSources->gettypeID(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
