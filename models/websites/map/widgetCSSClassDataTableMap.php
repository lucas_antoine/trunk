<?php

namespace MODELS\WEBSITES\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'widgetCSSClassData' table.
 *
 *
 * This class was autogenerated by Propel 1.6.7 on:
 *
 * Sun Feb 16 17:09:57 2014
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.websites.map
 */
class widgetCSSClassDataTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'websites.map.widgetCSSClassDataTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('widgetCSSClassData');
        $this->setPhpName('widgetCSSClassData');
        $this->setClassname('MODELS\\WEBSITES\\widgetCSSClassData');
        $this->setPackage('websites');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ID', 'id', 'INTEGER', true, null, null);
        $this->addForeignKey('WIDGETCSSCLASSID', 'widgetCSSClassID', 'INTEGER', 'widgetCSSClassName', 'ID', false, null, null);
        $this->addForeignKey('WEBSITEID', 'websiteID', 'INTEGER', 'websites', 'ID', false, null, null);
        $this->addColumn('CLASSNAME', 'className', 'VARCHAR', false, 45, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('websites', 'MODELS\\WEBSITES\\websites', RelationMap::MANY_TO_ONE, array('websiteID' => 'id', ), 'CASCADE', 'CASCADE');
        $this->addRelation('widgetCSSClassName', 'MODELS\\WEBSITES\\widgetCSSClassName', RelationMap::MANY_TO_ONE, array('widgetCSSClassID' => 'id', ), 'CASCADE', 'CASCADE');
    } // buildRelations()

} // widgetCSSClassDataTableMap
