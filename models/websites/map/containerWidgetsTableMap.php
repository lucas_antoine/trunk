<?php

namespace MODELS\WEBSITES\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'containerWidgets' table.
 *
 *
 * This class was autogenerated by Propel 1.6.7 on:
 *
 * Sun Feb 16 17:09:55 2014
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.websites.map
 */
class containerWidgetsTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'websites.map.containerWidgetsTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('containerWidgets');
        $this->setPhpName('containerWidgets');
        $this->setClassname('MODELS\\WEBSITES\\containerWidgets');
        $this->setPackage('websites');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ID', 'id', 'INTEGER', true, null, null);
        $this->addForeignKey('CONTAINERID', 'containerID', 'INTEGER', 'containers', 'ID', true, null, null);
        $this->addForeignKey('WIDGETTHEMEID', 'widgetThemeID', 'INTEGER', 'widgetThemes', 'ID', true, null, null);
        $this->addColumn('POSITION', 'position', 'INTEGER', false, null, null);
        $this->addColumn('ALIGN', 'align', 'CHAR', false, null, 'left');
        $this->addColumn('BACKGROUNDCOLOR', 'backgroundColor', 'VARCHAR', false, 45, null);
        $this->addColumn('BACKGROUNDPOSITION', 'backgroundPosition', 'VARCHAR', false, 45, null);
        $this->addColumn('BACKGROUNDREPEAT', 'backgroundRepeat', 'VARCHAR', true, 45, 'no-repeat');
        $this->addColumn('BORDERTOPSIZE', 'borderTopSize', 'INTEGER', true, null, 0);
        $this->addColumn('BORDERRIGHTSIZE', 'borderRightSize', 'INTEGER', true, null, 0);
        $this->addColumn('BORDERBOTTOMSIZE', 'borderBottomSize', 'INTEGER', true, null, 0);
        $this->addColumn('BORDERLEFTSIZE', 'borderLeftSize', 'INTEGER', true, null, 0);
        $this->addColumn('BORDERTOPLEFTRADIUS', 'borderTopLeftRadius', 'INTEGER', true, null, 0);
        $this->addColumn('BORDERTOPRIGHTRADIUS', 'borderTopRightRadius', 'INTEGER', true, null, 0);
        $this->addColumn('BORDERBOTTOMRIGHTRADIUS', 'borderBottomRightRadius', 'INTEGER', true, null, 0);
        $this->addColumn('BORDERBOTTOMLEFTRADIUS', 'borderBottomLeftRadius', 'INTEGER', true, null, 0);
        $this->addColumn('BORDERTOPCOLOR', 'borderTopColor', 'VARCHAR', false, 45, null);
        $this->addColumn('BORDERRIGHTCOLOR', 'borderRightColor', 'VARCHAR', false, 45, null);
        $this->addColumn('BORDERBOTTOMCOLOR', 'borderBottomColor', 'VARCHAR', false, 45, null);
        $this->addColumn('BORDERLEFTCOLOR', 'borderLeftColor', 'VARCHAR', false, 45, null);
        $this->addColumn('BORDERTOPSTYLE', 'borderTopStyle', 'VARCHAR', true, 45, 'solid');
        $this->addColumn('BORDERRIGHTSTYLE', 'borderRightStyle', 'VARCHAR', true, 45, 'solid');
        $this->addColumn('BORDERBOTTOMSTYLE', 'borderBottomStyle', 'VARCHAR', true, 45, 'solid');
        $this->addColumn('BORDERLEFTSTYLE', 'borderLeftStyle', 'VARCHAR', true, 45, 'solid');
        $this->addColumn('MARGINRIGHT', 'marginRight', 'INTEGER', true, null, 0);
        $this->addColumn('MARGINBOTTOM', 'marginBottom', 'INTEGER', true, null, 0);
        $this->addColumn('MARGINTOP', 'marginTop', 'INTEGER', true, null, 0);
        $this->addColumn('MARGINLEFT', 'marginLeft', 'INTEGER', true, null, 0);
        $this->addColumn('PADDINGTOP', 'paddingTop', 'INTEGER', true, null, 0);
        $this->addColumn('PADDINGRIGHT', 'paddingRight', 'INTEGER', true, null, 0);
        $this->addColumn('PADDINGBOTTOM', 'paddingBottom', 'INTEGER', true, null, 0);
        $this->addColumn('PADDINGLEFT', 'paddingLeft', 'INTEGER', true, null, 0);
        $this->addColumn('BACKGROUNDIMAGE', 'backgroundImage', 'BOOLEAN', true, 1, false);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('containers', 'MODELS\\WEBSITES\\containers', RelationMap::MANY_TO_ONE, array('containerID' => 'id', ), 'CASCADE', 'CASCADE');
        $this->addRelation('widgetThemes', 'MODELS\\WEBSITES\\widgetThemes', RelationMap::MANY_TO_ONE, array('widgetThemeID' => 'id', ), 'CASCADE', 'CASCADE');
        $this->addRelation('containerWidgetOptions', 'MODELS\\WEBSITES\\containerWidgetOptions', RelationMap::ONE_TO_MANY, array('id' => 'containerWidgetID', ), 'CASCADE', 'CASCADE', 'containerWidgetOptionss');
        $this->addRelation('widgetStyles', 'MODELS\\WEBSITES\\widgetStyles', RelationMap::ONE_TO_MANY, array('id' => 'containerWidgetID', ), 'CASCADE', 'CASCADE', 'widgetStyless');
    } // buildRelations()

} // containerWidgetsTableMap
