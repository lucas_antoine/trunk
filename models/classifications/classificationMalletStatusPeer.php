<?php

namespace MODELS\CLASSIFICATIONS;

use MODELS\CLASSIFICATIONS\om\BaseclassificationMalletStatusPeer;


/**
 * Skeleton subclass for performing query and update operations on the 'classificationMalletStatus' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.6.7 on:
 *
 * Mon Dec 23 22:54:00 2013
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.classifications
 */
class classificationMalletStatusPeer extends BaseclassificationMalletStatusPeer
{
}
