<?php

namespace MODELS\CLASSIFICATIONS\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use MODELS\CLASSIFICATIONS\classificationOriginProperties;
use MODELS\CLASSIFICATIONS\classifications;
use MODELS\CLASSIFICATIONS\sources;
use MODELS\CLASSIFICATIONS\sourcesPeer;
use MODELS\CLASSIFICATIONS\sourcesQuery;

/**
 * Base class that represents a query for the 'sources' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.6.7 on:
 *
 * Wed Nov 20 05:13:33 2013
 *
 * @method sourcesQuery orderByid($order = Criteria::ASC) Order by the id column
 * @method sourcesQuery orderByname($order = Criteria::ASC) Order by the name column
 *
 * @method sourcesQuery groupByid() Group by the id column
 * @method sourcesQuery groupByname() Group by the name column
 *
 * @method sourcesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method sourcesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method sourcesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method sourcesQuery leftJoinclassificationOriginProperties($relationAlias = null) Adds a LEFT JOIN clause to the query using the classificationOriginProperties relation
 * @method sourcesQuery rightJoinclassificationOriginProperties($relationAlias = null) Adds a RIGHT JOIN clause to the query using the classificationOriginProperties relation
 * @method sourcesQuery innerJoinclassificationOriginProperties($relationAlias = null) Adds a INNER JOIN clause to the query using the classificationOriginProperties relation
 *
 * @method sourcesQuery leftJoinclassifications($relationAlias = null) Adds a LEFT JOIN clause to the query using the classifications relation
 * @method sourcesQuery rightJoinclassifications($relationAlias = null) Adds a RIGHT JOIN clause to the query using the classifications relation
 * @method sourcesQuery innerJoinclassifications($relationAlias = null) Adds a INNER JOIN clause to the query using the classifications relation
 *
 * @method sources findOne(PropelPDO $con = null) Return the first sources matching the query
 * @method sources findOneOrCreate(PropelPDO $con = null) Return the first sources matching the query, or a new sources object populated from the query conditions when no match is found
 *
 * @method sources findOneByid(int $id) Return the first sources filtered by the id column
 * @method sources findOneByname(string $name) Return the first sources filtered by the name column
 *
 * @method array findByid(int $id) Return sources objects filtered by the id column
 * @method array findByname(string $name) Return sources objects filtered by the name column
 *
 * @package    propel.generator.classifications.om
 */
abstract class BasesourcesQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BasesourcesQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'CLASSIFICATIONS2', $modelName = 'MODELS\\CLASSIFICATIONS\\sources', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new sourcesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     sourcesQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return sourcesQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof sourcesQuery) {
            return $criteria;
        }
        $query = new sourcesQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   sources|sources[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = sourcesPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(sourcesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   sources A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `NAME` FROM `sources` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new sources();
            $obj->hydrate($row);
            sourcesPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return sources|sources[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|sources[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return sourcesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(sourcesPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return sourcesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(sourcesPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterByid(1234); // WHERE id = 1234
     * $query->filterByid(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterByid(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return sourcesQuery The current query, for fluid interface
     */
    public function filterByid($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(sourcesPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByname('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByname('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return sourcesQuery The current query, for fluid interface
     */
    public function filterByname($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(sourcesPeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query by a related classificationOriginProperties object
     *
     * @param   classificationOriginProperties|PropelObjectCollection $classificationOriginProperties  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   sourcesQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByclassificationOriginProperties($classificationOriginProperties, $comparison = null)
    {
        if ($classificationOriginProperties instanceof classificationOriginProperties) {
            return $this
                ->addUsingAlias(sourcesPeer::ID, $classificationOriginProperties->getsourceID(), $comparison);
        } elseif ($classificationOriginProperties instanceof PropelObjectCollection) {
            return $this
                ->useclassificationOriginPropertiesQuery()
                ->filterByPrimaryKeys($classificationOriginProperties->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByclassificationOriginProperties() only accepts arguments of type classificationOriginProperties or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the classificationOriginProperties relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return sourcesQuery The current query, for fluid interface
     */
    public function joinclassificationOriginProperties($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('classificationOriginProperties');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'classificationOriginProperties');
        }

        return $this;
    }

    /**
     * Use the classificationOriginProperties relation classificationOriginProperties object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MODELS\CLASSIFICATIONS\classificationOriginPropertiesQuery A secondary query class using the current class as primary query
     */
    public function useclassificationOriginPropertiesQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinclassificationOriginProperties($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'classificationOriginProperties', '\MODELS\CLASSIFICATIONS\classificationOriginPropertiesQuery');
    }

    /**
     * Filter the query by a related classifications object
     *
     * @param   classifications|PropelObjectCollection $classifications  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   sourcesQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByclassifications($classifications, $comparison = null)
    {
        if ($classifications instanceof classifications) {
            return $this
                ->addUsingAlias(sourcesPeer::ID, $classifications->getsourceID(), $comparison);
        } elseif ($classifications instanceof PropelObjectCollection) {
            return $this
                ->useclassificationsQuery()
                ->filterByPrimaryKeys($classifications->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByclassifications() only accepts arguments of type classifications or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the classifications relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return sourcesQuery The current query, for fluid interface
     */
    public function joinclassifications($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('classifications');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'classifications');
        }

        return $this;
    }

    /**
     * Use the classifications relation classifications object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MODELS\CLASSIFICATIONS\classificationsQuery A secondary query class using the current class as primary query
     */
    public function useclassificationsQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinclassifications($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'classifications', '\MODELS\CLASSIFICATIONS\classificationsQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   sources $sources Object to remove from the list of results
     *
     * @return sourcesQuery The current query, for fluid interface
     */
    public function prune($sources = null)
    {
        if ($sources) {
            $this->addUsingAlias(sourcesPeer::ID, $sources->getid(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
