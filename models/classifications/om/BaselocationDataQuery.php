<?php

namespace MODELS\CLASSIFICATIONS\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use MODELS\CLASSIFICATIONS\classifications;
use MODELS\CLASSIFICATIONS\locationData;
use MODELS\CLASSIFICATIONS\locationDataPeer;
use MODELS\CLASSIFICATIONS\locationDataQuery;

/**
 * Base class that represents a query for the 'locationData' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.6.7 on:
 *
 * Sun Feb 16 17:09:49 2014
 *
 * @method locationDataQuery orderByid($order = Criteria::ASC) Order by the id column
 * @method locationDataQuery orderBylongitude($order = Criteria::ASC) Order by the longitude column
 * @method locationDataQuery orderBylatitude($order = Criteria::ASC) Order by the latitude column
 * @method locationDataQuery orderByoffsetLon($order = Criteria::ASC) Order by the offsetLon column
 * @method locationDataQuery orderByoffsetLat($order = Criteria::ASC) Order by the offsetLat column
 * @method locationDataQuery orderByzip($order = Criteria::ASC) Order by the zip column
 *
 * @method locationDataQuery groupByid() Group by the id column
 * @method locationDataQuery groupBylongitude() Group by the longitude column
 * @method locationDataQuery groupBylatitude() Group by the latitude column
 * @method locationDataQuery groupByoffsetLon() Group by the offsetLon column
 * @method locationDataQuery groupByoffsetLat() Group by the offsetLat column
 * @method locationDataQuery groupByzip() Group by the zip column
 *
 * @method locationDataQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method locationDataQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method locationDataQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method locationDataQuery leftJoinclassifications($relationAlias = null) Adds a LEFT JOIN clause to the query using the classifications relation
 * @method locationDataQuery rightJoinclassifications($relationAlias = null) Adds a RIGHT JOIN clause to the query using the classifications relation
 * @method locationDataQuery innerJoinclassifications($relationAlias = null) Adds a INNER JOIN clause to the query using the classifications relation
 *
 * @method locationData findOne(PropelPDO $con = null) Return the first locationData matching the query
 * @method locationData findOneOrCreate(PropelPDO $con = null) Return the first locationData matching the query, or a new locationData object populated from the query conditions when no match is found
 *
 * @method locationData findOneByid(int $id) Return the first locationData filtered by the id column
 * @method locationData findOneBylongitude(double $longitude) Return the first locationData filtered by the longitude column
 * @method locationData findOneBylatitude(double $latitude) Return the first locationData filtered by the latitude column
 * @method locationData findOneByoffsetLon(double $offsetLon) Return the first locationData filtered by the offsetLon column
 * @method locationData findOneByoffsetLat(double $offsetLat) Return the first locationData filtered by the offsetLat column
 * @method locationData findOneByzip(string $zip) Return the first locationData filtered by the zip column
 *
 * @method array findByid(int $id) Return locationData objects filtered by the id column
 * @method array findBylongitude(double $longitude) Return locationData objects filtered by the longitude column
 * @method array findBylatitude(double $latitude) Return locationData objects filtered by the latitude column
 * @method array findByoffsetLon(double $offsetLon) Return locationData objects filtered by the offsetLon column
 * @method array findByoffsetLat(double $offsetLat) Return locationData objects filtered by the offsetLat column
 * @method array findByzip(string $zip) Return locationData objects filtered by the zip column
 *
 * @package    propel.generator.classifications.om
 */
abstract class BaselocationDataQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaselocationDataQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'CLASSIFICATIONS2', $modelName = 'MODELS\\CLASSIFICATIONS\\locationData', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new locationDataQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     locationDataQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return locationDataQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof locationDataQuery) {
            return $criteria;
        }
        $query = new locationDataQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   locationData|locationData[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = locationDataPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(locationDataPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   locationData A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `LONGITUDE`, `LATITUDE`, `OFFSETLON`, `OFFSETLAT`, `ZIP` FROM `locationData` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new locationData();
            $obj->hydrate($row);
            locationDataPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return locationData|locationData[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|locationData[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return locationDataQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(locationDataPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return locationDataQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(locationDataPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterByid(1234); // WHERE id = 1234
     * $query->filterByid(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterByid(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @see       filterByclassifications()
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return locationDataQuery The current query, for fluid interface
     */
    public function filterByid($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(locationDataPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the longitude column
     *
     * Example usage:
     * <code>
     * $query->filterBylongitude(1234); // WHERE longitude = 1234
     * $query->filterBylongitude(array(12, 34)); // WHERE longitude IN (12, 34)
     * $query->filterBylongitude(array('min' => 12)); // WHERE longitude > 12
     * </code>
     *
     * @param     mixed $longitude The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return locationDataQuery The current query, for fluid interface
     */
    public function filterBylongitude($longitude = null, $comparison = null)
    {
        if (is_array($longitude)) {
            $useMinMax = false;
            if (isset($longitude['min'])) {
                $this->addUsingAlias(locationDataPeer::LONGITUDE, $longitude['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($longitude['max'])) {
                $this->addUsingAlias(locationDataPeer::LONGITUDE, $longitude['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(locationDataPeer::LONGITUDE, $longitude, $comparison);
    }

    /**
     * Filter the query on the latitude column
     *
     * Example usage:
     * <code>
     * $query->filterBylatitude(1234); // WHERE latitude = 1234
     * $query->filterBylatitude(array(12, 34)); // WHERE latitude IN (12, 34)
     * $query->filterBylatitude(array('min' => 12)); // WHERE latitude > 12
     * </code>
     *
     * @param     mixed $latitude The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return locationDataQuery The current query, for fluid interface
     */
    public function filterBylatitude($latitude = null, $comparison = null)
    {
        if (is_array($latitude)) {
            $useMinMax = false;
            if (isset($latitude['min'])) {
                $this->addUsingAlias(locationDataPeer::LATITUDE, $latitude['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($latitude['max'])) {
                $this->addUsingAlias(locationDataPeer::LATITUDE, $latitude['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(locationDataPeer::LATITUDE, $latitude, $comparison);
    }

    /**
     * Filter the query on the offsetLon column
     *
     * Example usage:
     * <code>
     * $query->filterByoffsetLon(1234); // WHERE offsetLon = 1234
     * $query->filterByoffsetLon(array(12, 34)); // WHERE offsetLon IN (12, 34)
     * $query->filterByoffsetLon(array('min' => 12)); // WHERE offsetLon > 12
     * </code>
     *
     * @param     mixed $offsetLon The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return locationDataQuery The current query, for fluid interface
     */
    public function filterByoffsetLon($offsetLon = null, $comparison = null)
    {
        if (is_array($offsetLon)) {
            $useMinMax = false;
            if (isset($offsetLon['min'])) {
                $this->addUsingAlias(locationDataPeer::OFFSETLON, $offsetLon['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($offsetLon['max'])) {
                $this->addUsingAlias(locationDataPeer::OFFSETLON, $offsetLon['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(locationDataPeer::OFFSETLON, $offsetLon, $comparison);
    }

    /**
     * Filter the query on the offsetLat column
     *
     * Example usage:
     * <code>
     * $query->filterByoffsetLat(1234); // WHERE offsetLat = 1234
     * $query->filterByoffsetLat(array(12, 34)); // WHERE offsetLat IN (12, 34)
     * $query->filterByoffsetLat(array('min' => 12)); // WHERE offsetLat > 12
     * </code>
     *
     * @param     mixed $offsetLat The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return locationDataQuery The current query, for fluid interface
     */
    public function filterByoffsetLat($offsetLat = null, $comparison = null)
    {
        if (is_array($offsetLat)) {
            $useMinMax = false;
            if (isset($offsetLat['min'])) {
                $this->addUsingAlias(locationDataPeer::OFFSETLAT, $offsetLat['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($offsetLat['max'])) {
                $this->addUsingAlias(locationDataPeer::OFFSETLAT, $offsetLat['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(locationDataPeer::OFFSETLAT, $offsetLat, $comparison);
    }

    /**
     * Filter the query on the zip column
     *
     * Example usage:
     * <code>
     * $query->filterByzip('fooValue');   // WHERE zip = 'fooValue'
     * $query->filterByzip('%fooValue%'); // WHERE zip LIKE '%fooValue%'
     * </code>
     *
     * @param     string $zip The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return locationDataQuery The current query, for fluid interface
     */
    public function filterByzip($zip = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($zip)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $zip)) {
                $zip = str_replace('*', '%', $zip);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(locationDataPeer::ZIP, $zip, $comparison);
    }

    /**
     * Filter the query by a related classifications object
     *
     * @param   classifications|PropelObjectCollection $classifications The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   locationDataQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByclassifications($classifications, $comparison = null)
    {
        if ($classifications instanceof classifications) {
            return $this
                ->addUsingAlias(locationDataPeer::ID, $classifications->getid(), $comparison);
        } elseif ($classifications instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(locationDataPeer::ID, $classifications->toKeyValue('PrimaryKey', 'id'), $comparison);
        } else {
            throw new PropelException('filterByclassifications() only accepts arguments of type classifications or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the classifications relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return locationDataQuery The current query, for fluid interface
     */
    public function joinclassifications($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('classifications');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'classifications');
        }

        return $this;
    }

    /**
     * Use the classifications relation classifications object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MODELS\CLASSIFICATIONS\classificationsQuery A secondary query class using the current class as primary query
     */
    public function useclassificationsQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinclassifications($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'classifications', '\MODELS\CLASSIFICATIONS\classificationsQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   locationData $locationData Object to remove from the list of results
     *
     * @return locationDataQuery The current query, for fluid interface
     */
    public function prune($locationData = null)
    {
        if ($locationData) {
            $this->addUsingAlias(locationDataPeer::ID, $locationData->getid(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
