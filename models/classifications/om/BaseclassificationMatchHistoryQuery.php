<?php

namespace MODELS\CLASSIFICATIONS\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use MODELS\CLASSIFICATIONS\classificationMatch;
use MODELS\CLASSIFICATIONS\classificationMatchHistory;
use MODELS\CLASSIFICATIONS\classificationMatchHistoryPeer;
use MODELS\CLASSIFICATIONS\classificationMatchHistoryQuery;

/**
 * Base class that represents a query for the 'classificationMatchHistory' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.6.7 on:
 *
 * Sat Nov 30 08:28:07 2013
 *
 * @method classificationMatchHistoryQuery orderBymanuallyMatchID($order = Criteria::ASC) Order by the manuallyMatchID column
 * @method classificationMatchHistoryQuery orderBypropagatedMatchID($order = Criteria::ASC) Order by the propagatedMatchID column
 *
 * @method classificationMatchHistoryQuery groupBymanuallyMatchID() Group by the manuallyMatchID column
 * @method classificationMatchHistoryQuery groupBypropagatedMatchID() Group by the propagatedMatchID column
 *
 * @method classificationMatchHistoryQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method classificationMatchHistoryQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method classificationMatchHistoryQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method classificationMatchHistoryQuery leftJoinclassificationMatchRelatedBymanuallyMatchID($relationAlias = null) Adds a LEFT JOIN clause to the query using the classificationMatchRelatedBymanuallyMatchID relation
 * @method classificationMatchHistoryQuery rightJoinclassificationMatchRelatedBymanuallyMatchID($relationAlias = null) Adds a RIGHT JOIN clause to the query using the classificationMatchRelatedBymanuallyMatchID relation
 * @method classificationMatchHistoryQuery innerJoinclassificationMatchRelatedBymanuallyMatchID($relationAlias = null) Adds a INNER JOIN clause to the query using the classificationMatchRelatedBymanuallyMatchID relation
 *
 * @method classificationMatchHistoryQuery leftJoinclassificationMatchRelatedBypropagatedMatchID($relationAlias = null) Adds a LEFT JOIN clause to the query using the classificationMatchRelatedBypropagatedMatchID relation
 * @method classificationMatchHistoryQuery rightJoinclassificationMatchRelatedBypropagatedMatchID($relationAlias = null) Adds a RIGHT JOIN clause to the query using the classificationMatchRelatedBypropagatedMatchID relation
 * @method classificationMatchHistoryQuery innerJoinclassificationMatchRelatedBypropagatedMatchID($relationAlias = null) Adds a INNER JOIN clause to the query using the classificationMatchRelatedBypropagatedMatchID relation
 *
 * @method classificationMatchHistory findOne(PropelPDO $con = null) Return the first classificationMatchHistory matching the query
 * @method classificationMatchHistory findOneOrCreate(PropelPDO $con = null) Return the first classificationMatchHistory matching the query, or a new classificationMatchHistory object populated from the query conditions when no match is found
 *
 * @method classificationMatchHistory findOneBymanuallyMatchID(int $manuallyMatchID) Return the first classificationMatchHistory filtered by the manuallyMatchID column
 * @method classificationMatchHistory findOneBypropagatedMatchID(int $propagatedMatchID) Return the first classificationMatchHistory filtered by the propagatedMatchID column
 *
 * @method array findBymanuallyMatchID(int $manuallyMatchID) Return classificationMatchHistory objects filtered by the manuallyMatchID column
 * @method array findBypropagatedMatchID(int $propagatedMatchID) Return classificationMatchHistory objects filtered by the propagatedMatchID column
 *
 * @package    propel.generator.classifications.om
 */
abstract class BaseclassificationMatchHistoryQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseclassificationMatchHistoryQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'CLASSIFICATIONS2', $modelName = 'MODELS\\CLASSIFICATIONS\\classificationMatchHistory', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new classificationMatchHistoryQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     classificationMatchHistoryQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return classificationMatchHistoryQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof classificationMatchHistoryQuery) {
            return $criteria;
        }
        $query = new classificationMatchHistoryQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj = $c->findPk(array(12, 34), $con);
     * </code>
     *
     * @param array $key Primary key to use for the query
                         A Primary key composition: [$manuallyMatchID, $propagatedMatchID]
     * @param     PropelPDO $con an optional connection object
     *
     * @return   classificationMatchHistory|classificationMatchHistory[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = classificationMatchHistoryPeer::getInstanceFromPool(serialize(array((string) $key[0], (string) $key[1]))))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(classificationMatchHistoryPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   classificationMatchHistory A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `MANUALLYMATCHID`, `PROPAGATEDMATCHID` FROM `classificationMatchHistory` WHERE `MANUALLYMATCHID` = :p0 AND `PROPAGATEDMATCHID` = :p1';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key[0], PDO::PARAM_INT);
            $stmt->bindValue(':p1', $key[1], PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new classificationMatchHistory();
            $obj->hydrate($row);
            classificationMatchHistoryPeer::addInstanceToPool($obj, serialize(array((string) $key[0], (string) $key[1])));
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return classificationMatchHistory|classificationMatchHistory[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(array(12, 56), array(832, 123), array(123, 456)), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|classificationMatchHistory[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return classificationMatchHistoryQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {
        $this->addUsingAlias(classificationMatchHistoryPeer::MANUALLYMATCHID, $key[0], Criteria::EQUAL);
        $this->addUsingAlias(classificationMatchHistoryPeer::PROPAGATEDMATCHID, $key[1], Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return classificationMatchHistoryQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {
        if (empty($keys)) {
            return $this->add(null, '1<>1', Criteria::CUSTOM);
        }
        foreach ($keys as $key) {
            $cton0 = $this->getNewCriterion(classificationMatchHistoryPeer::MANUALLYMATCHID, $key[0], Criteria::EQUAL);
            $cton1 = $this->getNewCriterion(classificationMatchHistoryPeer::PROPAGATEDMATCHID, $key[1], Criteria::EQUAL);
            $cton0->addAnd($cton1);
            $this->addOr($cton0);
        }

        return $this;
    }

    /**
     * Filter the query on the manuallyMatchID column
     *
     * Example usage:
     * <code>
     * $query->filterBymanuallyMatchID(1234); // WHERE manuallyMatchID = 1234
     * $query->filterBymanuallyMatchID(array(12, 34)); // WHERE manuallyMatchID IN (12, 34)
     * $query->filterBymanuallyMatchID(array('min' => 12)); // WHERE manuallyMatchID > 12
     * </code>
     *
     * @see       filterByclassificationMatchRelatedBymanuallyMatchID()
     *
     * @param     mixed $manuallyMatchID The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return classificationMatchHistoryQuery The current query, for fluid interface
     */
    public function filterBymanuallyMatchID($manuallyMatchID = null, $comparison = null)
    {
        if (is_array($manuallyMatchID) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(classificationMatchHistoryPeer::MANUALLYMATCHID, $manuallyMatchID, $comparison);
    }

    /**
     * Filter the query on the propagatedMatchID column
     *
     * Example usage:
     * <code>
     * $query->filterBypropagatedMatchID(1234); // WHERE propagatedMatchID = 1234
     * $query->filterBypropagatedMatchID(array(12, 34)); // WHERE propagatedMatchID IN (12, 34)
     * $query->filterBypropagatedMatchID(array('min' => 12)); // WHERE propagatedMatchID > 12
     * </code>
     *
     * @see       filterByclassificationMatchRelatedBypropagatedMatchID()
     *
     * @param     mixed $propagatedMatchID The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return classificationMatchHistoryQuery The current query, for fluid interface
     */
    public function filterBypropagatedMatchID($propagatedMatchID = null, $comparison = null)
    {
        if (is_array($propagatedMatchID) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(classificationMatchHistoryPeer::PROPAGATEDMATCHID, $propagatedMatchID, $comparison);
    }

    /**
     * Filter the query by a related classificationMatch object
     *
     * @param   classificationMatch|PropelObjectCollection $classificationMatch The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   classificationMatchHistoryQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByclassificationMatchRelatedBymanuallyMatchID($classificationMatch, $comparison = null)
    {
        if ($classificationMatch instanceof classificationMatch) {
            return $this
                ->addUsingAlias(classificationMatchHistoryPeer::MANUALLYMATCHID, $classificationMatch->getid(), $comparison);
        } elseif ($classificationMatch instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(classificationMatchHistoryPeer::MANUALLYMATCHID, $classificationMatch->toKeyValue('PrimaryKey', 'id'), $comparison);
        } else {
            throw new PropelException('filterByclassificationMatchRelatedBymanuallyMatchID() only accepts arguments of type classificationMatch or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the classificationMatchRelatedBymanuallyMatchID relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return classificationMatchHistoryQuery The current query, for fluid interface
     */
    public function joinclassificationMatchRelatedBymanuallyMatchID($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('classificationMatchRelatedBymanuallyMatchID');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'classificationMatchRelatedBymanuallyMatchID');
        }

        return $this;
    }

    /**
     * Use the classificationMatchRelatedBymanuallyMatchID relation classificationMatch object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MODELS\CLASSIFICATIONS\classificationMatchQuery A secondary query class using the current class as primary query
     */
    public function useclassificationMatchRelatedBymanuallyMatchIDQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinclassificationMatchRelatedBymanuallyMatchID($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'classificationMatchRelatedBymanuallyMatchID', '\MODELS\CLASSIFICATIONS\classificationMatchQuery');
    }

    /**
     * Filter the query by a related classificationMatch object
     *
     * @param   classificationMatch|PropelObjectCollection $classificationMatch The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   classificationMatchHistoryQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByclassificationMatchRelatedBypropagatedMatchID($classificationMatch, $comparison = null)
    {
        if ($classificationMatch instanceof classificationMatch) {
            return $this
                ->addUsingAlias(classificationMatchHistoryPeer::PROPAGATEDMATCHID, $classificationMatch->getid(), $comparison);
        } elseif ($classificationMatch instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(classificationMatchHistoryPeer::PROPAGATEDMATCHID, $classificationMatch->toKeyValue('PrimaryKey', 'id'), $comparison);
        } else {
            throw new PropelException('filterByclassificationMatchRelatedBypropagatedMatchID() only accepts arguments of type classificationMatch or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the classificationMatchRelatedBypropagatedMatchID relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return classificationMatchHistoryQuery The current query, for fluid interface
     */
    public function joinclassificationMatchRelatedBypropagatedMatchID($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('classificationMatchRelatedBypropagatedMatchID');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'classificationMatchRelatedBypropagatedMatchID');
        }

        return $this;
    }

    /**
     * Use the classificationMatchRelatedBypropagatedMatchID relation classificationMatch object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MODELS\CLASSIFICATIONS\classificationMatchQuery A secondary query class using the current class as primary query
     */
    public function useclassificationMatchRelatedBypropagatedMatchIDQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinclassificationMatchRelatedBypropagatedMatchID($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'classificationMatchRelatedBypropagatedMatchID', '\MODELS\CLASSIFICATIONS\classificationMatchQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   classificationMatchHistory $classificationMatchHistory Object to remove from the list of results
     *
     * @return classificationMatchHistoryQuery The current query, for fluid interface
     */
    public function prune($classificationMatchHistory = null)
    {
        if ($classificationMatchHistory) {
            $this->addCond('pruneCond0', $this->getAliasedColName(classificationMatchHistoryPeer::MANUALLYMATCHID), $classificationMatchHistory->getmanuallyMatchID(), Criteria::NOT_EQUAL);
            $this->addCond('pruneCond1', $this->getAliasedColName(classificationMatchHistoryPeer::PROPAGATEDMATCHID), $classificationMatchHistory->getpropagatedMatchID(), Criteria::NOT_EQUAL);
            $this->combine(array('pruneCond0', 'pruneCond1'), Criteria::LOGICAL_OR);
        }

        return $this;
    }

}
