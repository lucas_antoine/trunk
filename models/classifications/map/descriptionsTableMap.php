<?php

namespace MODELS\CLASSIFICATIONS\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'descriptions' table.
 *
 *
 * This class was autogenerated by Propel 1.6.7 on:
 *
 * Sun Feb 16 17:09:48 2014
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.classifications.map
 */
class descriptionsTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'classifications.map.descriptionsTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('descriptions');
        $this->setPhpName('descriptions');
        $this->setClassname('MODELS\\CLASSIFICATIONS\\descriptions');
        $this->setPackage('classifications');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignPrimaryKey('ID', 'id', 'INTEGER' , 'classifications', 'ID', true, null, null);
        $this->addColumn('DESCRIPTION', 'description', 'LONGVARCHAR', true, null, null);
        $this->addPrimaryKey('LANGUAGEID', 'languageID', 'INTEGER', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('classifications', 'MODELS\\CLASSIFICATIONS\\classifications', RelationMap::MANY_TO_ONE, array('id' => 'id', ), 'CASCADE', 'CASCADE');
    } // buildRelations()

} // descriptionsTableMap
