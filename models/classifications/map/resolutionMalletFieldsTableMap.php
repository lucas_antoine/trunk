<?php

namespace MODELS\CLASSIFICATIONS\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'resolutionMalletFields' table.
 *
 *
 * This class was autogenerated by Propel 1.6.7 on:
 *
 * Wed Nov 13 00:34:23 2013
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.classifications.map
 */
class resolutionMalletFieldsTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'classifications.map.resolutionMalletFieldsTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('resolutionMalletFields');
        $this->setPhpName('resolutionMalletFields');
        $this->setClassname('MODELS\\CLASSIFICATIONS\\resolutionMalletFields');
        $this->setPackage('classifications');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ID', 'id', 'INTEGER', true, null, null);
        $this->addForeignKey('MALLETFIELDID', 'malletFieldID', 'INTEGER', 'malletFields', 'ID', false, null, null);
        $this->addForeignKey('TYPEID', 'typeID', 'INTEGER', 'resolutionData', 'ID', false, null, null);
        $this->addColumn('POSITION', 'position', 'INTEGER', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('malletFields', 'MODELS\\CLASSIFICATIONS\\malletFields', RelationMap::MANY_TO_ONE, array('malletFieldID' => 'id', ), null, null);
        $this->addRelation('resolutionData', 'MODELS\\CLASSIFICATIONS\\resolutionData', RelationMap::MANY_TO_ONE, array('typeID' => 'id', ), null, null);
    } // buildRelations()

} // resolutionMalletFieldsTableMap
