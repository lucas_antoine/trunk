<?php

namespace MODELS\JOBS\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use MODELS\JOBS\currencyRates;
use MODELS\JOBS\jobs;
use MODELS\JOBS\jobsSalary;
use MODELS\JOBS\jobsSalaryPeer;
use MODELS\JOBS\jobsSalaryQuery;
use MODELS\JOBS\salaryType;

/**
 * Base class that represents a query for the 'jobsSalary' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.6.7 on:
 *
 * Sun Feb 16 17:09:50 2014
 *
 * @method jobsSalaryQuery orderByid($order = Criteria::ASC) Order by the id column
 * @method jobsSalaryQuery orderBysalaryFrom($order = Criteria::ASC) Order by the salaryFrom column
 * @method jobsSalaryQuery orderBysalaryTo($order = Criteria::ASC) Order by the salaryTo column
 * @method jobsSalaryQuery orderBysalaryCurrencyID($order = Criteria::ASC) Order by the salaryCurrencyID column
 * @method jobsSalaryQuery orderBysalaryText($order = Criteria::ASC) Order by the salaryText column
 * @method jobsSalaryQuery orderBysalaryTypeID($order = Criteria::ASC) Order by the salaryTypeID column
 *
 * @method jobsSalaryQuery groupByid() Group by the id column
 * @method jobsSalaryQuery groupBysalaryFrom() Group by the salaryFrom column
 * @method jobsSalaryQuery groupBysalaryTo() Group by the salaryTo column
 * @method jobsSalaryQuery groupBysalaryCurrencyID() Group by the salaryCurrencyID column
 * @method jobsSalaryQuery groupBysalaryText() Group by the salaryText column
 * @method jobsSalaryQuery groupBysalaryTypeID() Group by the salaryTypeID column
 *
 * @method jobsSalaryQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method jobsSalaryQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method jobsSalaryQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method jobsSalaryQuery leftJoinjobs($relationAlias = null) Adds a LEFT JOIN clause to the query using the jobs relation
 * @method jobsSalaryQuery rightJoinjobs($relationAlias = null) Adds a RIGHT JOIN clause to the query using the jobs relation
 * @method jobsSalaryQuery innerJoinjobs($relationAlias = null) Adds a INNER JOIN clause to the query using the jobs relation
 *
 * @method jobsSalaryQuery leftJoinsalaryType($relationAlias = null) Adds a LEFT JOIN clause to the query using the salaryType relation
 * @method jobsSalaryQuery rightJoinsalaryType($relationAlias = null) Adds a RIGHT JOIN clause to the query using the salaryType relation
 * @method jobsSalaryQuery innerJoinsalaryType($relationAlias = null) Adds a INNER JOIN clause to the query using the salaryType relation
 *
 * @method jobsSalaryQuery leftJoincurrencyRates($relationAlias = null) Adds a LEFT JOIN clause to the query using the currencyRates relation
 * @method jobsSalaryQuery rightJoincurrencyRates($relationAlias = null) Adds a RIGHT JOIN clause to the query using the currencyRates relation
 * @method jobsSalaryQuery innerJoincurrencyRates($relationAlias = null) Adds a INNER JOIN clause to the query using the currencyRates relation
 *
 * @method jobsSalary findOne(PropelPDO $con = null) Return the first jobsSalary matching the query
 * @method jobsSalary findOneOrCreate(PropelPDO $con = null) Return the first jobsSalary matching the query, or a new jobsSalary object populated from the query conditions when no match is found
 *
 * @method jobsSalary findOneByid(int $id) Return the first jobsSalary filtered by the id column
 * @method jobsSalary findOneBysalaryFrom(double $salaryFrom) Return the first jobsSalary filtered by the salaryFrom column
 * @method jobsSalary findOneBysalaryTo(double $salaryTo) Return the first jobsSalary filtered by the salaryTo column
 * @method jobsSalary findOneBysalaryCurrencyID(int $salaryCurrencyID) Return the first jobsSalary filtered by the salaryCurrencyID column
 * @method jobsSalary findOneBysalaryText(string $salaryText) Return the first jobsSalary filtered by the salaryText column
 * @method jobsSalary findOneBysalaryTypeID(int $salaryTypeID) Return the first jobsSalary filtered by the salaryTypeID column
 *
 * @method array findByid(int $id) Return jobsSalary objects filtered by the id column
 * @method array findBysalaryFrom(double $salaryFrom) Return jobsSalary objects filtered by the salaryFrom column
 * @method array findBysalaryTo(double $salaryTo) Return jobsSalary objects filtered by the salaryTo column
 * @method array findBysalaryCurrencyID(int $salaryCurrencyID) Return jobsSalary objects filtered by the salaryCurrencyID column
 * @method array findBysalaryText(string $salaryText) Return jobsSalary objects filtered by the salaryText column
 * @method array findBysalaryTypeID(int $salaryTypeID) Return jobsSalary objects filtered by the salaryTypeID column
 *
 * @package    propel.generator.jobs.om
 */
abstract class BasejobsSalaryQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BasejobsSalaryQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'JOBS', $modelName = 'MODELS\\JOBS\\jobsSalary', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new jobsSalaryQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     jobsSalaryQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return jobsSalaryQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof jobsSalaryQuery) {
            return $criteria;
        }
        $query = new jobsSalaryQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   jobsSalary|jobsSalary[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = jobsSalaryPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(jobsSalaryPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   jobsSalary A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `SALARYFROM`, `SALARYTO`, `SALARYCURRENCYID`, `SALARYTEXT`, `SALARYTYPEID` FROM `jobsSalary` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new jobsSalary();
            $obj->hydrate($row);
            jobsSalaryPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return jobsSalary|jobsSalary[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|jobsSalary[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return jobsSalaryQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(jobsSalaryPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return jobsSalaryQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(jobsSalaryPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterByid(1234); // WHERE id = 1234
     * $query->filterByid(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterByid(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @see       filterByjobs()
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return jobsSalaryQuery The current query, for fluid interface
     */
    public function filterByid($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(jobsSalaryPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the salaryFrom column
     *
     * Example usage:
     * <code>
     * $query->filterBysalaryFrom(1234); // WHERE salaryFrom = 1234
     * $query->filterBysalaryFrom(array(12, 34)); // WHERE salaryFrom IN (12, 34)
     * $query->filterBysalaryFrom(array('min' => 12)); // WHERE salaryFrom > 12
     * </code>
     *
     * @param     mixed $salaryFrom The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return jobsSalaryQuery The current query, for fluid interface
     */
    public function filterBysalaryFrom($salaryFrom = null, $comparison = null)
    {
        if (is_array($salaryFrom)) {
            $useMinMax = false;
            if (isset($salaryFrom['min'])) {
                $this->addUsingAlias(jobsSalaryPeer::SALARYFROM, $salaryFrom['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($salaryFrom['max'])) {
                $this->addUsingAlias(jobsSalaryPeer::SALARYFROM, $salaryFrom['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(jobsSalaryPeer::SALARYFROM, $salaryFrom, $comparison);
    }

    /**
     * Filter the query on the salaryTo column
     *
     * Example usage:
     * <code>
     * $query->filterBysalaryTo(1234); // WHERE salaryTo = 1234
     * $query->filterBysalaryTo(array(12, 34)); // WHERE salaryTo IN (12, 34)
     * $query->filterBysalaryTo(array('min' => 12)); // WHERE salaryTo > 12
     * </code>
     *
     * @param     mixed $salaryTo The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return jobsSalaryQuery The current query, for fluid interface
     */
    public function filterBysalaryTo($salaryTo = null, $comparison = null)
    {
        if (is_array($salaryTo)) {
            $useMinMax = false;
            if (isset($salaryTo['min'])) {
                $this->addUsingAlias(jobsSalaryPeer::SALARYTO, $salaryTo['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($salaryTo['max'])) {
                $this->addUsingAlias(jobsSalaryPeer::SALARYTO, $salaryTo['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(jobsSalaryPeer::SALARYTO, $salaryTo, $comparison);
    }

    /**
     * Filter the query on the salaryCurrencyID column
     *
     * Example usage:
     * <code>
     * $query->filterBysalaryCurrencyID(1234); // WHERE salaryCurrencyID = 1234
     * $query->filterBysalaryCurrencyID(array(12, 34)); // WHERE salaryCurrencyID IN (12, 34)
     * $query->filterBysalaryCurrencyID(array('min' => 12)); // WHERE salaryCurrencyID > 12
     * </code>
     *
     * @see       filterBycurrencyRates()
     *
     * @param     mixed $salaryCurrencyID The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return jobsSalaryQuery The current query, for fluid interface
     */
    public function filterBysalaryCurrencyID($salaryCurrencyID = null, $comparison = null)
    {
        if (is_array($salaryCurrencyID)) {
            $useMinMax = false;
            if (isset($salaryCurrencyID['min'])) {
                $this->addUsingAlias(jobsSalaryPeer::SALARYCURRENCYID, $salaryCurrencyID['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($salaryCurrencyID['max'])) {
                $this->addUsingAlias(jobsSalaryPeer::SALARYCURRENCYID, $salaryCurrencyID['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(jobsSalaryPeer::SALARYCURRENCYID, $salaryCurrencyID, $comparison);
    }

    /**
     * Filter the query on the salaryText column
     *
     * Example usage:
     * <code>
     * $query->filterBysalaryText('fooValue');   // WHERE salaryText = 'fooValue'
     * $query->filterBysalaryText('%fooValue%'); // WHERE salaryText LIKE '%fooValue%'
     * </code>
     *
     * @param     string $salaryText The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return jobsSalaryQuery The current query, for fluid interface
     */
    public function filterBysalaryText($salaryText = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($salaryText)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $salaryText)) {
                $salaryText = str_replace('*', '%', $salaryText);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(jobsSalaryPeer::SALARYTEXT, $salaryText, $comparison);
    }

    /**
     * Filter the query on the salaryTypeID column
     *
     * Example usage:
     * <code>
     * $query->filterBysalaryTypeID(1234); // WHERE salaryTypeID = 1234
     * $query->filterBysalaryTypeID(array(12, 34)); // WHERE salaryTypeID IN (12, 34)
     * $query->filterBysalaryTypeID(array('min' => 12)); // WHERE salaryTypeID > 12
     * </code>
     *
     * @see       filterBysalaryType()
     *
     * @param     mixed $salaryTypeID The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return jobsSalaryQuery The current query, for fluid interface
     */
    public function filterBysalaryTypeID($salaryTypeID = null, $comparison = null)
    {
        if (is_array($salaryTypeID)) {
            $useMinMax = false;
            if (isset($salaryTypeID['min'])) {
                $this->addUsingAlias(jobsSalaryPeer::SALARYTYPEID, $salaryTypeID['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($salaryTypeID['max'])) {
                $this->addUsingAlias(jobsSalaryPeer::SALARYTYPEID, $salaryTypeID['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(jobsSalaryPeer::SALARYTYPEID, $salaryTypeID, $comparison);
    }

    /**
     * Filter the query by a related jobs object
     *
     * @param   jobs|PropelObjectCollection $jobs The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   jobsSalaryQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByjobs($jobs, $comparison = null)
    {
        if ($jobs instanceof jobs) {
            return $this
                ->addUsingAlias(jobsSalaryPeer::ID, $jobs->getid(), $comparison);
        } elseif ($jobs instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(jobsSalaryPeer::ID, $jobs->toKeyValue('PrimaryKey', 'id'), $comparison);
        } else {
            throw new PropelException('filterByjobs() only accepts arguments of type jobs or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the jobs relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return jobsSalaryQuery The current query, for fluid interface
     */
    public function joinjobs($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('jobs');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'jobs');
        }

        return $this;
    }

    /**
     * Use the jobs relation jobs object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MODELS\JOBS\jobsQuery A secondary query class using the current class as primary query
     */
    public function usejobsQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinjobs($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'jobs', '\MODELS\JOBS\jobsQuery');
    }

    /**
     * Filter the query by a related salaryType object
     *
     * @param   salaryType|PropelObjectCollection $salaryType The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   jobsSalaryQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBysalaryType($salaryType, $comparison = null)
    {
        if ($salaryType instanceof salaryType) {
            return $this
                ->addUsingAlias(jobsSalaryPeer::SALARYTYPEID, $salaryType->getid(), $comparison);
        } elseif ($salaryType instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(jobsSalaryPeer::SALARYTYPEID, $salaryType->toKeyValue('PrimaryKey', 'id'), $comparison);
        } else {
            throw new PropelException('filterBysalaryType() only accepts arguments of type salaryType or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the salaryType relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return jobsSalaryQuery The current query, for fluid interface
     */
    public function joinsalaryType($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('salaryType');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'salaryType');
        }

        return $this;
    }

    /**
     * Use the salaryType relation salaryType object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MODELS\JOBS\salaryTypeQuery A secondary query class using the current class as primary query
     */
    public function usesalaryTypeQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinsalaryType($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'salaryType', '\MODELS\JOBS\salaryTypeQuery');
    }

    /**
     * Filter the query by a related currencyRates object
     *
     * @param   currencyRates|PropelObjectCollection $currencyRates The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   jobsSalaryQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterBycurrencyRates($currencyRates, $comparison = null)
    {
        if ($currencyRates instanceof currencyRates) {
            return $this
                ->addUsingAlias(jobsSalaryPeer::SALARYCURRENCYID, $currencyRates->getid(), $comparison);
        } elseif ($currencyRates instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(jobsSalaryPeer::SALARYCURRENCYID, $currencyRates->toKeyValue('PrimaryKey', 'id'), $comparison);
        } else {
            throw new PropelException('filterBycurrencyRates() only accepts arguments of type currencyRates or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the currencyRates relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return jobsSalaryQuery The current query, for fluid interface
     */
    public function joincurrencyRates($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('currencyRates');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'currencyRates');
        }

        return $this;
    }

    /**
     * Use the currencyRates relation currencyRates object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MODELS\JOBS\currencyRatesQuery A secondary query class using the current class as primary query
     */
    public function usecurrencyRatesQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joincurrencyRates($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'currencyRates', '\MODELS\JOBS\currencyRatesQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   jobsSalary $jobsSalary Object to remove from the list of results
     *
     * @return jobsSalaryQuery The current query, for fluid interface
     */
    public function prune($jobsSalary = null)
    {
        if ($jobsSalary) {
            $this->addUsingAlias(jobsSalaryPeer::ID, $jobsSalary->getid(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
