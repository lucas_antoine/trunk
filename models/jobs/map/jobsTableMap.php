<?php

namespace MODELS\JOBS\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'jobs' table.
 *
 *
 * This class was autogenerated by Propel 1.6.7 on:
 *
 * Sun Feb 16 17:09:49 2014
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.jobs.map
 */
class jobsTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'jobs.map.jobsTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('jobs');
        $this->setPhpName('jobs');
        $this->setClassname('MODELS\\JOBS\\jobs');
        $this->setPackage('jobs');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ID', 'id', 'INTEGER', true, null, null);
        $this->addColumn('LANGUAGEID', 'languageID', 'INTEGER', false, null, 1);
        $this->addColumn('ORIGINID', 'originID', 'INTEGER', true, null, null);
        $this->addColumn('TITLE', 'title', 'VARCHAR', true, 255, null);
        $this->addColumn('DESCRIPTION', 'description', 'LONGVARCHAR', true, null, null);
        $this->addColumn('VACREF', 'vacRef', 'VARCHAR', true, 145, null);
        $this->addColumn('POSTEDDATE', 'postedDate', 'TIMESTAMP', true, null, null);
        $this->addColumn('MODIFICATIONDATE', 'modificationDate', 'TIMESTAMP', false, null, null);
        $this->addColumn('EXPIRATIONDATE', 'expirationDate', 'DATE', false, null, null);
        $this->addColumn('TAGFREE', 'tagFree', 'LONGVARCHAR', true, null, null);
        $this->addColumn('EDUCATION', 'education', 'LONGVARCHAR', false, null, null);
        $this->addColumn('REQUIREMENTS', 'requirements', 'LONGVARCHAR', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('duplicateJobsRelatedByjobID', 'MODELS\\JOBS\\duplicateJobs', RelationMap::ONE_TO_ONE, array('id' => 'jobID', ), 'CASCADE', 'CASCADE');
        $this->addRelation('duplicateJobsRelatedByduplicateJobID', 'MODELS\\JOBS\\duplicateJobs', RelationMap::ONE_TO_MANY, array('id' => 'duplicateJobID', ), 'CASCADE', 'CASCADE', 'duplicateJobssRelatedByduplicateJobID');
        $this->addRelation('jobsAnalytics', 'MODELS\\JOBS\\jobsAnalytics', RelationMap::ONE_TO_MANY, array('id' => 'id', ), 'CASCADE', 'CASCADE', 'jobsAnalyticss');
        $this->addRelation('jobsClassifications', 'MODELS\\JOBS\\jobsClassifications', RelationMap::ONE_TO_MANY, array('id' => 'jobID', ), 'CASCADE', 'CASCADE', 'jobsClassificationss');
        $this->addRelation('jobsPrivate', 'MODELS\\JOBS\\jobsPrivate', RelationMap::ONE_TO_ONE, array('id' => 'id', ), 'CASCADE', 'CASCADE');
        $this->addRelation('jobsSalary', 'MODELS\\JOBS\\jobsSalary', RelationMap::ONE_TO_ONE, array('id' => 'id', ), 'CASCADE', 'CASCADE');
        $this->addRelation('jobsSuspended', 'MODELS\\JOBS\\jobsSuspended', RelationMap::ONE_TO_ONE, array('id' => 'id', ), 'CASCADE', 'CASCADE');
    } // buildRelations()

} // jobsTableMap
