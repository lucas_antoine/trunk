<?php

namespace MODELS\JOBS\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'jobsPrivate' table.
 *
 *
 * This class was autogenerated by Propel 1.6.7 on:
 *
 * Sun Feb 16 17:09:50 2014
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.jobs.map
 */
class jobsPrivateTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'jobs.map.jobsPrivateTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('jobsPrivate');
        $this->setPhpName('jobsPrivate');
        $this->setClassname('MODELS\\JOBS\\jobsPrivate');
        $this->setPackage('jobs');
        $this->setUseIdGenerator(false);
        // columns
        $this->addForeignPrimaryKey('ID', 'id', 'INTEGER' , 'jobs', 'ID', true, null, null);
        $this->addColumn('BRANCHID', 'branchID', 'INTEGER', false, null, null);
        $this->addColumn('CONTACTNAME', 'contactName', 'CHAR', false, 150, null);
        $this->addColumn('CONTACTEMAIL', 'contactEmail', 'CHAR', false, 150, null);
        $this->addColumn('HTTPREF', 'HTTPREF', 'VARCHAR', true, 245, null);
        $this->addColumn('COMPANYNAME', 'companyName', 'VARCHAR', false, 145, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('jobs', 'MODELS\\JOBS\\jobs', RelationMap::MANY_TO_ONE, array('id' => 'id', ), 'CASCADE', 'CASCADE');
    } // buildRelations()

} // jobsPrivateTableMap
