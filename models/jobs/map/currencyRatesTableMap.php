<?php

namespace MODELS\JOBS\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'currencyRates' table.
 *
 *
 * This class was autogenerated by Propel 1.6.7 on:
 *
 * Sun Feb 16 17:09:49 2014
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.jobs.map
 */
class currencyRatesTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'jobs.map.currencyRatesTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('currencyRates');
        $this->setPhpName('currencyRates');
        $this->setClassname('MODELS\\JOBS\\currencyRates');
        $this->setPackage('jobs');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ID', 'id', 'INTEGER', true, null, null);
        $this->addColumn('CODE', 'code', 'VARCHAR', true, 3, null);
        $this->addColumn('RATE', 'rate', 'FLOAT', true, null, null);
        $this->addColumn('NAME', 'name', 'VARCHAR', true, 255, null);
        $this->addColumn('ALIAS', 'alias', 'VARCHAR', true, 255, null);
        $this->addColumn('SYMBOL', 'symbol', 'VARCHAR', true, 15, null);
        $this->addColumn('TESTCOLUMN', 'testcolumn', 'VARCHAR', false, 45, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('jobsSalary', 'MODELS\\JOBS\\jobsSalary', RelationMap::ONE_TO_MANY, array('id' => 'salaryCurrencyID', ), 'CASCADE', 'CASCADE', 'jobsSalarys');
    } // buildRelations()

} // currencyRatesTableMap
