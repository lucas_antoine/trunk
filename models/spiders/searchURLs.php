<?php

namespace MODELS\SPIDERS;

use MODELS\SPIDERS\om\BasesearchURLs;
use \CORE\site;

/**
 * Skeleton subclass for representing a row from the 'searchURLs' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.6.7 on:
 *
 * Sat Sep 21 14:53:56 2013
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.spiders
 */
class searchURLs extends BasesearchURLs
{

    /**
     *
     * @param int $spiderID
     * @param int $nbURLs
     * @return \MODELS\SPIDERS\searchURLs[]
     */
    public static function getRandomURLs($spiderID, $nbURLs){
        site::$s->setDB(DB_SPIDERS2);
        site::$s->q("CREATE TEMPORARY TABLE search_map_$spiderID (row_id int not NULL primary key AUTO_INCREMENT, random_id int not null)");
        site::$s->q("INSERT INTO  search_map_$spiderID  SELECT NULL AS row_id, id from searchURLs WHERE spiderID=$spiderID");

        $found = [];
        $req = site::$s->q("SELECT MAX(row_id) as max FROM search_map_$spiderID");
        $res = f($req);
        $max = $res['max'];
        while(count($found) < $nbURLs){
            $id = rand(1, $max);
            if(!array_key_exists($id, $found)){
                $req = site::$s->q("SELECT random_id from search_map_$spiderID  WHERE row_id=$id");
                $res=f($req);
                $found[] = \MODELS\SPIDERS\searchURLsQuery::create()->findPk($res['random_id']);
            }
        }
        site::$s->q("DROP TEMPORARY TABLE search_map_$spiderID ");
        return $found;

    }

    /**
     *
     * @return string The HTML
     * @throws \Exception When the code != 200
     */
    public function simpleCurl(){
        $ch = curl_init($this->geturl());
        $options = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_MAXREDIRS => 5,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 5,
            CURLOPT_HTTPHEADER => array(
                "Accept: text/html,application/xhtml+xml,application/xml",
                "Accept-Charset:ISO-8859-1,utf-8;",
                "Accept-Language:en-US,en;q=0.8,fr-FR;q=0.6,fr;q=0.4")
        );
        curl_setopt_array($ch, $options);
        $html = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
        if($info['http_code']!=200){
            throw new \myException(var_export($info, true));
        }
        return $html;
    }

    public function getSections($html){
        //Load SpiderConfig
        $spiderConfig = \MODELS\SPIDERS\spiderConfigQuery::create()->filterByspiderID($this->getspiderID())
                ->filterByregexNameID(1)->findOne();
        
        if($spiderConfig==null){
            throw new \myException("No spiderConfig this spider ");
        }
        if($spiderConfig->getconfigStatusID() < 2 ){
            throw new \myException("Some regex are invalid ");
        }

        //Load Section Regexes
        $regexes = [];
        $regex =  \MODELS\SPIDERS\regexQuery::create()->filterByspiderConfigID($spiderConfig->getid())
            ->findOne();

        while($regex!=null){
            $regexes[]=$regex;
            $regex = \MODELS\SPIDERS\regexQuery::create()->filterByspiderConfigID($spiderConfig->getid())->filterBypreviousRegexID($regex->getid())->findOne();
        }

        foreach($regexes as $regex){
            if(is_array($html)){
                foreach($html as $key=>$value){
                    $html[$key] = $regex->execute($value);
                }
            }
            else{
                $html = $regex->execute($html);
            }
        }
        return $html;

    }
}
