<?php

namespace MODELS\SPIDERS\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use MODELS\SPIDERS\regexNames;
use MODELS\SPIDERS\regexNamesPeer;
use MODELS\SPIDERS\regexNamesQuery;
use MODELS\SPIDERS\regexURLGroups;
use MODELS\SPIDERS\spiderConfig;

/**
 * Base class that represents a query for the 'regexNames' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.6.7 on:
 *
 * Sun Feb 16 17:09:52 2014
 *
 * @method regexNamesQuery orderByid($order = Criteria::ASC) Order by the id column
 * @method regexNamesQuery orderByregexGroupID($order = Criteria::ASC) Order by the regexGroupID column
 * @method regexNamesQuery orderByname($order = Criteria::ASC) Order by the name column
 * @method regexNamesQuery orderByrequired($order = Criteria::ASC) Order by the required column
 *
 * @method regexNamesQuery groupByid() Group by the id column
 * @method regexNamesQuery groupByregexGroupID() Group by the regexGroupID column
 * @method regexNamesQuery groupByname() Group by the name column
 * @method regexNamesQuery groupByrequired() Group by the required column
 *
 * @method regexNamesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method regexNamesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method regexNamesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method regexNamesQuery leftJoinregexURLGroups($relationAlias = null) Adds a LEFT JOIN clause to the query using the regexURLGroups relation
 * @method regexNamesQuery rightJoinregexURLGroups($relationAlias = null) Adds a RIGHT JOIN clause to the query using the regexURLGroups relation
 * @method regexNamesQuery innerJoinregexURLGroups($relationAlias = null) Adds a INNER JOIN clause to the query using the regexURLGroups relation
 *
 * @method regexNamesQuery leftJoinspiderConfig($relationAlias = null) Adds a LEFT JOIN clause to the query using the spiderConfig relation
 * @method regexNamesQuery rightJoinspiderConfig($relationAlias = null) Adds a RIGHT JOIN clause to the query using the spiderConfig relation
 * @method regexNamesQuery innerJoinspiderConfig($relationAlias = null) Adds a INNER JOIN clause to the query using the spiderConfig relation
 *
 * @method regexNames findOne(PropelPDO $con = null) Return the first regexNames matching the query
 * @method regexNames findOneOrCreate(PropelPDO $con = null) Return the first regexNames matching the query, or a new regexNames object populated from the query conditions when no match is found
 *
 * @method regexNames findOneByid(int $id) Return the first regexNames filtered by the id column
 * @method regexNames findOneByregexGroupID(int $regexGroupID) Return the first regexNames filtered by the regexGroupID column
 * @method regexNames findOneByname(string $name) Return the first regexNames filtered by the name column
 * @method regexNames findOneByrequired(boolean $required) Return the first regexNames filtered by the required column
 *
 * @method array findByid(int $id) Return regexNames objects filtered by the id column
 * @method array findByregexGroupID(int $regexGroupID) Return regexNames objects filtered by the regexGroupID column
 * @method array findByname(string $name) Return regexNames objects filtered by the name column
 * @method array findByrequired(boolean $required) Return regexNames objects filtered by the required column
 *
 * @package    propel.generator.spiders.om
 */
abstract class BaseregexNamesQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseregexNamesQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'SPIDERS2', $modelName = 'MODELS\\SPIDERS\\regexNames', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new regexNamesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     regexNamesQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return regexNamesQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof regexNamesQuery) {
            return $criteria;
        }
        $query = new regexNamesQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   regexNames|regexNames[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = regexNamesPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(regexNamesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   regexNames A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `REGEXGROUPID`, `NAME`, `REQUIRED` FROM `regexNames` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new regexNames();
            $obj->hydrate($row);
            regexNamesPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return regexNames|regexNames[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|regexNames[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return regexNamesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(regexNamesPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return regexNamesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(regexNamesPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterByid(1234); // WHERE id = 1234
     * $query->filterByid(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterByid(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return regexNamesQuery The current query, for fluid interface
     */
    public function filterByid($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(regexNamesPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the regexGroupID column
     *
     * Example usage:
     * <code>
     * $query->filterByregexGroupID(1234); // WHERE regexGroupID = 1234
     * $query->filterByregexGroupID(array(12, 34)); // WHERE regexGroupID IN (12, 34)
     * $query->filterByregexGroupID(array('min' => 12)); // WHERE regexGroupID > 12
     * </code>
     *
     * @see       filterByregexURLGroups()
     *
     * @param     mixed $regexGroupID The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return regexNamesQuery The current query, for fluid interface
     */
    public function filterByregexGroupID($regexGroupID = null, $comparison = null)
    {
        if (is_array($regexGroupID)) {
            $useMinMax = false;
            if (isset($regexGroupID['min'])) {
                $this->addUsingAlias(regexNamesPeer::REGEXGROUPID, $regexGroupID['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($regexGroupID['max'])) {
                $this->addUsingAlias(regexNamesPeer::REGEXGROUPID, $regexGroupID['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(regexNamesPeer::REGEXGROUPID, $regexGroupID, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByname('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByname('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return regexNamesQuery The current query, for fluid interface
     */
    public function filterByname($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(regexNamesPeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query on the required column
     *
     * Example usage:
     * <code>
     * $query->filterByrequired(true); // WHERE required = true
     * $query->filterByrequired('yes'); // WHERE required = true
     * </code>
     *
     * @param     boolean|string $required The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return regexNamesQuery The current query, for fluid interface
     */
    public function filterByrequired($required = null, $comparison = null)
    {
        if (is_string($required)) {
            $required = in_array(strtolower($required), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(regexNamesPeer::REQUIRED, $required, $comparison);
    }

    /**
     * Filter the query by a related regexURLGroups object
     *
     * @param   regexURLGroups|PropelObjectCollection $regexURLGroups The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   regexNamesQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByregexURLGroups($regexURLGroups, $comparison = null)
    {
        if ($regexURLGroups instanceof regexURLGroups) {
            return $this
                ->addUsingAlias(regexNamesPeer::REGEXGROUPID, $regexURLGroups->getid(), $comparison);
        } elseif ($regexURLGroups instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(regexNamesPeer::REGEXGROUPID, $regexURLGroups->toKeyValue('PrimaryKey', 'id'), $comparison);
        } else {
            throw new PropelException('filterByregexURLGroups() only accepts arguments of type regexURLGroups or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the regexURLGroups relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return regexNamesQuery The current query, for fluid interface
     */
    public function joinregexURLGroups($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('regexURLGroups');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'regexURLGroups');
        }

        return $this;
    }

    /**
     * Use the regexURLGroups relation regexURLGroups object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MODELS\SPIDERS\regexURLGroupsQuery A secondary query class using the current class as primary query
     */
    public function useregexURLGroupsQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinregexURLGroups($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'regexURLGroups', '\MODELS\SPIDERS\regexURLGroupsQuery');
    }

    /**
     * Filter the query by a related spiderConfig object
     *
     * @param   spiderConfig|PropelObjectCollection $spiderConfig  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   regexNamesQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByspiderConfig($spiderConfig, $comparison = null)
    {
        if ($spiderConfig instanceof spiderConfig) {
            return $this
                ->addUsingAlias(regexNamesPeer::ID, $spiderConfig->getregexNameID(), $comparison);
        } elseif ($spiderConfig instanceof PropelObjectCollection) {
            return $this
                ->usespiderConfigQuery()
                ->filterByPrimaryKeys($spiderConfig->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByspiderConfig() only accepts arguments of type spiderConfig or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the spiderConfig relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return regexNamesQuery The current query, for fluid interface
     */
    public function joinspiderConfig($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('spiderConfig');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'spiderConfig');
        }

        return $this;
    }

    /**
     * Use the spiderConfig relation spiderConfig object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MODELS\SPIDERS\spiderConfigQuery A secondary query class using the current class as primary query
     */
    public function usespiderConfigQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinspiderConfig($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'spiderConfig', '\MODELS\SPIDERS\spiderConfigQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   regexNames $regexNames Object to remove from the list of results
     *
     * @return regexNamesQuery The current query, for fluid interface
     */
    public function prune($regexNames = null)
    {
        if ($regexNames) {
            $this->addUsingAlias(regexNamesPeer::ID, $regexNames->getid(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
