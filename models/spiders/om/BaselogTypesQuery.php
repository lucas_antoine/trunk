<?php

namespace MODELS\SPIDERS\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use MODELS\SPIDERS\logTypes;
use MODELS\SPIDERS\logTypesPeer;
use MODELS\SPIDERS\logTypesQuery;
use MODELS\SPIDERS\spiderLog;

/**
 * Base class that represents a query for the 'logTypes' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.6.7 on:
 *
 * Sun Feb 16 17:09:52 2014
 *
 * @method logTypesQuery orderByid($order = Criteria::ASC) Order by the id column
 * @method logTypesQuery orderByname($order = Criteria::ASC) Order by the name column
 * @method logTypesQuery orderByseverity($order = Criteria::ASC) Order by the severity column
 *
 * @method logTypesQuery groupByid() Group by the id column
 * @method logTypesQuery groupByname() Group by the name column
 * @method logTypesQuery groupByseverity() Group by the severity column
 *
 * @method logTypesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method logTypesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method logTypesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method logTypesQuery leftJoinspiderLog($relationAlias = null) Adds a LEFT JOIN clause to the query using the spiderLog relation
 * @method logTypesQuery rightJoinspiderLog($relationAlias = null) Adds a RIGHT JOIN clause to the query using the spiderLog relation
 * @method logTypesQuery innerJoinspiderLog($relationAlias = null) Adds a INNER JOIN clause to the query using the spiderLog relation
 *
 * @method logTypes findOne(PropelPDO $con = null) Return the first logTypes matching the query
 * @method logTypes findOneOrCreate(PropelPDO $con = null) Return the first logTypes matching the query, or a new logTypes object populated from the query conditions when no match is found
 *
 * @method logTypes findOneByid(int $id) Return the first logTypes filtered by the id column
 * @method logTypes findOneByname(string $name) Return the first logTypes filtered by the name column
 * @method logTypes findOneByseverity(int $severity) Return the first logTypes filtered by the severity column
 *
 * @method array findByid(int $id) Return logTypes objects filtered by the id column
 * @method array findByname(string $name) Return logTypes objects filtered by the name column
 * @method array findByseverity(int $severity) Return logTypes objects filtered by the severity column
 *
 * @package    propel.generator.spiders.om
 */
abstract class BaselogTypesQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaselogTypesQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'SPIDERS2', $modelName = 'MODELS\\SPIDERS\\logTypes', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new logTypesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     logTypesQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return logTypesQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof logTypesQuery) {
            return $criteria;
        }
        $query = new logTypesQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   logTypes|logTypes[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = logTypesPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(logTypesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   logTypes A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `NAME`, `SEVERITY` FROM `logTypes` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new logTypes();
            $obj->hydrate($row);
            logTypesPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return logTypes|logTypes[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|logTypes[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return logTypesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(logTypesPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return logTypesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(logTypesPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterByid(1234); // WHERE id = 1234
     * $query->filterByid(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterByid(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return logTypesQuery The current query, for fluid interface
     */
    public function filterByid($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(logTypesPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByname('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByname('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return logTypesQuery The current query, for fluid interface
     */
    public function filterByname($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(logTypesPeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query on the severity column
     *
     * Example usage:
     * <code>
     * $query->filterByseverity(1234); // WHERE severity = 1234
     * $query->filterByseverity(array(12, 34)); // WHERE severity IN (12, 34)
     * $query->filterByseverity(array('min' => 12)); // WHERE severity > 12
     * </code>
     *
     * @param     mixed $severity The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return logTypesQuery The current query, for fluid interface
     */
    public function filterByseverity($severity = null, $comparison = null)
    {
        if (is_array($severity)) {
            $useMinMax = false;
            if (isset($severity['min'])) {
                $this->addUsingAlias(logTypesPeer::SEVERITY, $severity['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($severity['max'])) {
                $this->addUsingAlias(logTypesPeer::SEVERITY, $severity['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(logTypesPeer::SEVERITY, $severity, $comparison);
    }

    /**
     * Filter the query by a related spiderLog object
     *
     * @param   spiderLog|PropelObjectCollection $spiderLog  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   logTypesQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByspiderLog($spiderLog, $comparison = null)
    {
        if ($spiderLog instanceof spiderLog) {
            return $this
                ->addUsingAlias(logTypesPeer::ID, $spiderLog->getlogTypeID(), $comparison);
        } elseif ($spiderLog instanceof PropelObjectCollection) {
            return $this
                ->usespiderLogQuery()
                ->filterByPrimaryKeys($spiderLog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByspiderLog() only accepts arguments of type spiderLog or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the spiderLog relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return logTypesQuery The current query, for fluid interface
     */
    public function joinspiderLog($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('spiderLog');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'spiderLog');
        }

        return $this;
    }

    /**
     * Use the spiderLog relation spiderLog object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MODELS\SPIDERS\spiderLogQuery A secondary query class using the current class as primary query
     */
    public function usespiderLogQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinspiderLog($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'spiderLog', '\MODELS\SPIDERS\spiderLogQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   logTypes $logTypes Object to remove from the list of results
     *
     * @return logTypesQuery The current query, for fluid interface
     */
    public function prune($logTypes = null)
    {
        if ($logTypes) {
            $this->addUsingAlias(logTypesPeer::ID, $logTypes->getid(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
