<?php

namespace MODELS\SPIDERS\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use MODELS\SPIDERS\jobURLStatusTypes;
use MODELS\SPIDERS\jobURLStatusTypesPeer;
use MODELS\SPIDERS\jobURLStatusTypesQuery;
use MODELS\SPIDERS\jobURLs;

/**
 * Base class that represents a query for the 'jobURLStatusTypes' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.6.7 on:
 *
 * Sun Feb 16 17:09:52 2014
 *
 * @method jobURLStatusTypesQuery orderByid($order = Criteria::ASC) Order by the id column
 * @method jobURLStatusTypesQuery orderByname($order = Criteria::ASC) Order by the name column
 *
 * @method jobURLStatusTypesQuery groupByid() Group by the id column
 * @method jobURLStatusTypesQuery groupByname() Group by the name column
 *
 * @method jobURLStatusTypesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method jobURLStatusTypesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method jobURLStatusTypesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method jobURLStatusTypesQuery leftJoinjobURLs($relationAlias = null) Adds a LEFT JOIN clause to the query using the jobURLs relation
 * @method jobURLStatusTypesQuery rightJoinjobURLs($relationAlias = null) Adds a RIGHT JOIN clause to the query using the jobURLs relation
 * @method jobURLStatusTypesQuery innerJoinjobURLs($relationAlias = null) Adds a INNER JOIN clause to the query using the jobURLs relation
 *
 * @method jobURLStatusTypes findOne(PropelPDO $con = null) Return the first jobURLStatusTypes matching the query
 * @method jobURLStatusTypes findOneOrCreate(PropelPDO $con = null) Return the first jobURLStatusTypes matching the query, or a new jobURLStatusTypes object populated from the query conditions when no match is found
 *
 * @method jobURLStatusTypes findOneByid(int $id) Return the first jobURLStatusTypes filtered by the id column
 * @method jobURLStatusTypes findOneByname(string $name) Return the first jobURLStatusTypes filtered by the name column
 *
 * @method array findByid(int $id) Return jobURLStatusTypes objects filtered by the id column
 * @method array findByname(string $name) Return jobURLStatusTypes objects filtered by the name column
 *
 * @package    propel.generator.spiders.om
 */
abstract class BasejobURLStatusTypesQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BasejobURLStatusTypesQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'SPIDERS2', $modelName = 'MODELS\\SPIDERS\\jobURLStatusTypes', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new jobURLStatusTypesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     jobURLStatusTypesQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return jobURLStatusTypesQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof jobURLStatusTypesQuery) {
            return $criteria;
        }
        $query = new jobURLStatusTypesQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   jobURLStatusTypes|jobURLStatusTypes[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = jobURLStatusTypesPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(jobURLStatusTypesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   jobURLStatusTypes A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `NAME` FROM `jobURLStatusTypes` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new jobURLStatusTypes();
            $obj->hydrate($row);
            jobURLStatusTypesPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return jobURLStatusTypes|jobURLStatusTypes[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|jobURLStatusTypes[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return jobURLStatusTypesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(jobURLStatusTypesPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return jobURLStatusTypesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(jobURLStatusTypesPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterByid(1234); // WHERE id = 1234
     * $query->filterByid(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterByid(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return jobURLStatusTypesQuery The current query, for fluid interface
     */
    public function filterByid($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(jobURLStatusTypesPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByname('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByname('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return jobURLStatusTypesQuery The current query, for fluid interface
     */
    public function filterByname($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(jobURLStatusTypesPeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query by a related jobURLs object
     *
     * @param   jobURLs|PropelObjectCollection $jobURLs  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   jobURLStatusTypesQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByjobURLs($jobURLs, $comparison = null)
    {
        if ($jobURLs instanceof jobURLs) {
            return $this
                ->addUsingAlias(jobURLStatusTypesPeer::ID, $jobURLs->getstatusTypeID(), $comparison);
        } elseif ($jobURLs instanceof PropelObjectCollection) {
            return $this
                ->usejobURLsQuery()
                ->filterByPrimaryKeys($jobURLs->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByjobURLs() only accepts arguments of type jobURLs or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the jobURLs relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return jobURLStatusTypesQuery The current query, for fluid interface
     */
    public function joinjobURLs($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('jobURLs');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'jobURLs');
        }

        return $this;
    }

    /**
     * Use the jobURLs relation jobURLs object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MODELS\SPIDERS\jobURLsQuery A secondary query class using the current class as primary query
     */
    public function usejobURLsQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinjobURLs($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'jobURLs', '\MODELS\SPIDERS\jobURLsQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   jobURLStatusTypes $jobURLStatusTypes Object to remove from the list of results
     *
     * @return jobURLStatusTypesQuery The current query, for fluid interface
     */
    public function prune($jobURLStatusTypes = null)
    {
        if ($jobURLStatusTypes) {
            $this->addUsingAlias(jobURLStatusTypesPeer::ID, $jobURLStatusTypes->getid(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
