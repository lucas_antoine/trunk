<?php

namespace MODELS\SPIDERS\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use MODELS\SPIDERS\configStatus;
use MODELS\SPIDERS\regex;
use MODELS\SPIDERS\regexNames;
use MODELS\SPIDERS\spiderConfig;
use MODELS\SPIDERS\spiderConfigPeer;
use MODELS\SPIDERS\spiderConfigQuery;
use MODELS\SPIDERS\spiders;

/**
 * Base class that represents a query for the 'spiderConfig' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.6.7 on:
 *
 * Sun Feb 16 17:09:53 2014
 *
 * @method spiderConfigQuery orderByid($order = Criteria::ASC) Order by the id column
 * @method spiderConfigQuery orderByspiderID($order = Criteria::ASC) Order by the spiderID column
 * @method spiderConfigQuery orderByregexNameID($order = Criteria::ASC) Order by the regexNameID column
 * @method spiderConfigQuery orderByvalidateJob($order = Criteria::ASC) Order by the validateJob column
 * @method spiderConfigQuery orderByconfigStatusID($order = Criteria::ASC) Order by the configStatusID column
 *
 * @method spiderConfigQuery groupByid() Group by the id column
 * @method spiderConfigQuery groupByspiderID() Group by the spiderID column
 * @method spiderConfigQuery groupByregexNameID() Group by the regexNameID column
 * @method spiderConfigQuery groupByvalidateJob() Group by the validateJob column
 * @method spiderConfigQuery groupByconfigStatusID() Group by the configStatusID column
 *
 * @method spiderConfigQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method spiderConfigQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method spiderConfigQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method spiderConfigQuery leftJoinspiders($relationAlias = null) Adds a LEFT JOIN clause to the query using the spiders relation
 * @method spiderConfigQuery rightJoinspiders($relationAlias = null) Adds a RIGHT JOIN clause to the query using the spiders relation
 * @method spiderConfigQuery innerJoinspiders($relationAlias = null) Adds a INNER JOIN clause to the query using the spiders relation
 *
 * @method spiderConfigQuery leftJoinregexNames($relationAlias = null) Adds a LEFT JOIN clause to the query using the regexNames relation
 * @method spiderConfigQuery rightJoinregexNames($relationAlias = null) Adds a RIGHT JOIN clause to the query using the regexNames relation
 * @method spiderConfigQuery innerJoinregexNames($relationAlias = null) Adds a INNER JOIN clause to the query using the regexNames relation
 *
 * @method spiderConfigQuery leftJoinconfigStatus($relationAlias = null) Adds a LEFT JOIN clause to the query using the configStatus relation
 * @method spiderConfigQuery rightJoinconfigStatus($relationAlias = null) Adds a RIGHT JOIN clause to the query using the configStatus relation
 * @method spiderConfigQuery innerJoinconfigStatus($relationAlias = null) Adds a INNER JOIN clause to the query using the configStatus relation
 *
 * @method spiderConfigQuery leftJoinregex($relationAlias = null) Adds a LEFT JOIN clause to the query using the regex relation
 * @method spiderConfigQuery rightJoinregex($relationAlias = null) Adds a RIGHT JOIN clause to the query using the regex relation
 * @method spiderConfigQuery innerJoinregex($relationAlias = null) Adds a INNER JOIN clause to the query using the regex relation
 *
 * @method spiderConfig findOne(PropelPDO $con = null) Return the first spiderConfig matching the query
 * @method spiderConfig findOneOrCreate(PropelPDO $con = null) Return the first spiderConfig matching the query, or a new spiderConfig object populated from the query conditions when no match is found
 *
 * @method spiderConfig findOneByid(int $id) Return the first spiderConfig filtered by the id column
 * @method spiderConfig findOneByspiderID(int $spiderID) Return the first spiderConfig filtered by the spiderID column
 * @method spiderConfig findOneByregexNameID(int $regexNameID) Return the first spiderConfig filtered by the regexNameID column
 * @method spiderConfig findOneByvalidateJob(boolean $validateJob) Return the first spiderConfig filtered by the validateJob column
 * @method spiderConfig findOneByconfigStatusID(int $configStatusID) Return the first spiderConfig filtered by the configStatusID column
 *
 * @method array findByid(int $id) Return spiderConfig objects filtered by the id column
 * @method array findByspiderID(int $spiderID) Return spiderConfig objects filtered by the spiderID column
 * @method array findByregexNameID(int $regexNameID) Return spiderConfig objects filtered by the regexNameID column
 * @method array findByvalidateJob(boolean $validateJob) Return spiderConfig objects filtered by the validateJob column
 * @method array findByconfigStatusID(int $configStatusID) Return spiderConfig objects filtered by the configStatusID column
 *
 * @package    propel.generator.spiders.om
 */
abstract class BasespiderConfigQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BasespiderConfigQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'SPIDERS2', $modelName = 'MODELS\\SPIDERS\\spiderConfig', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new spiderConfigQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     spiderConfigQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return spiderConfigQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof spiderConfigQuery) {
            return $criteria;
        }
        $query = new spiderConfigQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   spiderConfig|spiderConfig[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = spiderConfigPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(spiderConfigPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   spiderConfig A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `SPIDERID`, `REGEXNAMEID`, `VALIDATEJOB`, `CONFIGSTATUSID` FROM `spiderConfig` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new spiderConfig();
            $obj->hydrate($row);
            spiderConfigPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return spiderConfig|spiderConfig[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|spiderConfig[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return spiderConfigQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(spiderConfigPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return spiderConfigQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(spiderConfigPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterByid(1234); // WHERE id = 1234
     * $query->filterByid(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterByid(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return spiderConfigQuery The current query, for fluid interface
     */
    public function filterByid($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(spiderConfigPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the spiderID column
     *
     * Example usage:
     * <code>
     * $query->filterByspiderID(1234); // WHERE spiderID = 1234
     * $query->filterByspiderID(array(12, 34)); // WHERE spiderID IN (12, 34)
     * $query->filterByspiderID(array('min' => 12)); // WHERE spiderID > 12
     * </code>
     *
     * @see       filterByspiders()
     *
     * @param     mixed $spiderID The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return spiderConfigQuery The current query, for fluid interface
     */
    public function filterByspiderID($spiderID = null, $comparison = null)
    {
        if (is_array($spiderID)) {
            $useMinMax = false;
            if (isset($spiderID['min'])) {
                $this->addUsingAlias(spiderConfigPeer::SPIDERID, $spiderID['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($spiderID['max'])) {
                $this->addUsingAlias(spiderConfigPeer::SPIDERID, $spiderID['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(spiderConfigPeer::SPIDERID, $spiderID, $comparison);
    }

    /**
     * Filter the query on the regexNameID column
     *
     * Example usage:
     * <code>
     * $query->filterByregexNameID(1234); // WHERE regexNameID = 1234
     * $query->filterByregexNameID(array(12, 34)); // WHERE regexNameID IN (12, 34)
     * $query->filterByregexNameID(array('min' => 12)); // WHERE regexNameID > 12
     * </code>
     *
     * @see       filterByregexNames()
     *
     * @param     mixed $regexNameID The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return spiderConfigQuery The current query, for fluid interface
     */
    public function filterByregexNameID($regexNameID = null, $comparison = null)
    {
        if (is_array($regexNameID)) {
            $useMinMax = false;
            if (isset($regexNameID['min'])) {
                $this->addUsingAlias(spiderConfigPeer::REGEXNAMEID, $regexNameID['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($regexNameID['max'])) {
                $this->addUsingAlias(spiderConfigPeer::REGEXNAMEID, $regexNameID['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(spiderConfigPeer::REGEXNAMEID, $regexNameID, $comparison);
    }

    /**
     * Filter the query on the validateJob column
     *
     * Example usage:
     * <code>
     * $query->filterByvalidateJob(true); // WHERE validateJob = true
     * $query->filterByvalidateJob('yes'); // WHERE validateJob = true
     * </code>
     *
     * @param     boolean|string $validateJob The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return spiderConfigQuery The current query, for fluid interface
     */
    public function filterByvalidateJob($validateJob = null, $comparison = null)
    {
        if (is_string($validateJob)) {
            $validateJob = in_array(strtolower($validateJob), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(spiderConfigPeer::VALIDATEJOB, $validateJob, $comparison);
    }

    /**
     * Filter the query on the configStatusID column
     *
     * Example usage:
     * <code>
     * $query->filterByconfigStatusID(1234); // WHERE configStatusID = 1234
     * $query->filterByconfigStatusID(array(12, 34)); // WHERE configStatusID IN (12, 34)
     * $query->filterByconfigStatusID(array('min' => 12)); // WHERE configStatusID > 12
     * </code>
     *
     * @see       filterByconfigStatus()
     *
     * @param     mixed $configStatusID The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return spiderConfigQuery The current query, for fluid interface
     */
    public function filterByconfigStatusID($configStatusID = null, $comparison = null)
    {
        if (is_array($configStatusID)) {
            $useMinMax = false;
            if (isset($configStatusID['min'])) {
                $this->addUsingAlias(spiderConfigPeer::CONFIGSTATUSID, $configStatusID['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($configStatusID['max'])) {
                $this->addUsingAlias(spiderConfigPeer::CONFIGSTATUSID, $configStatusID['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(spiderConfigPeer::CONFIGSTATUSID, $configStatusID, $comparison);
    }

    /**
     * Filter the query by a related spiders object
     *
     * @param   spiders|PropelObjectCollection $spiders The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   spiderConfigQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByspiders($spiders, $comparison = null)
    {
        if ($spiders instanceof spiders) {
            return $this
                ->addUsingAlias(spiderConfigPeer::SPIDERID, $spiders->getid(), $comparison);
        } elseif ($spiders instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(spiderConfigPeer::SPIDERID, $spiders->toKeyValue('PrimaryKey', 'id'), $comparison);
        } else {
            throw new PropelException('filterByspiders() only accepts arguments of type spiders or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the spiders relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return spiderConfigQuery The current query, for fluid interface
     */
    public function joinspiders($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('spiders');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'spiders');
        }

        return $this;
    }

    /**
     * Use the spiders relation spiders object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MODELS\SPIDERS\spidersQuery A secondary query class using the current class as primary query
     */
    public function usespidersQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinspiders($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'spiders', '\MODELS\SPIDERS\spidersQuery');
    }

    /**
     * Filter the query by a related regexNames object
     *
     * @param   regexNames|PropelObjectCollection $regexNames The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   spiderConfigQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByregexNames($regexNames, $comparison = null)
    {
        if ($regexNames instanceof regexNames) {
            return $this
                ->addUsingAlias(spiderConfigPeer::REGEXNAMEID, $regexNames->getid(), $comparison);
        } elseif ($regexNames instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(spiderConfigPeer::REGEXNAMEID, $regexNames->toKeyValue('PrimaryKey', 'id'), $comparison);
        } else {
            throw new PropelException('filterByregexNames() only accepts arguments of type regexNames or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the regexNames relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return spiderConfigQuery The current query, for fluid interface
     */
    public function joinregexNames($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('regexNames');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'regexNames');
        }

        return $this;
    }

    /**
     * Use the regexNames relation regexNames object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MODELS\SPIDERS\regexNamesQuery A secondary query class using the current class as primary query
     */
    public function useregexNamesQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinregexNames($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'regexNames', '\MODELS\SPIDERS\regexNamesQuery');
    }

    /**
     * Filter the query by a related configStatus object
     *
     * @param   configStatus|PropelObjectCollection $configStatus The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   spiderConfigQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByconfigStatus($configStatus, $comparison = null)
    {
        if ($configStatus instanceof configStatus) {
            return $this
                ->addUsingAlias(spiderConfigPeer::CONFIGSTATUSID, $configStatus->getid(), $comparison);
        } elseif ($configStatus instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(spiderConfigPeer::CONFIGSTATUSID, $configStatus->toKeyValue('PrimaryKey', 'id'), $comparison);
        } else {
            throw new PropelException('filterByconfigStatus() only accepts arguments of type configStatus or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the configStatus relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return spiderConfigQuery The current query, for fluid interface
     */
    public function joinconfigStatus($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('configStatus');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'configStatus');
        }

        return $this;
    }

    /**
     * Use the configStatus relation configStatus object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MODELS\SPIDERS\configStatusQuery A secondary query class using the current class as primary query
     */
    public function useconfigStatusQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinconfigStatus($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'configStatus', '\MODELS\SPIDERS\configStatusQuery');
    }

    /**
     * Filter the query by a related regex object
     *
     * @param   regex|PropelObjectCollection $regex  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   spiderConfigQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByregex($regex, $comparison = null)
    {
        if ($regex instanceof regex) {
            return $this
                ->addUsingAlias(spiderConfigPeer::ID, $regex->getspiderConfigID(), $comparison);
        } elseif ($regex instanceof PropelObjectCollection) {
            return $this
                ->useregexQuery()
                ->filterByPrimaryKeys($regex->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByregex() only accepts arguments of type regex or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the regex relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return spiderConfigQuery The current query, for fluid interface
     */
    public function joinregex($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('regex');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'regex');
        }

        return $this;
    }

    /**
     * Use the regex relation regex object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MODELS\SPIDERS\regexQuery A secondary query class using the current class as primary query
     */
    public function useregexQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinregex($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'regex', '\MODELS\SPIDERS\regexQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   spiderConfig $spiderConfig Object to remove from the list of results
     *
     * @return spiderConfigQuery The current query, for fluid interface
     */
    public function prune($spiderConfig = null)
    {
        if ($spiderConfig) {
            $this->addUsingAlias(spiderConfigPeer::ID, $spiderConfig->getid(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
