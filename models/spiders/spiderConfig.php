<?php

namespace MODELS\SPIDERS;

use MODELS\SPIDERS\om\BasespiderConfig;

use \CORE\site;

/**
 * Skeleton subclass for representing a row from the 'spiderConfig' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.6.7 on:
 *
 * Tue Oct  1 05:07:16 2013
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.spiders
 */
class spiderConfig extends BasespiderConfig
{
    
    
    public function checkRegexValidity(){


        $regexes = null;
        $regexes = \MODELS\SPIDERS\regexQuery::create()->filterByspiderConfigID($this->getid())->find();
        if($regexes->count() == 0 || $regexes==null){
            $this->setconfigStatusID( spiders::status_regex_notest);
            $this->save();            
            return "regex count = 0";
        }

        $valid=true;
        
        foreach($regexes as $regex){
            if($regex->getvalid() === false){
                $valid=false;
                
            }
        }
        
        if($valid && $this->getregexNames()->getregexGroupID() <= 2 && $this->getconfigStatusID() < spiders::status_regex_valid){            
            $this->setconfigStatusID( spiders::status_regex_valid);
            
            
        }
        
        elseif($valid && $this->getregexNames()->getregexGroupID()==3){
            //do nothing
            $this->setconfigStatusID( spiders::status_regex_test_pass);
        }
        elseif($valid==false){
            $this->setconfigStatusID( spiders::status_regex_invalid);
            return "one or more r3egex invalid";
        }

        $this->save();

        //Then check all the spiderConfigs belong to the same group as this one.
        $this->checkGroup(1);
        $this->checkGroup(2);
        $this->checkGroup(3);
        
    }


    public function testRegex(){

        switch($this->getregexNames()->getregexURLGroups()->getname()){
            case "search" :
                switch($this->getregexNames()->getname()){
                    case "section" :
                        return $this->testSections(10);
                    default :
                        return $this->testOnSearchSection(10);
                }
                break;
            case "job" :
                return $this->testOnJob(10);
                break;
        }
/**
 * 
 
        return [
            "curlSuccess" =>[
                "url tested1" => ["dataextracted1"],
                "url tested2" => ["dataextracted2"],
                "url tested3" => ["dataextracted3"]
            ],
            "curlError" => ["url tested4", "url tested5"]
        ];
 * 
 */

    }


    public function checkGroup($groupID){
        //urlGroup 1 = search
        $valid=true;

        $spiderConfigs = \MODELS\SPIDERS\spiderConfigQuery::create()->filterByspiderID($this->getspiderID())
                ->useregexNamesQuery()
                    ->filterByregexGroupID($groupID)
                ->endUse()
                ->find();
        
        
        switch($groupID){
            
            case 1 : //search
            case 2 : //job
                foreach($spiderConfigs as $spiderConfig){
                    if($spiderConfig->getconfigStatusID()!=5){
                        $valid = false;
                    }
                }
                break;
            case 3 : 
                foreach($spiderConfigs as $spiderConfig){
                    if($spiderConfig->getconfigStatusID() != 3){
                        $valid = false;
                    }
                }
                break;
        }
        if($valid){
            
            switch($groupID){
                case 1 : //search
                    
                    $this->getspiders()->setregexSearchStatus( spiders::curl_search_pass )->save();
                    break;
                case 2 : //job
                    $this->getspiders()->setregexJobStatus(spiders::curl_job_pass)->save();
                    break;
                case 3 : //expired
                    $this->getspiders()->setregex404Status( spiders::curl_job_pass )->save();
                    break;
                default : 
                    throw new \Exception();                    
            }
            
        }
        else{
            switch($groupID){
                case 1 : //search
                    
                    $this->getspiders()->setregexSearchStatus( spiders::curl_search_fail )->save();
                    break;
                case 2 : //job
                    $this->getspiders()->setregexJobStatus( spiders::curl_job_fail )->save();
                    break;
                case 3 : //expired
                    $this->getspiders()->setregex404Status( spiders::curl_job_fail )->save();
                    break;
                default : 
                    throw new \Exception();                    
            }
            
        }
    }

    public function execute($html){
        $regexes = [];
        $regex =  \MODELS\SPIDERS\regexQuery::create()->filterByspiderConfigID($this->getid())->filterBypreviousRegexID(null)->findOne();
        while($regex!=null){
            $regexes[]=$regex;
            $regex = \MODELS\SPIDERS\regexQuery::create()->filterByspiderConfigID($this->getid())->filterBypreviousRegexID($regex->getid())->findOne();
        }

        foreach($regexes as $regex){
            $html = $regex->execute($html);
        }
        return $html;
    }


    /**********************************PRIVATE***********************************/

    private function testSections($nbURLs){
        $results = [];
        $results['curlSuccess'] = [];
        $results['curlError'] = [];

        $urls = searchURLs::getRandomURLs($this->getspiderID(), $nbURLs);
        foreach ($urls as $url){
            try{
                $html = $url->simpleCurl();
                $results['curlSuccess'][$url->geturl()] = $url->getSections($html);
            }
            catch(\Exception $e){
                $results['curlError'][] = $url->geturl();
            }
        }
        return $results;
    }

    /**
     *
     * @param int $nbURLs
     * @return The formated array
     */
    private function testOnSearchSection($nbURLs){
        $results = [];
        $results['curlSuccess'] = [];
        $results['curlError'] = [];

        $urls = searchURLs::getRandomURLs($this->getspiderID(), $nbURLs);
        foreach ($urls as $url){
            $results['curlSuccess'][$url->geturl()] = [];
            try{
                $html = $url->simpleCurl();
                $sections = $url->getSections($html);
                foreach($sections as $section){
                    $results['curlSuccess'][$url->geturl()][] = $this->testSimpleRegex($section);
                }
            }
            catch(\Exception $e){
                $results['curlError'][] = $url->geturl();
            }
        }
        return $results;
    }

    private function testOnJob($nbURLs){
        $results = [];
        $results['curlSuccess'] = [];
        $results['curlError'] = [];

        $urls = jobURLs::getRandomURLs($this->getspiderID(), $nbURLs);
        foreach ($urls as $url){
            $results['curlSuccess'][$url->geturl()] = [];
            try{
                $html = $url->simpleCurl();
                $results['curlSuccess'][$url->geturl()] = $this->testSimpleRegex($html);
            }
            catch(\Exception $e){
                $results['curlError'][] = $url->geturl();
            }
        }
        return $results;
    }


    /**
     *
     * Only when the regex doesn't contain any preg_match_all
     * @param string $html
     * @return string
     */
    private function testSimpleRegex($html){
        return $this->execute($html);
    }
}
