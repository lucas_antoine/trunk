<?php

namespace MODELS\SPIDERS;

use MODELS\SPIDERS\om\BaselogTypesQuery;


/**
 * Skeleton subclass for performing query and update operations on the 'logTypes' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.6.7 on:
 *
 * Sun Jan 19 02:48:15 2014
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.spiders
 */
class logTypesQuery extends BaselogTypesQuery
{
}
