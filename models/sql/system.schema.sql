
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- pid
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `pid`;

CREATE TABLE `pid`
(
    `id` INTEGER NOT NULL,
    `websiteID` INTEGER NOT NULL,
    `time` DATETIME,
    `queue` TINYINT(1) DEFAULT 0 NOT NULL,
    `running` TINYINT(1) DEFAULT 0 NOT NULL,
    `endTime` DATETIME,
    `debug` TEXT,
    PRIMARY KEY (`id`,`websiteID`),
    INDEX `fk_pid_2_idx` (`websiteID`),
    CONSTRAINT `fk_pid_1`
        FOREIGN KEY (`id`)
        REFERENCES `processes` (`id`)
        ON UPDATE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- processes
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `processes`;

CREATE TABLE `processes`
(
    `id` INTEGER NOT NULL,
    `name` VARCHAR(45),
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
