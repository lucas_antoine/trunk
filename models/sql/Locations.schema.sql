
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- languages
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `languages`;

CREATE TABLE `languages`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `code3` CHAR(3) NOT NULL,
    `code2` CHAR(2) NOT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_languageCountries_2`
        FOREIGN KEY (`id`)
        REFERENCES `languageCountries` (`languageID`),
    CONSTRAINT `fk_languageNames_1`
        FOREIGN KEY (`id`)
        REFERENCES `languageNames` (`id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- languageCountries
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `languageCountries`;

CREATE TABLE `languageCountries`
(
    `countryID` INTEGER NOT NULL,
    `languageID` INTEGER NOT NULL,
    PRIMARY KEY (`countryID`,`languageID`),
    INDEX `fk_languageCountries_2` (`languageID`, `countryID`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- languageNames
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `languageNames`;

CREATE TABLE `languageNames`
(
    `id` INTEGER NOT NULL,
    `ENG` VARCHAR(150) NOT NULL,
    `FRE` VARCHAR(150) NOT NULL,
    `ARA` VARCHAR(150) NOT NULL,
    `SPA` VARCHAR(150) NOT NULL,
    `CHI` VARCHAR(150) NOT NULL,
    `POR` VARCHAR(150) NOT NULL,
    `ITA` VARCHAR(150) NOT NULL,
    `GER` VARCHAR(150) NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `fk_languageNames_1` (`id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- locationNames
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `locationNames`;

CREATE TABLE `locationNames`
(
    `ARA` CHAR(100),
    `CHI` CHAR(100),
    `ENG` CHAR(100),
    `FRE` CHAR(100),
    `GER` CHAR(100),
    `ITA` CHAR(100),
    `POR` CHAR(100),
    `SPA` CHAR(100),
    `id` INTEGER DEFAULT '0' NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- parentChild
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `parentChild`;

CREATE TABLE `parentChild`
(
    `id` INTEGER DEFAULT '0' NOT NULL,
    `parentID` INTEGER NOT NULL,
    `depth` INTEGER NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
