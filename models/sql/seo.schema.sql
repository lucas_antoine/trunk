
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- seoPageData
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `seoPageData`;

CREATE TABLE `seoPageData`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `seoPageID` INTEGER NOT NULL,
    `websiteID` INTEGER NOT NULL,
    `title` TEXT NOT NULL,
    `description` TEXT NOT NULL,
    `customText` TEXT NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `index4` (`seoPageID`, `websiteID`),
    INDEX `fk_seoPageData_1_idx` (`websiteID`),
    INDEX `fk_seoPageData_2_idx` (`seoPageID`),
    CONSTRAINT `fk_seoPageData_2`
        FOREIGN KEY (`seoPageID`)
        REFERENCES `seoPages` (`urlID`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- seoPages
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `seoPages`;

CREATE TABLE `seoPages`
(
    `urlID` INTEGER NOT NULL,
    `specialFunction` CHAR(50) NOT NULL,
    `defaultTitle` VARCHAR(255),
    `defaultDescription` TEXT,
    `defaultCustomText` TEXT,
    PRIMARY KEY (`urlID`)
) ENGINE=MyISAM;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
