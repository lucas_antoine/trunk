
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- aliases
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `aliases`;

CREATE TABLE `aliases`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `classificationID` INTEGER NOT NULL,
    `alias` VARCHAR(45) NOT NULL,
    `languageID` INTEGER NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `fk_aliases_2_idx` (`languageID`),
    INDEX `index2` (`id`, `languageID`),
    INDEX `fk_aliases_1_idx` (`classificationID`),
    CONSTRAINT `fk_aliases_1`
        FOREIGN KEY (`classificationID`)
        REFERENCES `classifications` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- classificationMalletStatus
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `classificationMalletStatus`;

CREATE TABLE `classificationMalletStatus`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `classificationID` INTEGER NOT NULL,
    `againstClassificationOriginID` INTEGER NOT NULL,
    `classificationStatusID` INTEGER NOT NULL,
    `lastCheckDate` DATE,
    `malletTrainAccuracy` DOUBLE,
    `malletTestAccuracy` DOUBLE,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `index2` (`classificationID`, `againstClassificationOriginID`),
    INDEX `fk_classificationMalletStatus_2_idx` (`againstClassificationOriginID`),
    INDEX `fk_classificationMalletStatus_3_idx` (`classificationStatusID`),
    INDEX `index3` (`lastCheckDate`),
    CONSTRAINT `fk_classificationMalletStatus_1`
        FOREIGN KEY (`classificationID`)
        REFERENCES `classifications` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `fk_classificationMalletStatus_2`
        FOREIGN KEY (`againstClassificationOriginID`)
        REFERENCES `classificationOrigins` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `fk_classificationMalletStatus_3`
        FOREIGN KEY (`classificationStatusID`)
        REFERENCES `classificationStatus` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- classificationMatch
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `classificationMatch`;

CREATE TABLE `classificationMatch`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `subjectID` INTEGER NOT NULL,
    `complementID` INTEGER NOT NULL,
    `malletAccuracyRatio` INTEGER,
    `matchStatusID` INTEGER,
    `nbImportedJobs` INTEGER DEFAULT 0 NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `index6` (`subjectID`, `complementID`),
    INDEX `fk_classificationMatch_1_idx` (`subjectID`),
    INDEX `fk_classificationMatch_3_idx` (`complementID`),
    INDEX `fk_classificationMatch_2_idx` (`matchStatusID`),
    CONSTRAINT `fk_classificationMatch_1`
        FOREIGN KEY (`subjectID`)
        REFERENCES `classifications` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `fk_classificationMatch_2`
        FOREIGN KEY (`matchStatusID`)
        REFERENCES `matchStatus` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `fk_classificationMatch_3`
        FOREIGN KEY (`complementID`)
        REFERENCES `classifications` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- classificationNames
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `classificationNames`;

CREATE TABLE `classificationNames`
(
    `id` INTEGER NOT NULL,
    `name` VARCHAR(150) NOT NULL,
    `languageID` INTEGER NOT NULL,
    PRIMARY KEY (`id`,`languageID`),
    INDEX `fk_classificationNames_1_idx` (`languageID`),
    INDEX `index3` (`name`),
    CONSTRAINT `classificationNames_ibfk_1`
        FOREIGN KEY (`id`)
        REFERENCES `classifications` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- classificationOrigins
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `classificationOrigins`;

CREATE TABLE `classificationOrigins`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `originID` INTEGER,
    `typeID` INTEGER,
    `isMaster` TINYINT(1) DEFAULT 1,
    `classificationStatusID` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `fk_classificationOriginProperties_1_idx` (`originID`),
    INDEX `fk_classificationOriginProperties_3_idx` (`typeID`),
    INDEX `fk_classificationOrigins_1_idx` (`classificationStatusID`),
    CONSTRAINT `fk_classificationOrigins_1`
        FOREIGN KEY (`classificationStatusID`)
        REFERENCES `classificationStatus` (`id`)
        ON UPDATE CASCADE,
    CONSTRAINT `fk_classificationOrigins_3`
        FOREIGN KEY (`typeID`)
        REFERENCES `types` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- classificationStatus
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `classificationStatus`;

CREATE TABLE `classificationStatus`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `statusName` VARCHAR(45),
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- classifications
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `classifications`;

CREATE TABLE `classifications`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `parentID` INTEGER NOT NULL,
    `classificationOriginID` INTEGER,
    `contextID` VARCHAR(150) NOT NULL,
    `depth` INTEGER NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `index5` (`depth`),
    INDEX `index6` (`parentID`),
    INDEX `contextID` (`contextID`),
    INDEX `index7` (`parentID`, `depth`),
    INDEX `fk_classifications_1_idx` (`classificationOriginID`),
    CONSTRAINT `fk_classifications_1`
        FOREIGN KEY (`classificationOriginID`)
        REFERENCES `classificationOrigins` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- descriptions
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `descriptions`;

CREATE TABLE `descriptions`
(
    `id` INTEGER NOT NULL,
    `description` TEXT NOT NULL,
    `languageID` INTEGER NOT NULL,
    PRIMARY KEY (`id`,`languageID`),
    INDEX `fk_descriptions_1_idx` (`languageID`),
    CONSTRAINT `classificationDescription_ibfk_1`
        FOREIGN KEY (`id`)
        REFERENCES `classifications` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- levels
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `levels`;

CREATE TABLE `levels`
(
    `id` INTEGER NOT NULL,
    `level0` INTEGER,
    `level1` INTEGER,
    `level2` INTEGER,
    `level3` INTEGER,
    `level4` INTEGER,
    `level5` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `index2` (`level0`),
    INDEX `index3` (`level1`),
    INDEX `index4` (`level2`),
    INDEX `index5` (`level3`),
    INDEX `index6` (`level4`),
    INDEX `index7` (`level5`),
    CONSTRAINT `fk_levels_1`
        FOREIGN KEY (`id`)
        REFERENCES `classifications` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- locationData
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `locationData`;

CREATE TABLE `locationData`
(
    `id` INTEGER NOT NULL,
    `longitude` DOUBLE,
    `latitude` DOUBLE,
    `offsetLon` DOUBLE,
    `offsetLat` DOUBLE,
    `zip` VARCHAR(45),
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_locationData_1`
        FOREIGN KEY (`id`)
        REFERENCES `classifications` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- matchStatus
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `matchStatus`;

CREATE TABLE `matchStatus`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45),
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- resolutionTypes
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `resolutionTypes`;

CREATE TABLE `resolutionTypes`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45),
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- stopWords
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `stopWords`;

CREATE TABLE `stopWords`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `ENG` CHAR(255),
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- types
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `types`;

CREATE TABLE `types`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45),
    `resolutionTypeID` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `fk_types_1_idx` (`resolutionTypeID`),
    CONSTRAINT `fk_types_1`
        FOREIGN KEY (`resolutionTypeID`)
        REFERENCES `resolutionTypes` (`id`)
        ON UPDATE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- wikipedia
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `wikipedia`;

CREATE TABLE `wikipedia`
(
    `id` INTEGER NOT NULL,
    `url` VARCHAR(45),
    `description` VARCHAR(45),
    `languageID` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `fk_wikipedia_2_idx` (`languageID`),
    CONSTRAINT `fk_wikipedia_1`
        FOREIGN KEY (`id`)
        REFERENCES `classifications` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
