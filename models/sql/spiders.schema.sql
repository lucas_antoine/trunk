
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- classificationResolutionStatus
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `classificationResolutionStatus`;

CREATE TABLE `classificationResolutionStatus`
(
    `jobURLID` INTEGER NOT NULL,
    `classificationOriginID` INTEGER NOT NULL,
    `resolutionStatusID` INTEGER NOT NULL,
    PRIMARY KEY (`jobURLID`,`classificationOriginID`,`resolutionStatusID`),
    INDEX `fk_classificationResolutionStatus_2_idx` (`classificationOriginID`),
    INDEX `fk_classificationResolutionStatus_3_idx` (`resolutionStatusID`),
    INDEX `index4` (`jobURLID`, `classificationOriginID`),
    CONSTRAINT `fk_classificationResolutionStatus_1`
        FOREIGN KEY (`jobURLID`)
        REFERENCES `jobURLs` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `fk_classificationResolutionStatus_3`
        FOREIGN KEY (`resolutionStatusID`)
        REFERENCES `resolutionStatus` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- configStatus
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `configStatus`;

CREATE TABLE `configStatus`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45),
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- errorTypes
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `errorTypes`;

CREATE TABLE `errorTypes`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(145),
    `type` enum('curl','parsing'),
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- jobURLData
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jobURLData`;

CREATE TABLE `jobURLData`
(
    `jobURLID` INTEGER NOT NULL,
    `jobReference` VARCHAR(145),
    `title` VARCHAR(245),
    `description` TEXT,
    `description2` TEXT,
    `companyName` VARCHAR(245),
    `city` CHAR(245),
    `requirements` TEXT,
    `education` TEXT,
    `employmentType` CHAR(145),
    PRIMARY KEY (`jobURLID`),
    CONSTRAINT `fk_jobURLData_1`
        FOREIGN KEY (`jobURLID`)
        REFERENCES `jobURLs` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- jobURLErrors
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jobURLErrors`;

CREATE TABLE `jobURLErrors`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `jobURLID` INTEGER,
    `errorID` INTEGER,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `index4` (`jobURLID`, `errorID`),
    INDEX `fk_jobURLErrors_1_idx` (`jobURLID`),
    INDEX `fk_jobURLErrors_2_idx` (`errorID`),
    CONSTRAINT `fk_jobURLErrors_1`
        FOREIGN KEY (`jobURLID`)
        REFERENCES `jobURLs` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `fk_jobURLErrors_2`
        FOREIGN KEY (`errorID`)
        REFERENCES `errorTypes` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- jobURLSearchURL
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jobURLSearchURL`;

CREATE TABLE `jobURLSearchURL`
(
    `jobURLID` INTEGER NOT NULL,
    `searchURLID` INTEGER NOT NULL,
    PRIMARY KEY (`jobURLID`,`searchURLID`),
    INDEX `fk_jobURLSearchURL_2_idx` (`searchURLID`),
    CONSTRAINT `fk_jobURLSearchURL_1`
        FOREIGN KEY (`jobURLID`)
        REFERENCES `jobURLs` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `fk_jobURLSearchURL_2`
        FOREIGN KEY (`searchURLID`)
        REFERENCES `searchURLs` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- jobURLStatusTypes
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jobURLStatusTypes`;

CREATE TABLE `jobURLStatusTypes`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45),
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- jobURLs
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jobURLs`;

CREATE TABLE `jobURLs`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `url` VARCHAR(245) NOT NULL,
    `addedDate` DATETIME,
    `lastSpiderDate` DATETIME,
    `jobID` INTEGER,
    `statusTypeID` INTEGER DEFAULT 6 NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `index4` (`url`),
    INDEX `fk_jobURLs_2_idx` (`jobID`),
    INDEX `fk_jobURLs_3` (`statusTypeID`),
    INDEX `index5` (`addedDate`),
    CONSTRAINT `fk_jobURLs_3`
        FOREIGN KEY (`statusTypeID`)
        REFERENCES `jobURLStatusTypes` (`id`)
        ON UPDATE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- logTypes
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `logTypes`;

CREATE TABLE `logTypes`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(145),
    `severity` INTEGER,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- preferences
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `preferences`;

CREATE TABLE `preferences`
(
    `spiderID` INTEGER NOT NULL,
    `nbPages` INTEGER DEFAULT 3,
    `nbJobsPerPage` INTEGER DEFAULT 15,
    `paginationStartNumber` INTEGER DEFAULT 1,
    `firstPagePagination` TINYINT(1) DEFAULT 0,
    `acceptableCurlFailPercent` INTEGER DEFAULT 2,
    `acceptableInvalidJobPercent` INTEGER DEFAULT 5,
    `searchHTTPType` enum('GET','POST'),
    `jobURLPrefix` VARCHAR(245),
    PRIMARY KEY (`spiderID`),
    INDEX `fk_preferences_1_idx` (`spiderID`),
    CONSTRAINT `fk_preferences_1`
        FOREIGN KEY (`spiderID`)
        REFERENCES `spiders` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- regex
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `regex`;

CREATE TABLE `regex`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `previousRegexID` INTEGER,
    `spiderConfigID` INTEGER NOT NULL,
    `regexTypeID` INTEGER NOT NULL,
    `expression` VARCHAR(245),
    `options` TEXT,
    `processID` INTEGER,
    `valid` TINYINT(1),
    PRIMARY KEY (`id`),
    INDEX `fk_regex_1_idx` (`spiderConfigID`),
    INDEX `fk_regex_2_idx` (`previousRegexID`),
    INDEX `fk_regex_3_idx` (`regexTypeID`),
    CONSTRAINT `fk_regex_1`
        FOREIGN KEY (`spiderConfigID`)
        REFERENCES `spiderConfig` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `fk_regex_2`
        FOREIGN KEY (`previousRegexID`)
        REFERENCES `regex` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `fk_regex_3`
        FOREIGN KEY (`regexTypeID`)
        REFERENCES `regexTypes` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- regexNames
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `regexNames`;

CREATE TABLE `regexNames`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `regexGroupID` INTEGER,
    `name` VARCHAR(145),
    `required` TINYINT(1),
    PRIMARY KEY (`id`),
    UNIQUE INDEX `index2` (`name`, `regexGroupID`),
    INDEX `fk_regexNames_1_idx` (`regexGroupID`),
    CONSTRAINT `fk_regexNames_1`
        FOREIGN KEY (`regexGroupID`)
        REFERENCES `regexURLGroups` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- regexTypes
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `regexTypes`;

CREATE TABLE `regexTypes`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45),
    `format` enum('string','array1','array2'),
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- regexURLGroups
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `regexURLGroups`;

CREATE TABLE `regexURLGroups`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45),
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- resolutionStatus
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `resolutionStatus`;

CREATE TABLE `resolutionStatus`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- runningStatus
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `runningStatus`;

CREATE TABLE `runningStatus`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(145),
    PRIMARY KEY (`id`),
    UNIQUE INDEX `name_UNIQUE` (`name`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- searchPOST
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `searchPOST`;

CREATE TABLE `searchPOST`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `spiderID` INTEGER,
    `key` enum('string','cID','nameID','pageString','pageNumber'),
    `value` CHAR(255),
    `name` VARCHAR(145),
    PRIMARY KEY (`id`),
    INDEX `fk_searchPOST_1_idx` (`spiderID`),
    CONSTRAINT `fk_searchPOST_1`
        FOREIGN KEY (`spiderID`)
        REFERENCES `spiders` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- searchURLClassifications
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `searchURLClassifications`;

CREATE TABLE `searchURLClassifications`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `searchURLID` INTEGER,
    `classificationID` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `fk_searchURLClassifications_1_idx` (`searchURLID`),
    INDEX `fk_searchURLClassifications_2_idx` (`classificationID`),
    INDEX `searchURLID` (`searchURLID`),
    CONSTRAINT `fk_searchURLClassifications_1`
        FOREIGN KEY (`searchURLID`)
        REFERENCES `searchURLs` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- searchURLErrors
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `searchURLErrors`;

CREATE TABLE `searchURLErrors`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `searchURLID` INTEGER,
    `errorID` INTEGER,
    `nb` INTEGER,
    `debug` TEXT,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `index4` (`searchURLID`, `errorID`),
    INDEX `fk_searchURLErrors_1_idx` (`searchURLID`),
    INDEX `fk_searchURLErrors_2_idx` (`errorID`),
    CONSTRAINT `fk_searchURLErrors_1`
        FOREIGN KEY (`searchURLID`)
        REFERENCES `searchURLs` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `fk_searchURLErrors_2`
        FOREIGN KEY (`errorID`)
        REFERENCES `errorTypes` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- searchURLs
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `searchURLs`;

CREATE TABLE `searchURLs`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `spiderID` INTEGER,
    `url` VARCHAR(245),
    `POSTData` TEXT,
    `lastSpiderDate` DATETIME,
    `page` INTEGER,
    `nbSections` INTEGER,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `index3` (`url`),
    INDEX `fk_searchURLs_1_idx` (`spiderID`),
    CONSTRAINT `fk_searchURLs_1`
        FOREIGN KEY (`spiderID`)
        REFERENCES `spiders` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- spiderConfig
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `spiderConfig`;

CREATE TABLE `spiderConfig`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `spiderID` INTEGER,
    `regexNameID` INTEGER,
    `validateJob` TINYINT(1) DEFAULT 1,
    `configStatusID` INTEGER,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `index5` (`regexNameID`, `spiderID`),
    INDEX `fk_spiderConfig_1_idx` (`spiderID`),
    INDEX `fk_spiderConfig_2_idx` (`regexNameID`),
    INDEX `fk_spiderConfig_3_idx` (`configStatusID`),
    CONSTRAINT `fk_spiderConfig_1`
        FOREIGN KEY (`spiderID`)
        REFERENCES `spiders` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `fk_spiderConfig_2`
        FOREIGN KEY (`regexNameID`)
        REFERENCES `regexNames` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `fk_spiderConfig_3`
        FOREIGN KEY (`configStatusID`)
        REFERENCES `configStatus` (`id`)
        ON UPDATE CASCADE
        ON DELETE SET NULL
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- spiderLog
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `spiderLog`;

CREATE TABLE `spiderLog`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `spiderID` INTEGER NOT NULL,
    `logTypeID` INTEGER NOT NULL,
    `scriptPath` VARCHAR(145) NOT NULL,
    `scriptLine` INTEGER NOT NULL,
    `message` VARCHAR(255),
    `errorDate` DATETIME NOT NULL,
    `context` TEXT,
    PRIMARY KEY (`id`),
    INDEX `fk_spiderExceptions_1_idx` (`spiderID`),
    INDEX `fk_spiderExceptions_2_idx` (`logTypeID`),
    CONSTRAINT `fk_spiderLog_1`
        FOREIGN KEY (`spiderID`)
        REFERENCES `spiders` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `fk_spiderLog_2`
        FOREIGN KEY (`logTypeID`)
        REFERENCES `logTypes` (`id`)
        ON UPDATE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- spiderStats
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `spiderStats`;

CREATE TABLE `spiderStats`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `spiderID` INTEGER,
    `reportDate` DATE,
    `nbLiveJobs` INTEGER,
    `nbJobsSpideredToday` INTEGER,
    `nbJobsAddedToday` INTEGER,
    `nbJobsDeletedToday` INTEGER,
    `ratioJobsLiveExpired` DOUBLE,
    PRIMARY KEY (`id`),
    INDEX `fk_spiderStats_1_idx` (`spiderID`),
    CONSTRAINT `fk_spiderStats_1`
        FOREIGN KEY (`spiderID`)
        REFERENCES `spiders` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- spiderTheme
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `spiderTheme`;

CREATE TABLE `spiderTheme`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `spiderID` INTEGER,
    `name` VARCHAR(145),
    `exampleURL` VARCHAR(245),
    PRIMARY KEY (`id`),
    INDEX `fk_spiderTheme_1_idx` (`spiderID`),
    INDEX `uniquename` (`name`, `spiderID`),
    CONSTRAINT `fk_spiderTheme_1`
        FOREIGN KEY (`spiderID`)
        REFERENCES `spiders` (`id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- spiderTimeLog
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `spiderTimeLog`;

CREATE TABLE `spiderTimeLog`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `spiderID` INTEGER NOT NULL,
    `runningStatusID` INTEGER NOT NULL,
    `startDate` DATETIME,
    `endDate` DATETIME,
    `endStatus` enum('stopped','success','error','SIGKILL','reboot'),
    PRIMARY KEY (`id`),
    INDEX `fk_spiderTimeLog_1_idx` (`spiderID`),
    INDEX `fk_spiderTimeLog_2_idx` (`runningStatusID`),
    CONSTRAINT `fk_spiderTimeLog_1`
        FOREIGN KEY (`spiderID`)
        REFERENCES `spiders` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `fk_spiderTimeLog_2`
        FOREIGN KEY (`runningStatusID`)
        REFERENCES `runningStatus` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- spiders
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `spiders`;

CREATE TABLE `spiders`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `originID` INTEGER,
    `status` INTEGER DEFAULT 0,
    `countryID` INTEGER NOT NULL,
    `urlBuilderStatus` INTEGER(2) DEFAULT 0,
    `importStatus` INTEGER(2) DEFAULT 0,
    `regexSearchStatus` INTEGER(2) DEFAULT 0,
    `regexJobStatus` INTEGER(2) DEFAULT 0,
    `regex404Status` INTEGER(2),
    `curlFailPercentReached` TINYINT(1) DEFAULT 0,
    `invalidJobPercentReached` TINYINT(1) DEFAULT 0,
    `PID` INTEGER,
    `currentRunningStatusID` INTEGER DEFAULT 1,
    PRIMARY KEY (`id`),
    INDEX `fk_spiders_1_idx` (`originID`),
    INDEX `fk_spiders_2_idx` (`countryID`),
    INDEX `fk_spiders_1_idx1` (`currentRunningStatusID`),
    CONSTRAINT `fk_spiders_1`
        FOREIGN KEY (`currentRunningStatusID`)
        REFERENCES `runningStatus` (`id`)
        ON UPDATE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- urlBuilder
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `urlBuilder`;

CREATE TABLE `urlBuilder`
(
    `order` INTEGER NOT NULL,
    `spiderID` INTEGER NOT NULL,
    `key` enum('string','cID','nameID','pageString','pageNumber'),
    `value` CHAR(245),
    `maxLevel` INTEGER DEFAULT 0,
    PRIMARY KEY (`order`,`spiderID`),
    INDEX `fk_urlBuilder_1_idx` (`spiderID`),
    CONSTRAINT `fk_urlBuilder_1`
        FOREIGN KEY (`spiderID`)
        REFERENCES `spiders` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
