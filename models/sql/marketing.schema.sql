
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- OAUTH
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `OAUTH`;

CREATE TABLE `OAUTH`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `websiteID` INTEGER,
    `providerID` INTEGER,
    `contactID` INTEGER,
    `apiKey` VARCHAR(145),
    `secret` VARCHAR(145),
    `token` VARCHAR(145),
    `tokenExpirationDate` DATE,
    PRIMARY KEY (`id`),
    INDEX `fk_OAUTH_1_idx` (`websiteID`),
    INDEX `fk_OAUTH_2_idx` (`providerID`),
    CONSTRAINT `fk_OAUTH_2`
        FOREIGN KEY (`providerID`)
        REFERENCES `OAUTHProvider` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- OAUTHProvider
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `OAUTHProvider`;

CREATE TABLE `OAUTHProvider`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45),
    `website` VARCHAR(145),
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- branchContacts
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `branchContacts`;

CREATE TABLE `branchContacts`
(
    `branchID` INTEGER NOT NULL AUTO_INCREMENT,
    `contactID` INTEGER,
    `positionTYpeID` INTEGER NOT NULL,
    PRIMARY KEY (`branchID`,`positionTYpeID`),
    INDEX `fk_branchContacts_1_idx` (`contactID`),
    INDEX `fk_branchContacts_2_idx` (`branchID`),
    INDEX `fk_branchContacts_3_idx` (`positionTYpeID`),
    CONSTRAINT `fk_branchContacts_1`
        FOREIGN KEY (`contactID`)
        REFERENCES `contacts` (`id`),
    CONSTRAINT `fk_branchContacts_2`
        FOREIGN KEY (`branchID`)
        REFERENCES `branches` (`id`),
    CONSTRAINT `fk_branchContacts_3`
        FOREIGN KEY (`positionTYpeID`)
        REFERENCES `positionTypes` (`id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- branchTags
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `branchTags`;

CREATE TABLE `branchTags`
(
    `branchID` INTEGER NOT NULL AUTO_INCREMENT,
    `tagID` INTEGER NOT NULL,
    PRIMARY KEY (`branchID`,`tagID`),
    INDEX `fk_branchTags_1_idx` (`branchID`),
    INDEX `fk_branchTags_2_idx` (`tagID`),
    CONSTRAINT `fk_branchTags_1`
        FOREIGN KEY (`branchID`)
        REFERENCES `branches` (`id`),
    CONSTRAINT `fk_branchTags_2`
        FOREIGN KEY (`tagID`)
        REFERENCES `tags` (`id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- branches
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `branches`;

CREATE TABLE `branches`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `companyID` INTEGER,
    `locationID` INTEGER,
    `name` VARCHAR(45),
    `linkedInURL` VARCHAR(145) NOT NULL,
    `websiteURL` VARCHAR(145),
    PRIMARY KEY (`id`),
    INDEX `fk_branches_1_idx` (`companyID`),
    CONSTRAINT `fk_branches_1`
        FOREIGN KEY (`companyID`)
        REFERENCES `company` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- company
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `company`;

CREATE TABLE `company`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45) NOT NULL,
    `websiteURL` VARCHAR(145),
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- contacts
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `contacts`;

CREATE TABLE `contacts`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `fullName` INTEGER,
    `email` VARCHAR(145),
    `phoneNumber` VARCHAR(145),
    `linkedInURL` VARCHAR(45),
    PRIMARY KEY (`id`),
    UNIQUE INDEX `linkedInURL_UNIQUE` (`linkedInURL`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- origins
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `origins`;

CREATE TABLE `origins`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `branchID` INTEGER,
    `originType` enum('spider','xml','SOAP','REST','OAUTH','noJobs'),
    `unverifiedCompanyWeight` INTEGER,
    `mustVerifyCompany` TINYINT(1) DEFAULT 0,
    `verifiedCompanyWeight` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `fk_origins_1_idx` (`branchID`),
    CONSTRAINT `fk_origins_1`
        FOREIGN KEY (`branchID`)
        REFERENCES `branches` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- positionTypes
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `positionTypes`;

CREATE TABLE `positionTypes`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45),
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- tagGroups
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `tagGroups`;

CREATE TABLE `tagGroups`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45),
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- tags
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `tags`;

CREATE TABLE `tags`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(145),
    `description` VARCHAR(245),
    `tagGroupID` INTEGER,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `name_UNIQUE` (`name`),
    INDEX `fk_tags_1_idx` (`tagGroupID`),
    CONSTRAINT `fk_tags_1`
        FOREIGN KEY (`tagGroupID`)
        REFERENCES `tagGroups` (`id`)
) ENGINE=MyISAM;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
