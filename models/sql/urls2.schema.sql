
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- attributes
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `attributes`;

CREATE TABLE `attributes`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `path` VARCHAR(150),
    `attributeType` enum('method','property'),
    `attributeName` VARCHAR(45),
    `urlName` VARCHAR(45),
    PRIMARY KEY (`id`),
    UNIQUE INDEX `index2` (`attributeType`, `attributeName`, `path`),
    UNIQUE INDEX `urlName_UNIQUE` (`urlName`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- constructors
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `constructors`;

CREATE TABLE `constructors`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `instanciatorID` INTEGER,
    `urlID` INTEGER,
    `required` TINYINT(1),
    `defaultValue` VARCHAR(45),
    `dataValue` VARCHAR(255),
    `dataAttributeID` INTEGER,
    `name` VARCHAR(45),
    PRIMARY KEY (`id`),
    INDEX `fk_constructors_1_idx` (`urlID`),
    INDEX `fk_constructors_2_idx` (`instanciatorID`),
    INDEX `fk_constructors_3_idx` (`dataAttributeID`),
    CONSTRAINT `fk_constructors_1`
        FOREIGN KEY (`urlID`)
        REFERENCES `urls` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `fk_constructors_2`
        FOREIGN KEY (`instanciatorID`)
        REFERENCES `instanciators` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `fk_constructors_3`
        FOREIGN KEY (`dataAttributeID`)
        REFERENCES `attributes` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- gets
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `gets`;

CREATE TABLE `gets`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `constructorID` INTEGER,
    `attributeID` INTEGER,
    `urlName` VARCHAR(45),
    PRIMARY KEY (`id`),
    INDEX `fk_gets_1_idx` (`constructorID`),
    INDEX `fk_gets_2_idx` (`attributeID`),
    CONSTRAINT `fk_gets_1`
        FOREIGN KEY (`constructorID`)
        REFERENCES `constructors` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `fk_gets_2`
        FOREIGN KEY (`attributeID`)
        REFERENCES `attributes` (`id`)
        ON UPDATE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- instanciators
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `instanciators`;

CREATE TABLE `instanciators`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `path` VARCHAR(45),
    `functionName` VARCHAR(45),
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- instanciatorsAttributes
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `instanciatorsAttributes`;

CREATE TABLE `instanciatorsAttributes`
(
    `instanciatorID` INTEGER NOT NULL,
    `attributeID` INTEGER NOT NULL,
    PRIMARY KEY (`instanciatorID`,`attributeID`),
    INDEX `fk_instanciatorsattributes_1_idx` (`instanciatorID`),
    INDEX `fk_instanciatorsattributes_2_idx` (`attributeID`),
    CONSTRAINT `fk_instanciatorsattributes_1`
        FOREIGN KEY (`instanciatorID`)
        REFERENCES `instanciators` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `fk_instanciatorsattributes_2`
        FOREIGN KEY (`attributeID`)
        REFERENCES `attributes` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- items
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `items`;

CREATE TABLE `items`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45),
    `urlID` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `fk_items_1_idx` (`urlID`),
    CONSTRAINT `fk_items_1`
        FOREIGN KEY (`urlID`)
        REFERENCES `urls` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- literals
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `literals`;

CREATE TABLE `literals`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45),
    `type` enum('string','integer'),
    PRIMARY KEY (`id`),
    UNIQUE INDEX `name_UNIQUE` (`name`, `type`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- urlBuilding
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `urlBuilding`;

CREATE TABLE `urlBuilding`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `itemID` INTEGER,
    `type` enum('const','constructor','object','literal'),
    `constValue` VARCHAR(45),
    `constructorID` INTEGER,
    `attributeID` INTEGER,
    `literalID` INTEGER,
    `order` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `fk_urlBuilding_2_idx` (`itemID`),
    INDEX `fk_urlBuilding_1_idx` (`literalID`),
    INDEX `fk_urlBuilding_4_idx` (`attributeID`),
    INDEX `fk_urlBuilding_3_idx` (`constructorID`),
    CONSTRAINT `fk_urlBuilding_1`
        FOREIGN KEY (`literalID`)
        REFERENCES `literals` (`id`)
        ON UPDATE CASCADE,
    CONSTRAINT `fk_urlBuilding_2`
        FOREIGN KEY (`itemID`)
        REFERENCES `items` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `fk_urlBuilding_3`
        FOREIGN KEY (`constructorID`)
        REFERENCES `constructors` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `fk_urlBuilding_4`
        FOREIGN KEY (`attributeID`)
        REFERENCES `attributes` (`id`)
        ON UPDATE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- urlLiterals
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `urlLiterals`;

CREATE TABLE `urlLiterals`
(
    `urlID` INTEGER NOT NULL,
    `literalID` INTEGER NOT NULL,
    `required` TINYINT(1),
    `defaultValue` VARCHAR(45),
    PRIMARY KEY (`urlID`,`literalID`),
    INDEX `fk_urlLiterals_2_idx` (`literalID`),
    CONSTRAINT `fk_urlLiterals_1`
        FOREIGN KEY (`urlID`)
        REFERENCES `urls` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `fk_urlLiterals_2`
        FOREIGN KEY (`literalID`)
        REFERENCES `literals` (`id`)
        ON UPDATE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- urlNames
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `urlNames`;

CREATE TABLE `urlNames`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `urlName` VARCHAR(45),
    PRIMARY KEY (`id`),
    UNIQUE INDEX `urlName_UNIQUE` (`urlName`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- urls
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `urls`;

CREATE TABLE `urls`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `moduleName` VARCHAR(45),
    `urlNameID` INTEGER,
    `websiteID` INTEGER,
    `action` VARCHAR(45),
    `method` VARCHAR(45),
    `urlType` enum('page','ajax','redirect'),
    PRIMARY KEY (`id`),
    UNIQUE INDEX `index3` (`urlNameID`, `websiteID`),
    INDEX `fk_urls_1_idx` (`websiteID`),
    INDEX `fk_urls_2_idx` (`urlNameID`),
    CONSTRAINT `fk_urls_2`
        FOREIGN KEY (`urlNameID`)
        REFERENCES `urlNames` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
