
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- currencyRates
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `currencyRates`;

CREATE TABLE `currencyRates`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `code` VARCHAR(3) NOT NULL,
    `rate` FLOAT NOT NULL,
    `name` VARCHAR(255) NOT NULL,
    `alias` VARCHAR(255) NOT NULL,
    `symbol` VARCHAR(15) NOT NULL,
    `testcolumn` VARCHAR(45),
    PRIMARY KEY (`id`),
    INDEX `code` (`code`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- duplicateJobs
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `duplicateJobs`;

CREATE TABLE `duplicateJobs`
(
    `jobID` INTEGER NOT NULL,
    `duplicateJobID` INTEGER,
    PRIMARY KEY (`jobID`),
    INDEX `fk_duplicateJobs_2_idx` (`duplicateJobID`),
    CONSTRAINT `fk_duplicateJobs_1`
        FOREIGN KEY (`jobID`)
        REFERENCES `jobs` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `fk_duplicateJobs_2`
        FOREIGN KEY (`duplicateJobID`)
        REFERENCES `jobs` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- jobs
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jobs`;

CREATE TABLE `jobs`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `languageID` INTEGER DEFAULT 1,
    `originID` INTEGER NOT NULL,
    `title` VARCHAR(255) NOT NULL,
    `description` TEXT NOT NULL,
    `vacRef` VARCHAR(145) NOT NULL,
    `postedDate` DATETIME NOT NULL,
    `modificationDate` DATETIME,
    `expirationDate` DATE,
    `tagFree` TEXT NOT NULL,
    `education` TEXT,
    `requirements` TEXT,
    PRIMARY KEY (`id`),
    INDEX `fk_jobs_2_idx` (`languageID`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- jobsAnalytics
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jobsAnalytics`;

CREATE TABLE `jobsAnalytics`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `websiteID` INTEGER NOT NULL,
    `visits` INTEGER DEFAULT 0,
    `clicks` INTEGER DEFAULT 0,
    PRIMARY KEY (`id`,`websiteID`),
    INDEX `fk_jobsAnalytics_2_idx` (`websiteID`),
    CONSTRAINT `fk_jobsAnalytics_1`
        FOREIGN KEY (`id`)
        REFERENCES `jobs` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- jobsClassifications
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jobsClassifications`;

CREATE TABLE `jobsClassifications`
(
    `id` DOUBLE NOT NULL AUTO_INCREMENT,
    `jobID` INTEGER NOT NULL,
    `classificationID` INTEGER NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `jobID` (`jobID`, `classificationID`),
    INDEX `classificationID` (`classificationID`),
    INDEX `jobID_2` (`jobID`),
    INDEX `fk_jobsXIndustries_1_idx` (`classificationID`),
    INDEX `fk_jobsXIndustries_2_idx` (`jobID`),
    CONSTRAINT `fk_jobsXIndustries_2`
        FOREIGN KEY (`jobID`)
        REFERENCES `jobs` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- jobsPrivate
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jobsPrivate`;

CREATE TABLE `jobsPrivate`
(
    `id` INTEGER NOT NULL,
    `branchID` INTEGER,
    `contactName` CHAR(150),
    `contactEmail` CHAR(150),
    `HTTPREF` VARCHAR(245) NOT NULL,
    `companyName` VARCHAR(145),
    PRIMARY KEY (`id`),
    UNIQUE INDEX `HTTPREF_UNIQUE` (`HTTPREF`),
    INDEX `companyID` (`branchID`),
    INDEX `fk_jobsCompany_1_idx` (`id`),
    CONSTRAINT `fk_jobsCompany_1`
        FOREIGN KEY (`id`)
        REFERENCES `jobs` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- jobsSalary
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jobsSalary`;

CREATE TABLE `jobsSalary`
(
    `id` INTEGER NOT NULL,
    `salaryFrom` FLOAT NOT NULL,
    `salaryTo` FLOAT NOT NULL,
    `salaryCurrencyID` INTEGER,
    `salaryText` VARCHAR(145),
    `salaryTypeID` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `fk_jobsPropertyIDs_3_idx` (`id`),
    INDEX `fk_jobsSalary_1_idx` (`salaryTypeID`),
    INDEX `fk_jobsSalary_2_idx` (`salaryCurrencyID`),
    CONSTRAINT `fk_jobsPropertyIDs_3`
        FOREIGN KEY (`id`)
        REFERENCES `jobs` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `fk_jobsSalary_1`
        FOREIGN KEY (`salaryTypeID`)
        REFERENCES `salaryType` (`id`)
        ON UPDATE CASCADE,
    CONSTRAINT `fk_jobsSalary_2`
        FOREIGN KEY (`salaryCurrencyID`)
        REFERENCES `currencyRates` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- jobsSuspended
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jobsSuspended`;

CREATE TABLE `jobsSuspended`
(
    `id` INTEGER NOT NULL,
    `suspendedDate` DATE NOT NULL,
    `suspendedReasonID` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `fk_suspendedJobs_1_idx` (`id`),
    INDEX `fk_jobsSuspended_1_idx` (`suspendedReasonID`),
    CONSTRAINT `fk_jobsSuspended_1`
        FOREIGN KEY (`suspendedReasonID`)
        REFERENCES `suspendedReasons` (`id`)
        ON UPDATE CASCADE,
    CONSTRAINT `fk_suspendedJobs_1`
        FOREIGN KEY (`id`)
        REFERENCES `jobs` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- salaryType
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `salaryType`;

CREATE TABLE `salaryType`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(100) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- suspendedReasons
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `suspendedReasons`;

CREATE TABLE `suspendedReasons`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(145),
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
