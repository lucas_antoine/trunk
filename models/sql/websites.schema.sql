
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- containerWidgetOptions
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `containerWidgetOptions`;

CREATE TABLE `containerWidgetOptions`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `containerWidgetID` INTEGER,
    `widgetOptionID` INTEGER,
    `value` TEXT,
    PRIMARY KEY (`id`),
    INDEX `fk_containerWidgetOptions_1_idx` (`widgetOptionID`),
    INDEX `fk_containerWidgetOptions_2_idx` (`containerWidgetID`),
    CONSTRAINT `fk_containerWidgetOptions_1`
        FOREIGN KEY (`widgetOptionID`)
        REFERENCES `widgetOptions` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `fk_containerWidgetOptions_2`
        FOREIGN KEY (`containerWidgetID`)
        REFERENCES `containerWidgets` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- containerWidgets
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `containerWidgets`;

CREATE TABLE `containerWidgets`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `containerID` INTEGER NOT NULL,
    `widgetThemeID` INTEGER NOT NULL,
    `position` INTEGER,
    `align` enum('left','right','center') DEFAULT 'left',
    `backgroundColor` VARCHAR(45),
    `backgroundPosition` VARCHAR(45),
    `backgroundRepeat` VARCHAR(45) DEFAULT 'no-repeat' NOT NULL,
    `borderTopSize` INTEGER DEFAULT 0 NOT NULL,
    `borderRightSize` INTEGER DEFAULT 0 NOT NULL,
    `borderBottomSize` INTEGER DEFAULT 0 NOT NULL,
    `borderLeftSize` INTEGER DEFAULT 0 NOT NULL,
    `borderTopLeftRadius` INTEGER DEFAULT 0 NOT NULL,
    `borderTopRightRadius` INTEGER DEFAULT 0 NOT NULL,
    `borderBottomRightRadius` INTEGER DEFAULT 0 NOT NULL,
    `borderBottomLeftRadius` INTEGER DEFAULT 0 NOT NULL,
    `borderTopColor` VARCHAR(45),
    `borderRightColor` VARCHAR(45),
    `borderBottomColor` VARCHAR(45),
    `borderLeftColor` VARCHAR(45),
    `borderTopStyle` VARCHAR(45) DEFAULT 'solid' NOT NULL,
    `borderRightStyle` VARCHAR(45) DEFAULT 'solid' NOT NULL,
    `borderBottomStyle` VARCHAR(45) DEFAULT 'solid' NOT NULL,
    `borderLeftStyle` VARCHAR(45) DEFAULT 'solid' NOT NULL,
    `marginRight` INTEGER DEFAULT 0 NOT NULL,
    `marginBottom` INTEGER DEFAULT 0 NOT NULL,
    `marginTop` INTEGER DEFAULT 0 NOT NULL,
    `marginLeft` INTEGER DEFAULT 0 NOT NULL,
    `paddingTop` INTEGER DEFAULT 0 NOT NULL,
    `paddingRight` INTEGER DEFAULT 0 NOT NULL,
    `paddingBottom` INTEGER DEFAULT 0 NOT NULL,
    `paddingLeft` INTEGER DEFAULT 0 NOT NULL,
    `backgroundImage` TINYINT(1) DEFAULT 0 NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `fk_containerWidgets_1_idx` (`containerID`),
    INDEX `fk_containerWidgets_2_idx` (`widgetThemeID`),
    CONSTRAINT `fk_containerWidgets_1`
        FOREIGN KEY (`containerID`)
        REFERENCES `containers` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `fk_containerWidgets_2`
        FOREIGN KEY (`widgetThemeID`)
        REFERENCES `widgetThemes` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- containers
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `containers`;

CREATE TABLE `containers`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `columns` INTEGER,
    `rowID` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `fk_containers_1_idx` (`rowID`),
    CONSTRAINT `fk_containers_1`
        FOREIGN KEY (`rowID`)
        REFERENCES `row` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- defaultClassificationOrigins
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `defaultClassificationOrigins`;

CREATE TABLE `defaultClassificationOrigins`
(
    `classificationOriginID` INTEGER NOT NULL,
    PRIMARY KEY (`classificationOriginID`),
    INDEX `fk_defaultClassificationOrigins_1_idx` (`classificationOriginID`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- layoutRows
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `layoutRows`;

CREATE TABLE `layoutRows`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `layoutID` INTEGER,
    `rowID` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `fk_layoutRows_1_idx` (`layoutID`),
    INDEX `fk_layoutRows_3_idx` (`rowID`),
    CONSTRAINT `fk_layoutRows_1`
        FOREIGN KEY (`layoutID`)
        REFERENCES `layouts` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `fk_layoutRows_3`
        FOREIGN KEY (`rowID`)
        REFERENCES `row` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- layouts
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `layouts`;

CREATE TABLE `layouts`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `websiteID` INTEGER,
    `urlNameID` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `fk_layouts_1_idx` (`websiteID`),
    INDEX `fk_layouts_2_idx` (`urlNameID`),
    CONSTRAINT `fk_layouts_1`
        FOREIGN KEY (`websiteID`)
        REFERENCES `websites` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- options
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `options`;

CREATE TABLE `options`
(
    `websiteID` INTEGER NOT NULL,
    `preprodDevMode` TINYINT(1),
    `prodMaintenanceMode` TINYINT(1),
    `preprodUrlDevMode` TINYINT(1),
    `nbResultsPerPage` INTEGER,
    `nbRefineItems` INTEGER,
    `defaultUrlID` INTEGER,
    `lastHtAccessGenerationDate` DATETIME,
    `colorScheme` TEXT,
    `width` INTEGER,
    `gutter` INTEGER,
    `nbColumn` INTEGER,
    `imgFolderName` VARCHAR(145) DEFAULT 'img/',
    `cssFolderName` VARCHAR(145) DEFAULT 'css/style.css',
    `jsFolderName` VARCHAR(145) DEFAULT 'js/scripts.js',
    `cssClassNamingType` enum('random','name1','name2') DEFAULT 'random',
    `cssClassNamingOption` VARCHAR(45),
    `expiredRatioEnable` TINYINT(1) DEFAULT 0 NOT NULL,
    `expiredRatio` INTEGER,
    `sitemapSearchURLs` TINYINT(1),
    `sitemapJobURLs` TINYINT(1),
    `sitemapSearchURLDepth` INTEGER,
    `sitemapJobURLMaxDays` INTEGER,
    `sitemapPing` TINYINT(1) DEFAULT 0,
    PRIMARY KEY (`websiteID`),
    CONSTRAINT `fk_options_1`
        FOREIGN KEY (`websiteID`)
        REFERENCES `websites` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- row
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `row`;

CREATE TABLE `row`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `columns` INTEGER,
    `containerID` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `fk_containers_row` (`containerID`),
    CONSTRAINT `fk_containers_row`
        FOREIGN KEY (`containerID`)
        REFERENCES `containers` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- urlNameAllowed
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `urlNameAllowed`;

CREATE TABLE `urlNameAllowed`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `widgetID` INTEGER,
    `urlNameID` INTEGER,
    PRIMARY KEY (`id`),
    INDEX `fk_urlNameAllowed_1_idx` (`widgetID`),
    INDEX `fk_urlNameAllowed_2_idx` (`urlNameID`),
    CONSTRAINT `fk_urlNameAllowed_1`
        FOREIGN KEY (`widgetID`)
        REFERENCES `widgets` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- websiteClassificationOrigins
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `websiteClassificationOrigins`;

CREATE TABLE `websiteClassificationOrigins`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `websiteID` INTEGER,
    `classificationOriginID` INTEGER,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `index4` (`websiteID`, `classificationOriginID`),
    INDEX `fk_websiteSources_2_idx` (`websiteID`),
    INDEX `fk_websiteSources_1_idx` (`classificationOriginID`),
    CONSTRAINT `fk_websiteClassificationOrigins_2`
        FOREIGN KEY (`websiteID`)
        REFERENCES `websites` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- websiteClassifications
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `websiteClassifications`;

CREATE TABLE `websiteClassifications`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `websiteID` INTEGER NOT NULL,
    `classificationID` INTEGER NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `websiteXCat_unique` (`websiteID`, `classificationID`),
    INDEX `fk_websitesXCategories_1_idx` (`classificationID`),
    INDEX `fk_websitesXCategories_2_idx` (`websiteID`),
    CONSTRAINT `fk_websitesXCategories_2`
        FOREIGN KEY (`websiteID`)
        REFERENCES `websites` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- websiteOrigins
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `websiteOrigins`;

CREATE TABLE `websiteOrigins`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `websiteID` INTEGER,
    `originID` INTEGER,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `websitesSource_unique` (`websiteID`, `originID`),
    INDEX `fk_websitesSources_1_idx` (`websiteID`),
    INDEX `fk_websiteOrigins_1_idx` (`originID`),
    CONSTRAINT `fk_websitesSources_1`
        FOREIGN KEY (`websiteID`)
        REFERENCES `websites` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- websites
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `websites`;

CREATE TABLE `websites`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `websiteName` VARCHAR(255) NOT NULL,
    `url` VARCHAR(255) NOT NULL,
    `IP` CHAR(50),
    `facebookPageID` DOUBLE DEFAULT 0,
    `piwikID` CHAR(20),
    `googlePlusID` VARCHAR(100),
    `bingKey` VARCHAR(100),
    `googleSiteVerificationID` VARCHAR(100),
    `maxJobDays` INTEGER,
    `linkedInID` VARCHAR(45),
    `formulaParam1` INTEGER,
    `formulaParam2` INTEGER,
    PRIMARY KEY (`id`),
    UNIQUE INDEX `id_UNIQUE` (`id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- widgetCSSClassData
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `widgetCSSClassData`;

CREATE TABLE `widgetCSSClassData`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `widgetCSSClassID` INTEGER,
    `websiteID` INTEGER,
    `className` VARCHAR(45),
    PRIMARY KEY (`id`),
    UNIQUE INDEX `index2` (`className`, `websiteID`),
    INDEX `fk_widgetCSSClassData_1_idx` (`websiteID`),
    INDEX `fk_widgetCSSClassData_2_idx` (`widgetCSSClassID`),
    CONSTRAINT `fk_widgetCSSClassData_1`
        FOREIGN KEY (`websiteID`)
        REFERENCES `websites` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `fk_widgetCSSClassData_2`
        FOREIGN KEY (`widgetCSSClassID`)
        REFERENCES `widgetCSSClassName` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- widgetCSSClassName
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `widgetCSSClassName`;

CREATE TABLE `widgetCSSClassName`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `themeID` INTEGER,
    `originalClassName` VARCHAR(45),
    PRIMARY KEY (`id`),
    UNIQUE INDEX `originalClassName_UNIQUE` (`originalClassName`, `themeID`),
    INDEX `fk_widgetCSSClassName_1_idx` (`themeID`),
    CONSTRAINT `fk_widgetCSSClassName_1`
        FOREIGN KEY (`themeID`)
        REFERENCES `widgetThemes` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- widgetOptions
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `widgetOptions`;

CREATE TABLE `widgetOptions`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `widgetThemeID` INTEGER,
    `optionName` VARCHAR(45),
    `type` VARCHAR(45),
    `defaultValue` VARCHAR(45),
    `required` TINYINT(1),
    `min` INTEGER,
    `max` INTEGER,
    `allowedValues` TEXT,
    PRIMARY KEY (`id`),
    INDEX `fk_widgetOptions_1_idx` (`widgetThemeID`),
    CONSTRAINT `fk_widgetOptions_1`
        FOREIGN KEY (`widgetThemeID`)
        REFERENCES `widgetThemes` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- widgetStyles
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `widgetStyles`;

CREATE TABLE `widgetStyles`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `websiteID` INTEGER,
    `widgetVariableID` INTEGER,
    `containerWidgetID` INTEGER,
    `variableValue` VARCHAR(245),
    PRIMARY KEY (`id`),
    INDEX `fk_widgetStyles_2_idx` (`widgetVariableID`),
    INDEX `fk_widgetStyles_3_idx` (`containerWidgetID`),
    INDEX `fk_widgetStyles_1_idx` (`websiteID`),
    CONSTRAINT `fk_widgetStyles_1`
        FOREIGN KEY (`websiteID`)
        REFERENCES `websites` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `fk_widgetStyles_2`
        FOREIGN KEY (`widgetVariableID`)
        REFERENCES `widgetThemeVariables` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    CONSTRAINT `fk_widgetStyles_3`
        FOREIGN KEY (`containerWidgetID`)
        REFERENCES `containerWidgets` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- widgetThemeVariables
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `widgetThemeVariables`;

CREATE TABLE `widgetThemeVariables`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `widgetThemeID` INTEGER,
    `variableName` VARCHAR(45),
    `defaultValue` VARCHAR(245),
    `description` VARCHAR(45),
    `variableType` VARCHAR(25),
    `tag` VARCHAR(45),
    PRIMARY KEY (`id`),
    UNIQUE INDEX `index3` (`variableName`, `widgetThemeID`),
    INDEX `fk_widgetThemeVariables_1_idx` (`widgetThemeID`),
    CONSTRAINT `fk_widgetThemeVariables_1`
        FOREIGN KEY (`widgetThemeID`)
        REFERENCES `widgetThemes` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- widgetThemes
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `widgetThemes`;

CREATE TABLE `widgetThemes`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `widgetID` INTEGER,
    `themeName` VARCHAR(45),
    PRIMARY KEY (`id`),
    UNIQUE INDEX `index2` (`widgetID`, `themeName`),
    INDEX `fk_widgetThemes_1_idx` (`widgetID`),
    CONSTRAINT `fk_widgetThemes_1`
        FOREIGN KEY (`widgetID`)
        REFERENCES `widgets` (`id`)
        ON UPDATE CASCADE
        ON DELETE CASCADE
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- widgets
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `widgets`;

CREATE TABLE `widgets`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(45),
    `module` VARCHAR(45),
    `description` TEXT,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
