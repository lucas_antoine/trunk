<?php

namespace MODELS\MARKETING\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'tags' table.
 *
 *
 * This class was autogenerated by Propel 1.6.7 on:
 *
 * Sun Feb 16 17:09:51 2014
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.marketing.map
 */
class tagsTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'marketing.map.tagsTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('tags');
        $this->setPhpName('tags');
        $this->setClassname('MODELS\\MARKETING\\tags');
        $this->setPackage('marketing');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('ID', 'id', 'INTEGER', true, null, null);
        $this->addColumn('NAME', 'name', 'VARCHAR', false, 145, null);
        $this->addColumn('DESCRIPTION', 'description', 'VARCHAR', false, 245, null);
        $this->addForeignKey('TAGGROUPID', 'tagGroupID', 'INTEGER', 'tagGroups', 'ID', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('tagGroups', 'MODELS\\MARKETING\\tagGroups', RelationMap::MANY_TO_ONE, array('tagGroupID' => 'id', ), null, null);
        $this->addRelation('branchTags', 'MODELS\\MARKETING\\branchTags', RelationMap::ONE_TO_MANY, array('id' => 'tagID', ), null, null, 'branchTagss');
    } // buildRelations()

} // tagsTableMap
