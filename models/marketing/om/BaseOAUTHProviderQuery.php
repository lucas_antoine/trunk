<?php

namespace MODELS\MARKETING\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use MODELS\MARKETING\OAUTH;
use MODELS\MARKETING\OAUTHProvider;
use MODELS\MARKETING\OAUTHProviderPeer;
use MODELS\MARKETING\OAUTHProviderQuery;

/**
 * Base class that represents a query for the 'OAUTHProvider' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.6.7 on:
 *
 * Sun Feb 16 17:09:50 2014
 *
 * @method OAUTHProviderQuery orderByid($order = Criteria::ASC) Order by the id column
 * @method OAUTHProviderQuery orderByname($order = Criteria::ASC) Order by the name column
 * @method OAUTHProviderQuery orderBywebsite($order = Criteria::ASC) Order by the website column
 *
 * @method OAUTHProviderQuery groupByid() Group by the id column
 * @method OAUTHProviderQuery groupByname() Group by the name column
 * @method OAUTHProviderQuery groupBywebsite() Group by the website column
 *
 * @method OAUTHProviderQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method OAUTHProviderQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method OAUTHProviderQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method OAUTHProviderQuery leftJoinOAUTH($relationAlias = null) Adds a LEFT JOIN clause to the query using the OAUTH relation
 * @method OAUTHProviderQuery rightJoinOAUTH($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OAUTH relation
 * @method OAUTHProviderQuery innerJoinOAUTH($relationAlias = null) Adds a INNER JOIN clause to the query using the OAUTH relation
 *
 * @method OAUTHProvider findOne(PropelPDO $con = null) Return the first OAUTHProvider matching the query
 * @method OAUTHProvider findOneOrCreate(PropelPDO $con = null) Return the first OAUTHProvider matching the query, or a new OAUTHProvider object populated from the query conditions when no match is found
 *
 * @method OAUTHProvider findOneByid(int $id) Return the first OAUTHProvider filtered by the id column
 * @method OAUTHProvider findOneByname(string $name) Return the first OAUTHProvider filtered by the name column
 * @method OAUTHProvider findOneBywebsite(string $website) Return the first OAUTHProvider filtered by the website column
 *
 * @method array findByid(int $id) Return OAUTHProvider objects filtered by the id column
 * @method array findByname(string $name) Return OAUTHProvider objects filtered by the name column
 * @method array findBywebsite(string $website) Return OAUTHProvider objects filtered by the website column
 *
 * @package    propel.generator.marketing.om
 */
abstract class BaseOAUTHProviderQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseOAUTHProviderQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'MARKETING2', $modelName = 'MODELS\\MARKETING\\OAUTHProvider', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new OAUTHProviderQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     OAUTHProviderQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return OAUTHProviderQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof OAUTHProviderQuery) {
            return $criteria;
        }
        $query = new OAUTHProviderQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   OAUTHProvider|OAUTHProvider[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = OAUTHProviderPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(OAUTHProviderPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   OAUTHProvider A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `NAME`, `WEBSITE` FROM `OAUTHProvider` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new OAUTHProvider();
            $obj->hydrate($row);
            OAUTHProviderPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return OAUTHProvider|OAUTHProvider[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|OAUTHProvider[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return OAUTHProviderQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(OAUTHProviderPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return OAUTHProviderQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(OAUTHProviderPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterByid(1234); // WHERE id = 1234
     * $query->filterByid(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterByid(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return OAUTHProviderQuery The current query, for fluid interface
     */
    public function filterByid($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(OAUTHProviderPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByname('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByname('%fooValue%'); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return OAUTHProviderQuery The current query, for fluid interface
     */
    public function filterByname($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $name)) {
                $name = str_replace('*', '%', $name);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(OAUTHProviderPeer::NAME, $name, $comparison);
    }

    /**
     * Filter the query on the website column
     *
     * Example usage:
     * <code>
     * $query->filterBywebsite('fooValue');   // WHERE website = 'fooValue'
     * $query->filterBywebsite('%fooValue%'); // WHERE website LIKE '%fooValue%'
     * </code>
     *
     * @param     string $website The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return OAUTHProviderQuery The current query, for fluid interface
     */
    public function filterBywebsite($website = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($website)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $website)) {
                $website = str_replace('*', '%', $website);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(OAUTHProviderPeer::WEBSITE, $website, $comparison);
    }

    /**
     * Filter the query by a related OAUTH object
     *
     * @param   OAUTH|PropelObjectCollection $oAUTH  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   OAUTHProviderQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByOAUTH($oAUTH, $comparison = null)
    {
        if ($oAUTH instanceof OAUTH) {
            return $this
                ->addUsingAlias(OAUTHProviderPeer::ID, $oAUTH->getproviderID(), $comparison);
        } elseif ($oAUTH instanceof PropelObjectCollection) {
            return $this
                ->useOAUTHQuery()
                ->filterByPrimaryKeys($oAUTH->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByOAUTH() only accepts arguments of type OAUTH or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OAUTH relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return OAUTHProviderQuery The current query, for fluid interface
     */
    public function joinOAUTH($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OAUTH');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OAUTH');
        }

        return $this;
    }

    /**
     * Use the OAUTH relation OAUTH object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MODELS\MARKETING\OAUTHQuery A secondary query class using the current class as primary query
     */
    public function useOAUTHQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinOAUTH($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OAUTH', '\MODELS\MARKETING\OAUTHQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   OAUTHProvider $oAUTHProvider Object to remove from the list of results
     *
     * @return OAUTHProviderQuery The current query, for fluid interface
     */
    public function prune($oAUTHProvider = null)
    {
        if ($oAUTHProvider) {
            $this->addUsingAlias(OAUTHProviderPeer::ID, $oAUTHProvider->getid(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
