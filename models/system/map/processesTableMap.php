<?php

namespace MODELS\SYSTEM\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'processes' table.
 *
 *
 * This class was autogenerated by Propel 1.6.7 on:
 *
 * Sun Feb 16 17:09:54 2014
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.system.map
 */
class processesTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'system.map.processesTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('processes');
        $this->setPhpName('processes');
        $this->setClassname('MODELS\\SYSTEM\\processes');
        $this->setPackage('system');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('ID', 'id', 'INTEGER', true, null, null);
        $this->addColumn('NAME', 'name', 'VARCHAR', false, 45, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('pid', 'MODELS\\SYSTEM\\pid', RelationMap::ONE_TO_MANY, array('id' => 'id', ), null, 'CASCADE', 'pids');
    } // buildRelations()

} // processesTableMap
