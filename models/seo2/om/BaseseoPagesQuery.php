<?php

namespace MODELS\SEO2\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use MODELS\SEO2\seoPageData;
use MODELS\SEO2\seoPages;
use MODELS\SEO2\seoPagesPeer;
use MODELS\SEO2\seoPagesQuery;

/**
 * Base class that represents a query for the 'seoPages' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.6.7 on:
 *
 * Sun Feb 16 17:09:51 2014
 *
 * @method seoPagesQuery orderByurlID($order = Criteria::ASC) Order by the urlID column
 * @method seoPagesQuery orderByspecialFunction($order = Criteria::ASC) Order by the specialFunction column
 * @method seoPagesQuery orderBydefaultTitle($order = Criteria::ASC) Order by the defaultTitle column
 * @method seoPagesQuery orderBydefaultDescription($order = Criteria::ASC) Order by the defaultDescription column
 * @method seoPagesQuery orderBydefaultCustomText($order = Criteria::ASC) Order by the defaultCustomText column
 *
 * @method seoPagesQuery groupByurlID() Group by the urlID column
 * @method seoPagesQuery groupByspecialFunction() Group by the specialFunction column
 * @method seoPagesQuery groupBydefaultTitle() Group by the defaultTitle column
 * @method seoPagesQuery groupBydefaultDescription() Group by the defaultDescription column
 * @method seoPagesQuery groupBydefaultCustomText() Group by the defaultCustomText column
 *
 * @method seoPagesQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method seoPagesQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method seoPagesQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method seoPagesQuery leftJoinseoPageData($relationAlias = null) Adds a LEFT JOIN clause to the query using the seoPageData relation
 * @method seoPagesQuery rightJoinseoPageData($relationAlias = null) Adds a RIGHT JOIN clause to the query using the seoPageData relation
 * @method seoPagesQuery innerJoinseoPageData($relationAlias = null) Adds a INNER JOIN clause to the query using the seoPageData relation
 *
 * @method seoPages findOne(PropelPDO $con = null) Return the first seoPages matching the query
 * @method seoPages findOneOrCreate(PropelPDO $con = null) Return the first seoPages matching the query, or a new seoPages object populated from the query conditions when no match is found
 *
 * @method seoPages findOneByurlID(int $urlID) Return the first seoPages filtered by the urlID column
 * @method seoPages findOneByspecialFunction(string $specialFunction) Return the first seoPages filtered by the specialFunction column
 * @method seoPages findOneBydefaultTitle(string $defaultTitle) Return the first seoPages filtered by the defaultTitle column
 * @method seoPages findOneBydefaultDescription(string $defaultDescription) Return the first seoPages filtered by the defaultDescription column
 * @method seoPages findOneBydefaultCustomText(string $defaultCustomText) Return the first seoPages filtered by the defaultCustomText column
 *
 * @method array findByurlID(int $urlID) Return seoPages objects filtered by the urlID column
 * @method array findByspecialFunction(string $specialFunction) Return seoPages objects filtered by the specialFunction column
 * @method array findBydefaultTitle(string $defaultTitle) Return seoPages objects filtered by the defaultTitle column
 * @method array findBydefaultDescription(string $defaultDescription) Return seoPages objects filtered by the defaultDescription column
 * @method array findBydefaultCustomText(string $defaultCustomText) Return seoPages objects filtered by the defaultCustomText column
 *
 * @package    propel.generator.seo2.om
 */
abstract class BaseseoPagesQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseseoPagesQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'SEO2', $modelName = 'MODELS\\SEO2\\seoPages', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new seoPagesQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     seoPagesQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return seoPagesQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof seoPagesQuery) {
            return $criteria;
        }
        $query = new seoPagesQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   seoPages|seoPages[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = seoPagesPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(seoPagesPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   seoPages A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `URLID`, `SPECIALFUNCTION`, `DEFAULTTITLE`, `DEFAULTDESCRIPTION`, `DEFAULTCUSTOMTEXT` FROM `seoPages` WHERE `URLID` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new seoPages();
            $obj->hydrate($row);
            seoPagesPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return seoPages|seoPages[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|seoPages[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return seoPagesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(seoPagesPeer::URLID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return seoPagesQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(seoPagesPeer::URLID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the urlID column
     *
     * Example usage:
     * <code>
     * $query->filterByurlID(1234); // WHERE urlID = 1234
     * $query->filterByurlID(array(12, 34)); // WHERE urlID IN (12, 34)
     * $query->filterByurlID(array('min' => 12)); // WHERE urlID > 12
     * </code>
     *
     * @param     mixed $urlID The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return seoPagesQuery The current query, for fluid interface
     */
    public function filterByurlID($urlID = null, $comparison = null)
    {
        if (is_array($urlID) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(seoPagesPeer::URLID, $urlID, $comparison);
    }

    /**
     * Filter the query on the specialFunction column
     *
     * Example usage:
     * <code>
     * $query->filterByspecialFunction('fooValue');   // WHERE specialFunction = 'fooValue'
     * $query->filterByspecialFunction('%fooValue%'); // WHERE specialFunction LIKE '%fooValue%'
     * </code>
     *
     * @param     string $specialFunction The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return seoPagesQuery The current query, for fluid interface
     */
    public function filterByspecialFunction($specialFunction = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($specialFunction)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $specialFunction)) {
                $specialFunction = str_replace('*', '%', $specialFunction);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(seoPagesPeer::SPECIALFUNCTION, $specialFunction, $comparison);
    }

    /**
     * Filter the query on the defaultTitle column
     *
     * Example usage:
     * <code>
     * $query->filterBydefaultTitle('fooValue');   // WHERE defaultTitle = 'fooValue'
     * $query->filterBydefaultTitle('%fooValue%'); // WHERE defaultTitle LIKE '%fooValue%'
     * </code>
     *
     * @param     string $defaultTitle The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return seoPagesQuery The current query, for fluid interface
     */
    public function filterBydefaultTitle($defaultTitle = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($defaultTitle)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $defaultTitle)) {
                $defaultTitle = str_replace('*', '%', $defaultTitle);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(seoPagesPeer::DEFAULTTITLE, $defaultTitle, $comparison);
    }

    /**
     * Filter the query on the defaultDescription column
     *
     * Example usage:
     * <code>
     * $query->filterBydefaultDescription('fooValue');   // WHERE defaultDescription = 'fooValue'
     * $query->filterBydefaultDescription('%fooValue%'); // WHERE defaultDescription LIKE '%fooValue%'
     * </code>
     *
     * @param     string $defaultDescription The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return seoPagesQuery The current query, for fluid interface
     */
    public function filterBydefaultDescription($defaultDescription = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($defaultDescription)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $defaultDescription)) {
                $defaultDescription = str_replace('*', '%', $defaultDescription);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(seoPagesPeer::DEFAULTDESCRIPTION, $defaultDescription, $comparison);
    }

    /**
     * Filter the query on the defaultCustomText column
     *
     * Example usage:
     * <code>
     * $query->filterBydefaultCustomText('fooValue');   // WHERE defaultCustomText = 'fooValue'
     * $query->filterBydefaultCustomText('%fooValue%'); // WHERE defaultCustomText LIKE '%fooValue%'
     * </code>
     *
     * @param     string $defaultCustomText The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return seoPagesQuery The current query, for fluid interface
     */
    public function filterBydefaultCustomText($defaultCustomText = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($defaultCustomText)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $defaultCustomText)) {
                $defaultCustomText = str_replace('*', '%', $defaultCustomText);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(seoPagesPeer::DEFAULTCUSTOMTEXT, $defaultCustomText, $comparison);
    }

    /**
     * Filter the query by a related seoPageData object
     *
     * @param   seoPageData|PropelObjectCollection $seoPageData  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   seoPagesQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByseoPageData($seoPageData, $comparison = null)
    {
        if ($seoPageData instanceof seoPageData) {
            return $this
                ->addUsingAlias(seoPagesPeer::URLID, $seoPageData->getseoPageID(), $comparison);
        } elseif ($seoPageData instanceof PropelObjectCollection) {
            return $this
                ->useseoPageDataQuery()
                ->filterByPrimaryKeys($seoPageData->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByseoPageData() only accepts arguments of type seoPageData or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the seoPageData relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return seoPagesQuery The current query, for fluid interface
     */
    public function joinseoPageData($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('seoPageData');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'seoPageData');
        }

        return $this;
    }

    /**
     * Use the seoPageData relation seoPageData object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MODELS\SEO2\seoPageDataQuery A secondary query class using the current class as primary query
     */
    public function useseoPageDataQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinseoPageData($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'seoPageData', '\MODELS\SEO2\seoPageDataQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   seoPages $seoPages Object to remove from the list of results
     *
     * @return seoPagesQuery The current query, for fluid interface
     */
    public function prune($seoPages = null)
    {
        if ($seoPages) {
            $this->addUsingAlias(seoPagesPeer::URLID, $seoPages->geturlID(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
