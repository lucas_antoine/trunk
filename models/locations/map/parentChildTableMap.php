<?php

namespace MODELS\LOCATIONS\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'parentChild' table.
 *
 *
 * This class was autogenerated by Propel 1.6.7 on:
 *
 * Sun Feb 16 17:09:49 2014
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.locations.map
 */
class parentChildTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'locations.map.parentChildTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('parentChild');
        $this->setPhpName('parentChild');
        $this->setClassname('MODELS\\LOCATIONS\\parentChild');
        $this->setPackage('locations');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('ID', 'id', 'INTEGER', true, null, 0);
        $this->addColumn('PARENTID', 'parentID', 'INTEGER', true, null, null);
        $this->addColumn('DEPTH', 'depth', 'INTEGER', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // parentChildTableMap
