<?php

namespace MODELS\LOCATIONS\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \ModelJoin;
use \PDO;
use \Propel;
use \PropelCollection;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use MODELS\LOCATIONS\language;
use MODELS\LOCATIONS\languageCountry;
use MODELS\LOCATIONS\languageName;
use MODELS\LOCATIONS\languagePeer;
use MODELS\LOCATIONS\languageQuery;

/**
 * Base class that represents a query for the 'languages' table.
 *
 *
 *
 * This class was autogenerated by Propel 1.6.7 on:
 *
 * Sun Feb 16 17:09:49 2014
 *
 * @method languageQuery orderByid($order = Criteria::ASC) Order by the id column
 * @method languageQuery orderBycode3($order = Criteria::ASC) Order by the code3 column
 * @method languageQuery orderBycode2($order = Criteria::ASC) Order by the code2 column
 *
 * @method languageQuery groupByid() Group by the id column
 * @method languageQuery groupBycode3() Group by the code3 column
 * @method languageQuery groupBycode2() Group by the code2 column
 *
 * @method languageQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method languageQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method languageQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method languageQuery leftJoinLanguageCountry($relationAlias = null) Adds a LEFT JOIN clause to the query using the LanguageCountry relation
 * @method languageQuery rightJoinLanguageCountry($relationAlias = null) Adds a RIGHT JOIN clause to the query using the LanguageCountry relation
 * @method languageQuery innerJoinLanguageCountry($relationAlias = null) Adds a INNER JOIN clause to the query using the LanguageCountry relation
 *
 * @method languageQuery leftJoinLanguageName($relationAlias = null) Adds a LEFT JOIN clause to the query using the LanguageName relation
 * @method languageQuery rightJoinLanguageName($relationAlias = null) Adds a RIGHT JOIN clause to the query using the LanguageName relation
 * @method languageQuery innerJoinLanguageName($relationAlias = null) Adds a INNER JOIN clause to the query using the LanguageName relation
 *
 * @method language findOne(PropelPDO $con = null) Return the first language matching the query
 * @method language findOneOrCreate(PropelPDO $con = null) Return the first language matching the query, or a new language object populated from the query conditions when no match is found
 *
 * @method language findOneByid(int $id) Return the first language filtered by the id column
 * @method language findOneBycode3(string $code3) Return the first language filtered by the code3 column
 * @method language findOneBycode2(string $code2) Return the first language filtered by the code2 column
 *
 * @method array findByid(int $id) Return language objects filtered by the id column
 * @method array findBycode3(string $code3) Return language objects filtered by the code3 column
 * @method array findBycode2(string $code2) Return language objects filtered by the code2 column
 *
 * @package    propel.generator.locations.om
 */
abstract class BaselanguageQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaselanguageQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'Locations', $modelName = 'MODELS\\LOCATIONS\\language', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new languageQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     languageQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return languageQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof languageQuery) {
            return $criteria;
        }
        $query = new languageQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   language|language[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = languagePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(languagePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return   language A model object, or null if the key is not found
     * @throws   PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `ID`, `CODE3`, `CODE2` FROM `languages` WHERE `ID` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new language();
            $obj->hydrate($row);
            languagePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return language|language[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|language[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return languageQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(languagePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return languageQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(languagePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterByid(1234); // WHERE id = 1234
     * $query->filterByid(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterByid(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @see       filterByLanguageCountry()
     *
     * @see       filterByLanguageName()
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return languageQuery The current query, for fluid interface
     */
    public function filterByid($id = null, $comparison = null)
    {
        if (is_array($id) && null === $comparison) {
            $comparison = Criteria::IN;
        }

        return $this->addUsingAlias(languagePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the code3 column
     *
     * Example usage:
     * <code>
     * $query->filterBycode3('fooValue');   // WHERE code3 = 'fooValue'
     * $query->filterBycode3('%fooValue%'); // WHERE code3 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $code3 The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return languageQuery The current query, for fluid interface
     */
    public function filterBycode3($code3 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($code3)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $code3)) {
                $code3 = str_replace('*', '%', $code3);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(languagePeer::CODE3, $code3, $comparison);
    }

    /**
     * Filter the query on the code2 column
     *
     * Example usage:
     * <code>
     * $query->filterBycode2('fooValue');   // WHERE code2 = 'fooValue'
     * $query->filterBycode2('%fooValue%'); // WHERE code2 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $code2 The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return languageQuery The current query, for fluid interface
     */
    public function filterBycode2($code2 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($code2)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $code2)) {
                $code2 = str_replace('*', '%', $code2);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(languagePeer::CODE2, $code2, $comparison);
    }

    /**
     * Filter the query by a related languageCountry object
     *
     * @param   languageCountry|PropelObjectCollection $languageCountry The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   languageQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByLanguageCountry($languageCountry, $comparison = null)
    {
        if ($languageCountry instanceof languageCountry) {
            return $this
                ->addUsingAlias(languagePeer::ID, $languageCountry->getlanguageID(), $comparison);
        } elseif ($languageCountry instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(languagePeer::ID, $languageCountry->toKeyValue('languageID', 'languageID'), $comparison);
        } else {
            throw new PropelException('filterByLanguageCountry() only accepts arguments of type languageCountry or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the LanguageCountry relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return languageQuery The current query, for fluid interface
     */
    public function joinLanguageCountry($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('LanguageCountry');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'LanguageCountry');
        }

        return $this;
    }

    /**
     * Use the LanguageCountry relation languageCountry object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MODELS\LOCATIONS\languageCountryQuery A secondary query class using the current class as primary query
     */
    public function useLanguageCountryQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinLanguageCountry($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'LanguageCountry', '\MODELS\LOCATIONS\languageCountryQuery');
    }

    /**
     * Filter the query by a related languageName object
     *
     * @param   languageName|PropelObjectCollection $languageName The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return   languageQuery The current query, for fluid interface
     * @throws   PropelException - if the provided filter is invalid.
     */
    public function filterByLanguageName($languageName, $comparison = null)
    {
        if ($languageName instanceof languageName) {
            return $this
                ->addUsingAlias(languagePeer::ID, $languageName->getid(), $comparison);
        } elseif ($languageName instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(languagePeer::ID, $languageName->toKeyValue('PrimaryKey', 'id'), $comparison);
        } else {
            throw new PropelException('filterByLanguageName() only accepts arguments of type languageName or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the LanguageName relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return languageQuery The current query, for fluid interface
     */
    public function joinLanguageName($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('LanguageName');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'LanguageName');
        }

        return $this;
    }

    /**
     * Use the LanguageName relation languageName object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   \MODELS\LOCATIONS\languageNameQuery A secondary query class using the current class as primary query
     */
    public function useLanguageNameQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinLanguageName($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'LanguageName', '\MODELS\LOCATIONS\languageNameQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   language $language Object to remove from the list of results
     *
     * @return languageQuery The current query, for fluid interface
     */
    public function prune($language = null)
    {
        if ($language) {
            $this->addUsingAlias(languagePeer::ID, $language->getid(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
