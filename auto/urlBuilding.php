<?
//generated on the 2014-02-09T10:11:34+08:00


namespace{
/**
This Url <b>error</b> uses the following consrcutors : 

 [ constructors used in urlrewrite ]


 [ literals ]


* @return \CORE\URL\url
*/                
function URL_error($data, $urlMode=null){
    $ub = new \CORE\URL\urlBuilder(11);
    return $ub->generateURL($data,$urlMode);
}

/**
This Url <b>getsearchurl</b> uses the following consrcutors : 

 [ constructors used in urlrewrite ]


 [ literals ]


* @return \CORE\URL\url
*/                
function URL_getsearchurl($data, $urlMode=null){
    $ub = new \CORE\URL\urlBuilder(9);
    return $ub->generateURL($data,$urlMode);
}

/**
This Url <b>home</b> uses the following consrcutors : 

 [ constructors used in urlrewrite ]


 [ literals ]


* @return \CORE\URL\url
*/                
function URL_home($data, $urlMode=null){
    $ub = new \CORE\URL\urlBuilder(8);
    return $ub->generateURL($data,$urlMode);
}

/**
This Url <b>job</b> uses the following consrcutors : 

 [ constructors used in urlrewrite ]

Name : job
Path : \JOBSEARCH\COMPONENT\job

 [ literals ]


* @return \CORE\URL\url
*/                
function URL_job($data, $urlMode=null){
    $ub = new \CORE\URL\urlBuilder(7);
    return $ub->generateURL($data,$urlMode);
}

/**
This Url <b>jobRedirection</b> uses the following consrcutors : 

 [ constructors used in urlrewrite ]

Name : Job
Path : \JOBSEARCH\COMPONENT\job

 [ literals ]


* @return \CORE\URL\url
*/                
function URL_jobRedirection($data, $urlMode=null){
    $ub = new \CORE\URL\urlBuilder(12);
    return $ub->generateURL($data,$urlMode);
}

/**
This Url <b>locationautocomplete</b> uses the following consrcutors : 

 [ constructors used in urlrewrite ]


 [ literals ]


* @return \CORE\URL\url
*/                
function URL_locationautocomplete($data, $urlMode=null){
    $ub = new \CORE\URL\urlBuilder(10);
    return $ub->generateURL($data,$urlMode);
}

/**
This Url <b>search</b> uses the following consrcutors : 

 [ constructors used in urlrewrite ]

Name : location
Path : \JOBSEARCH\COMPONENT\classificationP
Name : industry
Path : \JOBSEARCH\COMPONENT\classificationP
Name : employmentType
Path : \JOBSEARCH\COMPONENT\classificationP
Name : keyword
Path : \JOBSEARCH\COMPONENT\keyword

 [ literals ]


* @return \CORE\URL\url
*/                
function URL_search($data, $urlMode=null){
    $ub = new \CORE\URL\urlBuilder(6);
    return $ub->generateURL($data,$urlMode);
}

/**
* @return \JOBSEARCH\COMPONENT\classificationP[]
*/
function GET__JOBSEARCH_COMPONENT_classificationP(){
    return isset(\CORE\site::$urlLoader->extractedObjects["\JOBSEARCH\COMPONENT\classificationP"]) && count(\CORE\site::$urlLoader->extractedObjects["\JOBSEARCH\COMPONENT\classificationP"])>0 ? \CORE\site::$urlLoader->extractedObjects["\JOBSEARCH\COMPONENT\classificationP"] : null;
}

/**
* @return \JOBSEARCH\COMPONENT\keyword[]
*/
function GET__JOBSEARCH_COMPONENT_keyword(){
    return isset(\CORE\site::$urlLoader->extractedObjects["\JOBSEARCH\COMPONENT\keyword"]) && count(\CORE\site::$urlLoader->extractedObjects["\JOBSEARCH\COMPONENT\keyword"])>0 ? \CORE\site::$urlLoader->extractedObjects["\JOBSEARCH\COMPONENT\keyword"] : null;
}

/**
* @return \JOBSEARCH\COMPONENT\job[]
*/
function GET__JOBSEARCH_COMPONENT_job(){
    return isset(\CORE\site::$urlLoader->extractedObjects["\JOBSEARCH\COMPONENT\job"]) && count(\CORE\site::$urlLoader->extractedObjects["\JOBSEARCH\COMPONENT\job"])>0 ? \CORE\site::$urlLoader->extractedObjects["\JOBSEARCH\COMPONENT\job"] : null;
}


/**
 * @return integer
 */
function GET__LITERAL__PAGE(){
    return isset(\CORE\site::$urlLoader->extractedLiterals["Page"]) ? \CORE\site::$urlLoader->extractedLiterals["Page"] : null;
}

/**
 * @return integer
 */
function GET__LITERAL__WHEN(){
    return isset(\CORE\site::$urlLoader->extractedLiterals["When"]) ? \CORE\site::$urlLoader->extractedLiterals["When"] : null;
}
}

?>