<?php
session_start();

header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE'); 
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-Requested-With, X-File-Name, accept, content-type, x-requested-with");
        
if(!isset($_SERVER['FRAMEWORK_PATH'])){
    throw new \myException("please setup a setenv DEVMODE path_to_THE_framework in your Vhost file. if you are in CLI, just do a export DEVMODE=/path/here in your terminal.");
}
if(!is_dir($_SERVER['FRAMEWORK_PATH'])){
    throw new \myException("The framework path specified in your vhost is not valid : '".$_SERVER['FRAMEWORK_PATH']);
}
if( !file_exists($_SERVER['FRAMEWORK_PATH'] . "/php/core/environment.php") ){
    throw new \myException("Required file  : '".$_SERVER['FRAMEWORK_PATH']."/php/core/environment.php' is missing");
}
require_once $_SERVER['FRAMEWORK_PATH'] . "/php/core/environment.php";

\CORE\environment::load(__DIR__);
require_once CORE_BASE."site.php";
use \CORE\site;


try{
    site::init();
    echo site::draw();
}
catch(\Exception $e){
    //throw $e;

    if(DEVMODE == "development"){
        throw $e;
    }
    else{
        $ub = new \CORE\URL\urlBuilder(11);
        $url = $ub->generateURL([]);
        header("Location: ".$url->url);
    }

}
?>
